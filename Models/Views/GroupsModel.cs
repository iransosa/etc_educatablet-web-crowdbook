﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Web_CrowdBook.Models.Views
{
    public class GroupsModel
    {
        public List<ETC.Structures.Models.Groups.Group> Groups { get; set; }
        public int Page { get; set; }
        public bool IsSearch { get; set; }
    }
}