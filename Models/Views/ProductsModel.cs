﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Web_CrowdBook.Models.Views
{
    public class ProductsModel
    {
        public List<ETC.Structures.Models.Groups.GroupProduct> Products { get; set; }
    }
}