﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Web_CrowdBook.Models
{
    public class ErrorModels : BaseModel
    {
        public string Description { get; set; }
    }
}