﻿using System;
using System.Collections.Generic;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Web_CrowdBook.CustomAttributes;

namespace Web_CrowdBook.Models
{
    public class ProfileModel : BaseModel
    {
        public ProfileModel()
        {
            //LevelList = new List<System.Web.Mvc.SelectListItem>();
            GenderList = new List<System.Web.Mvc.SelectListItem>();
            GenderList.Add(new System.Web.Mvc.SelectListItem() { Text = "N/A", Value = "" });
            GenderList.Add(new System.Web.Mvc.SelectListItem() { Text = "", Value = "" });
            GenderList.Add(new System.Web.Mvc.SelectListItem() { Text = "", Value = "" });
        }
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El nombre es requerido")]
        [StringLength(40, MinimumLength = 1, ErrorMessage = "El Nombre debe tener entre 1 y 40 caracteres")]
        [RegularExpression(@"[^0-9_\|°¬!#\$%/\()\?¡¿+{}\[\]:.\,;@ª^*<>=&\-\~\^]*$", ErrorMessage = "El Nombre no permite números ni caracteres especiales.")]
        public string FirstName { get; set; }

        [Display(Name = "Apellido")]
        [Required(ErrorMessage = "El apellido es requerido")]
        [StringLength(40, MinimumLength = 1, ErrorMessage = "El Apellido debe tener entre 1 y 40 caracteres")]
        [RegularExpression(@"[^0-9_\|°¬!#\$%/\()\?¡¿+{}\[\]:.\,;@ª^*<>=&\-\~\^]*$", ErrorMessage = "El Apellido no permite números ni caracteres especiales.")]
        public string LastName { get; set; }

        [Display(Name = "Sexo")]
        public string GenderAbbreviature { get; set; }
        public IList<System.Web.Mvc.SelectListItem> GenderList { get; set; }

        [Display(Name = "Avatar")]
        [ValidateImageFile]
        public HttpPostedFileBase AvatarFile { get; set; }
                
        //[Display(Name = "Universidad")]
        //public long? IdInstitution { get; set; }
        //public string Institution { get; set; }

        //public IList<System.Web.Mvc.SelectListItem> LevelList { get; set; }
        //[Display(Name = "Carrera")]
        //public long? IdLevel { get; set; }
        //public string Level { get; set; }
    }
}