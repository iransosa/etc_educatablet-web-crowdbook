﻿using System;
using System.Collections.Generic;
using System.Web;
using ETC.Structures.Models.Groups;

namespace Web_CrowdBook.Models
{
    public class GroupModels : BaseModel
    {
        public GroupModels()
        {
            Products = new List<GroupProduct>();
        }
        public Group Group { get; set; }
        public long IdPerson { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsOnwer { get; set; }
        public bool IsMember { get; set; }
        public bool MemberRequest { get; set; }
        public bool Editable { get; set; }
        public bool SendMessage { get; set; }
        //public bool DeleteMessage { get; set; }
        public bool PublishResource { get; set; }
        public bool DeleteResource { get; set; }
        public ImageUploadGroupModel ImageFile { get; set; }
        public List<GroupProduct> Products { get; set; }
        //public int ProductsCount { get; set; }
    }
}