﻿using System;
using System.Collections.Generic;
using System.Web;
using ETC.Structures.Models.Groups;
using System.ComponentModel.DataAnnotations;
using Web_CrowdBook.CustomAttributes;
using Web_CrowdBook.ViewModels;

namespace Web_CrowdBook.Models
{
    public class EditGroupModel : BaseModel
    {
        public EditGroupModel()
        {
            Group = new GroupVM();
            LevelList = new List<System.Web.Mvc.SelectListItem>();
            GroupFile = new GroupFile();
        }
        public GroupVM Group { get; set; }
        public GroupFile GroupFile { get; set; }
        public List<GroupType> GroupsTypes { get; set; }
        public List<GroupMode> GroupsModes { get; set; }        
        public IList<System.Web.Mvc.SelectListItem> LevelList { get; set; }
    }   
    public class CreateGroupModel : EditGroupModel
    {
        [Display(Name = "Avatar")]
        [ValidateImageFile]
        public HttpPostedFileBase AvatarFile { get; set; }
    }
    public class UpdateGroupModel : EditGroupModel
    {
        public ImageUploadGroupModel ImageFile { get; set; }
    } 
    public class GroupCreatedModel : BaseModel
    {
        public Group Group { get; set; }
    }
}