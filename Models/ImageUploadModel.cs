﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Web_CrowdBook.CustomAttributes;

namespace Web_CrowdBook.Models
{
    public class ImageUploadModel
    {
        [Required (ErrorMessage="Debe subir una imagen")]
        [ValidateImageFile]
        public HttpPostedFileBase ImageFile { get; set; }
    }
    public class ImageUploadGroupModel : ImageUploadModel
    {
        [Required(ErrorMessage = "El grupo es requerido")]
        [Range(1, Int64.MaxValue)]
        public long IdGroup { get; set; }
    }
}