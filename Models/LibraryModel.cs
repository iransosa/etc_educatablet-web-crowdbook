﻿using System;
using System.Collections.Generic;
using System.Web;
using ETC.Structures.Models.Groups;
using Web_CrowdBook.Models.Views;

namespace Web_CrowdBook.Models
{
    public class LibraryModel : BaseModel
    {
        public LibraryModel()
        {
            Products = new ProductsModel();
        }
        public ProductsModel Products { get; set; }
        public string Search { get; set; }
        public bool IsSearch { get; set; }
        public int Page { get; set; }
    }
}