﻿using System;
using System.Collections.Generic;
using System.Web;
using ETC.Structures.Models;
using Web_CrowdBook.Utils;

namespace Web_CrowdBook.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                bool cookieExpired = false;
                if (HttpContext.Current.Request.Cookies[MyCookies.PERSON_SESSION] == null || HttpContext.Current.Session[Sessions.PERSON_SESSION] == null)
                    cookieExpired = true;
                //load the person session object
                if (cookieExpired)
                {
                    PersonCrowdBook person = new PersonCrowdBook();
                    //load the person object for the session
                    PersonSession personTemp = PersonSession.jsonToObject(HttpContext.Current.User.Identity.Name);
                    //adds the person object to sessions
                    ETC.BL.Person.PersonBL blPerson = new ETC.BL.Person.PersonBL();
                    PersonInformation personInfo = blPerson.GetPersonInformation(personTemp.Email, Utils.Constants.BRAND, "");
                    person = Utils.Utils.ConvertPersonEtToPersonSession(personInfo, personInfo.email);
                    HttpContext.Current.Session.Add(Sessions.PERSON_SESSION, person);
                    HttpCookie cookie = new HttpCookie(Sessions.PERSON_SESSION);
                    cookie.Expires = DateTime.Now.AddMinutes(3);
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
            }
        }
        public PersonCrowdBook Person { get; set; }
    }
}