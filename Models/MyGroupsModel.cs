﻿using System;
using System.Collections.Generic;
using System.Web;
using ETC.Structures.Models.Groups;
using Web_CrowdBook.Models.Views;

namespace Web_CrowdBook.Models
{
    public class MyGroupsModel : BaseModel
    {
        public MyGroupsModel()
        {
            //Groups = new List<Group>();
            Groups = new GroupsModel();
            Groups.Groups = new List<Group>();
        }
        //public List<Group> Groups { get; set; }
        public GroupsModel Groups { get; set; }
        public string Search { get; set; }
        int page;
        public int Page
        {
            get
            {
                return page;
            }
            set
            {
                page = value;
                Groups.Page = value;
            }
        }
        bool isSearch;
        public bool IsSearch
        {
            get
            {
                return isSearch;
            }
            set
            {
                isSearch = value;
                Groups.IsSearch = value;
            }
        }
    }
}