﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace Web_CrowdBook.Models
{
    public class AccountModel
    {
        public LoginModel Login { get; set; }
        public RegisterModel Register { get; set; }

        [Required(ErrorMessage = "El Email es requerido")]
        [EmailAddress(ErrorMessage = "El correo electrónico no es válido")]
        [Display(Name = "Correo Electrónico")]
        [MaxLength(255)]
        public string Email { get; set; }

        public string EncriptionKey { get; set; }
    }
    public class LoginModel
    {
        [Required(ErrorMessage = "El Email es requerido")]
        [EmailAddress(ErrorMessage = "El correo electrónico no es válido")]
        [Display(Name = "Correo Electrónico")]
        //[System.Web.Mvc.Remote("doesUserNameExist", "Login", HttpMethod = "POST", ErrorMessage = "{0} ya existe, por favor ingrese uno distinto.", AdditionalFields = "FLAG")]
        public string LoginEmail { get; set; }

        [Required(ErrorMessage = "La contraseña es requerida")]
        //El maximo de caractes permitidos para la contraseña es 20, se extendio a 500 por que llega encriptada sin embargo se debe validar en el controller
        [RegularExpression(Utils.Constants.REGEX_PASSWORD_ENCRYPTED, ErrorMessage = "{0} debe tener entre 8 y 500 caracteres y contener letras y número.")]
        //[StringLength(500, MinimumLength = 4)]
        [Display(Name = "Contraseña")]
        [System.Web.Mvc.AllowHtml] 
        public string LoginPassword { get; set; }
    }
    public class RegisterModel
    {
        [Required(ErrorMessage = "El Email es requerido")]
        [EmailAddress(ErrorMessage = "El correo electrónico no es válido")]
        [Display(Name = "Correo Electrónico")]
        //[System.Web.Mvc.Remote("doesUserNameExist", "Login", HttpMethod = "POST", ErrorMessage = "{0} ya existe, por favor ingrese uno distinto.", AdditionalFields = "FLAG")]
        public string Email { get; set; }

        [Required(ErrorMessage = "La contraseña es requerida")]
        //El maximo de caractes permitidos para la contraseña es 20, se extendio a 500 por que llega encriptada sin embargo se debe validar en el controller
        [RegularExpression(Utils.Constants.REGEX_PASSWORD_ENCRYPTED, ErrorMessage = "La contraseña debe tener entre 8 y 20 caracteres y contener letras y número.")]
        [Display(Name = "Contraseña")]
        [System.Web.Mvc.AllowHtml] 
        public string Password { get; set; }

        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y su confirmación no coinciden.")]
        [System.Web.Mvc.AllowHtml] 
        public string PasswordConfirm { get; set; }

        [Required(ErrorMessage = "El nombre es requerido")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Tamaño del campo inválido mínimo 1 máximo 100")]
        [RegularExpression(@"[^0-9_\|°¬!#\$%/\()\?¡¿+{}\[\]:.\,;@ª^*<>=&\-\~\^]*$", ErrorMessage = "El Nombre no permite números ni caracteres especiales.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "El apellido es requerido")]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Tamaño del campo inválido mínimo 1 máximo 100")]
        [RegularExpression(@"[^0-9_\|°¬!#\$%/\()\?¡¿+{}\[\]:.\,;@ª^*<>=&\-\~\^]*$", ErrorMessage = "El Apellido no permite números ni caracteres especiales.")]
        public string LastName { get; set; }
    }
    public class ForgotPassword
    {
        [Required(ErrorMessage = @"El campo email es requerido")]
        [EmailAddress(ErrorMessage = @"El formato del email es incorrecto")]
        public string Email { get; set; }
    }
    public class ChangePasswordModel : BaseModel
    {
        [Required(ErrorMessage = "La contraseña es requerida")]
        [Display(Name = "Contraseña actual")]
        [System.Web.Mvc.AllowHtml]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "La contraseña nueva es requerida")]
        [RegularExpression(Utils.Constants.REGEX_PASSWORD_ENCRYPTED, ErrorMessage = "{0} debe tener entre 8 y 500 caracteres y contener letras y número.")]
        [Display(Name = "Contraseña nueva")]
        [System.Web.Mvc.AllowHtml]
        public string Password { get; set; }

        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y su confirmación no coinciden.")]
        [System.Web.Mvc.AllowHtml]
        public string PasswordConfirm { get; set; }

        public string EncriptionKey { get; set; }
    }

    //public class ChangePasswordVm
    //{
    //    [Display(Name = "Nombre de Usuario")]
    //    public string UP_ID { get; set; }

    //    [StringLength(150, MinimumLength = 6)]
    //    [Display(Name = "Contraseña Actual")]
    //    public string OLD_PASSWORD { get; set; }

    //    [StringLength(150, MinimumLength = 6)]
    //    [RegularExpression("^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])(?!.*\\W)(?!.*_).{8,20}$", ErrorMessage = "{0} sólo puede contener letras y números con la siguiente combinación una letra mayúscula, una letra minúscula y un número y debe tener una longitud entre 8 y 20 caracteres.")]
    //    [Display(Name = "Nueva Contraseña")]
    //    public string UP_PASSWORD { get; set; }

    //    [Display(Name = "Confirmar Nueva Contraseña")]
    //    [Compare("UP_PASSWORD", ErrorMessage = "La contraseña y su confirmación no coinciden.")]
    //    public string UP_PASSWORD_CONF { get; set; }
    //}
    //public class PasswordRecoveryVm
    //{
    //    [Required(ErrorMessage = "Campo requerido")]
    //    [EmailAddress]
    //    [Display(Name = "Correo Electrónico")]
    //    public string UP_ID { get; set; }

    //    [Required(ErrorMessage = "Campo requerido")]
    //    [StringLength(150, MinimumLength = 6)]
    //    [RegularExpression("^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z])(?!.*\\W)(?!.*_).{8,20}$", ErrorMessage = "{0} sólo puede contener letras y números con la siguiente combinación una letra mayúscula, una letra minúscula y un número y debe tener una longitud entre 8 y 20 caracteres.")]
    //    [Display(Name = "Nueva Contraseña")]
    //    public string UP_PASSWORD { get; set; }

    //    [Required(ErrorMessage = "Campo requerido")]
    //    [Display(Name = "Confirmar Nueva Contraseña")]
    //    [Compare("UP_PASSWORD", ErrorMessage = "La contraseña y su confirmación no coinciden.")]
    //    public string UP_PASSWORD_CONF { get; set; }

    //    public string token { get; set; }
    //}
}