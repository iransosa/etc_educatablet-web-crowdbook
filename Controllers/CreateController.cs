﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using Web_CrowdBook.Models;
using System.Linq;
using ETC.Structures.Models.Groups;
using ETC.BL.Groups;
using ETC.Structures.Models;
using Web_CrowdBook.Utils;
using ETC.BL.Exceptions;
using Web_CrowdBook.Helpers;
using Web_CrowdBook.ViewModels;

namespace Web_CrowdBook.Controllers
{
    [Authorize]
    public class CreateController : Controller
    {
        IGroupBL bl = new GroupBL();

        #region Create
        [HttpGet]
        public ActionResult Index(long id = 0)
        {
            CreateGroupModel model = new CreateGroupModel();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Person = p;
            model.GroupsModes = bl.GetGroupsModes();
            model.GroupsTypes = bl.GetGroupsTypes();
            return View("Index", model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(CreateGroupModel model)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            if (ModelState.IsValid)
            {
                Group g = Mappers.GroupVMToGroup(model.Group);
                if (g.Institution != null)
                {
                    if (!string.IsNullOrEmpty(model.Group.Institution))
                    {
                        ETC.BL.Institutions.IInstitutionBL blInstitution = new ETC.BL.Institutions.InstitutionBL();
                        List<Institution> i = blInstitution.GetInstitutionsByName(model.Group.Institution, 1, 1);
                        if (i.Count > 0)
                            g.Institution = new Institution() { Id = i[0].Id };
                    }
                }
                long idGroup = bl.SaveGroup(g, (long)p.Id);
                if (idGroup != 0)
                {
                    if (model.AvatarFile != null)
                        SaveAvatar(model.AvatarFile, idGroup, (long)p.Id);
                    UtilsContext.CleanCookieSession(this.HttpContext);
                    return RedirectToAction("Created", "Create", new { id = idGroup });
                }
                else
                {
                    TempData["GlobalAlertMessage"] = Messages.INTERNAL_ERROR;
                    TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
                    return RedirectToAction("Index", "Create");
                }
            }
            model.Person = p;
            model.GroupsModes = bl.GetGroupsModes();
            model.GroupsTypes = bl.GetGroupsTypes();
            return View("Index", model);
        } 
        #endregion

        #region Edit
        [HttpGet]
        public ActionResult Edit(long id)
        {
            EditGroupModel model = new EditGroupModel();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];

            Group group = bl.GetGroupById(id);
            if (group == null)
                return RedirectToAction("NotFound", "Error");
            if (!bl.ValidateGroupMemberPermission(id, (long)p.Id, ETC.BL.Common.GroupPermission.EDIT_GROUP))
                return RedirectToAction("NotFound", "Error");

            model.Group = (GroupVM)Mappers.GroupToGroupVM(group);
            if (model.Group.IdInstitution != 0)
            {
                IList<System.Web.Mvc.SelectListItem> lvls = DataAccess.GetInstitutionLevels(Convert.ToInt32(model.Group.IdInstitution));
                if (lvls != null && lvls.Count > 0)
                    model.LevelList = lvls;
            }
            model.Person = p;
            model.GroupsModes = bl.GetGroupsModes();
            model.GroupsTypes = bl.GetGroupsTypes();
            if (TempData["GlobalAlertMessage"] != null)
            {
                ViewBag.GlobalAlertMessage = TempData["GlobalAlertMessage"];
                ViewBag.GlobalAlertClass = TempData["GlobalAlertClass"];
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(string action, EditGroupModel model)
        {
            if (model.Group.IdGroup == 0)
                return RedirectToAction("NotFound", "Error");
            if (model.Group.ClosingDate != null && model.Group.ExpirationDate != null)
            {
                if (model.Group.ClosingDate > model.Group.ExpirationDate)
                    ModelState.AddModelError("DateClosing bigger", "La Fecha de Cierre no puede ser mayor a la Fecha de Expiración.A");
            }
            if (model.Group.ClosingDate != null)
            {
                if (model.Group.ClosingDate.Value < DateTime.Now || model.Group.ExpirationDate < DateTime.Now)
                    ModelState.AddModelError("DateClosing bigger", "La Fecha de Cierre y Expiración deben ser mayor a la de hoy.");
            }
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            if (ModelState.IsValid)
            {
                Group g = Mappers.GroupVMToGroup(model.Group);
                if (g.Institution != null)
                {
                    if (!string.IsNullOrEmpty(model.Group.Institution))
                    {
                        ETC.BL.Institutions.IInstitutionBL blInstitution = new ETC.BL.Institutions.InstitutionBL();
                        List<Institution> i = blInstitution.GetInstitutionsByName(model.Group.Institution, 1, 1);
                        if (i.Count > 0)
                            g.Institution = new Institution() { Id = i[0].Id };
                    }
                }
                long idGroup = bl.SaveGroup(g, (long)p.Id);
                if (idGroup != 0)
                {
                    return RedirectToAction("Saved", "Create", new { id = idGroup });
                }
                else
                {
                    TempData["GlobalAlertMessage"] = Messages.INTERNAL_ERROR;
                    TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
                    return RedirectToAction("Index", "Create");
                }
            }
            model.Person = p;
            model.GroupsModes = bl.GetGroupsModes();
            model.GroupsTypes = bl.GetGroupsTypes();
            return View(model);
        }
        #endregion

        #region SaveAvatar
        [HttpPost]
        public ActionResult SaveAvatar(ImageUploadGroupModel model, int idGroup)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            if (ModelState.IsValid && model.ImageFile != null && idGroup != 0)
            {
                if (SaveAvatar(model.ImageFile, model.IdGroup, (long)p.Id))
                {
                    TempData["GlobalAlertMessage"] = Messages.AVATAR_CREATED;
                    TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_SUCCESS"];
                }
                else
                {
                    TempData["GlobalAlertMessage"] = Messages.INTERNAL_ERROR;
                    TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_INFO"];
                }
            }
            else
            {
                TempData["GlobalAlertMessage"] = string.Format(FileHandler.FILES_VALID, FileHandler.GetAllowedImageExtensions());
                TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
            }
            return RedirectToAction("Edit", "Create", new { id = model.IdGroup });
        }
        bool SaveAvatar(HttpPostedFileBase file, long idGroup, long idPerson)
        {
            try
            {
                string sha1 = "";
                byte[] bytes = FileHandler.GetFileFromStream(file, ref sha1);
                string mimetype = "";
                if (FileHandler.ValidateImageExtension(bytes, out mimetype))
                {
                    //bl.SaveGroupAvatar(idGroup, bytes, sha1, mimetype, idPerson);
                    bl.SaveGroupFile(idGroup, bytes, sha1, mimetype, idPerson, ETC.BL.Common.FileType.AVATAR);
                    return true;
                }
            }
            catch (Exception)
            {

            }
            return false;
        } 
        #endregion

        #region SaveGroupFile
        [HttpPost]
        public ActionResult SaveGroupFile(ImageUploadGroupModel model, int idGroup, string type)
        {
            ETC.BL.Common.FileType fileType;
            if (type == "avatar")
                fileType = ETC.BL.Common.FileType.AVATAR;
            else if (type == "cover")
                fileType = ETC.BL.Common.FileType.COVER;
            else
                return RedirectToAction("Index", "Group", new { id = model.IdGroup });
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            if (ModelState.IsValid && model.ImageFile != null && idGroup != 0)
            {
                try
                {
                    string sha1 = "", mimetype = "";
                    HttpPostedFileBase file = model.ImageFile;
                    byte[] bytes = FileHandler.GetFileFromStream(file, ref sha1);
                    if (FileHandler.ValidateImageExtension(bytes, out mimetype))
                    {
                        bl.SaveGroupFile(idGroup, bytes, sha1, mimetype, idPerson, fileType);
                        TempData["GlobalAlertMessage"] = Messages.AVATAR_CREATED;
                        TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_SUCCESS"];
                    }
                    else
                    {
                        TempData["GlobalAlertMessage"] = string.Format(FileHandler.FILES_VALID, FileHandler.GetAllowedImageExtensions());
                        TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_INFO"];
                    }
                }
                catch (Exception)
                {
                    TempData["GlobalAlertMessage"] = Messages.INTERNAL_ERROR;
                    TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
                }
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                if (idGroup == 0)
                    sb.Append(Messages.INTERNAL_ERROR);
                else if (model.ImageFile == null)
                    sb.Append(string.Format(FileHandler.FILES_VALID, FileHandler.GetAllowedImageExtensions()));
                else
                {
                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            sb.Append(error.ErrorMessage);
                            sb.Append("<br/>");
                        }
                    }
                }
                TempData["GlobalAlertMessage"] = sb.ToString();
                TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_INFO"];
            }
            return RedirectToAction("Index", "Group", new { id = model.IdGroup });
        }
        #endregion

        #region SaveGroupFileJson
        [HttpPost]
        public WrappedJsonResult SaveGroupFileJson(ImageUploadGroupModel model, int idGroup, string type)
        {
            ETC.BL.Common.FileType fileType;
            if (type == "avatar")
                fileType = ETC.BL.Common.FileType.AVATAR;
            else if (type == "cover")
                fileType = ETC.BL.Common.FileType.COVER;
            else
            {
                return new WrappedJsonResult
                {
                    Data = new { IsValid = false, Message = Utils.Messages.INTERNAL_ERROR, ImagePath = string.Empty }
                };
            }
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            if (ModelState.IsValid && model.ImageFile != null && idGroup != 0)
            {
                try
                {
                    string sha1 = "", mimetype = "";
                    HttpPostedFileBase file = model.ImageFile;
                    byte[] bytes = FileHandler.GetFileFromStream(file, ref sha1);
                    if (FileHandler.ValidateImageExtension(bytes, out mimetype))
                    {
                        //bl.SaveGroupFile(idGroup, bytes, sha1, mimetype, idPerson, fileType);
                        //Insert temp image
                        bl.SaveGroupFileTemp(idGroup, bytes, sha1, mimetype, idPerson, fileType);
                        return new WrappedJsonResult
                        {
                            Data = new { IsValid = true, Message = Messages.AVATAR_CREATED, ImagePath = string.Empty }
                        };
                    }
                    else
                    {
                        return new WrappedJsonResult
                        {
                            Data = new { IsValid = false, Message = string.Format(FileHandler.FILES_VALID, FileHandler.GetAllowedImageExtensions()), ImagePath = string.Empty }
                        };
                    }
                }
                catch (Exception)
                {
                    return new WrappedJsonResult
                    {
                        Data = new { IsValid = false, Message = Utils.Messages.INTERNAL_ERROR, ImagePath = string.Empty }
                    };
                }
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                if (idGroup == 0)
                    sb.Append(Messages.INTERNAL_ERROR);
                else if (model.ImageFile == null)
                    sb.Append(string.Format(FileHandler.FILES_VALID, FileHandler.GetAllowedImageExtensions()));
                else
                {
                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            sb.Append(error.ErrorMessage);
                            sb.Append("<br/>");
                        }
                    }
                }
                return new WrappedJsonResult
                {
                    Data = new { IsValid = false, Message = sb.ToString(), ImagePath = string.Empty }
                };                
            }
        }
        #endregion

        #region SaveGroupResource
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveGroupResource(HttpPostedFileBase file, HttpPostedFileBase cover, int idGroup, string name, string description)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            string errors = "";
            if (!FileHandler.ValidateResourceType(file, ref errors))
                return GoToGroup(idGroup, errors);
            if (!MimeTypes.ValidMimeTypeFromFile(file.InputStream, ref errors))
                return GoToGroup(idGroup, errors);
            if (cover != null)
            {
                if (!FileHandler.ValidateImageType(cover, ref errors))
                    return GoToGroup(idGroup, errors);
            }
            if (string.IsNullOrEmpty(name))
                return GoToGroup(idGroup, Messages.RESOURCE_NAME_REQUIRED);
            name = name.Trim();
            if (name.Length > Characters.RESOURCE_NAME_MAX)
                return GoToGroup(idGroup, Messages.RESOURCE_NAME_CHARS);
            if (!string.IsNullOrEmpty(description))
            {
                description = description.Trim();
                if (description.Length > Characters.RESOURCE_DESCRIPTION_MAX)
                    return GoToGroup(idGroup, Messages.RESOURCE_DESCRIPTION_CHARS);
            }

            if (ModelState.IsValid && file != null && idGroup != 0)
            {
                try
                {
                    //Product product = new Product();
                    //product.Description = description;
                    System.IO.Stream coverStream = null;
                    string coverName = "", coverMime = "";
                    if (cover != null)
                    {
                        //Valid cover Image
                        if (!FileHandler.ValidateImage(cover, ref coverStream))
                        {
                            return Json(new { success = false, message = string.Format(FileHandler.FILES_VALID, FileHandler.GetAllowedResourceExtensions()) });
                        }
                        //coverStream = cover.InputStream;
                        coverName = cover.FileName;
                        coverMime = cover.ContentType;
                    }
                    GroupProduct product = null;
                    bl.SaveGroupProduct(idGroup, p, name, description, file.InputStream, file.FileName, file.ContentType, coverStream, coverName, coverMime, out product);
                    UtilsContext.CleanCookieSession(this.HttpContext);
                    TempData["GlobalAlertMessage"] = Messages.RESOURCE_CREATED;
                    TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_SUCCESS"];
                }
                catch (Exception)
                {
                    TempData["GlobalAlertMessage"] = Messages.INTERNAL_ERROR;
                    TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
                }
            }
            else
            {
                TempData["GlobalAlertMessage"] = string.Format(FileHandler.FILE_REQUIRED, FileHandler.GetAllowedResourceExtensions());
                TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
            }
            return RedirectToAction("Index", "Group", new { id = idGroup });
        }
        #endregion

        #region Close
        [HttpPost]
        public ActionResult Close(long idGroup, DateTime date)
        {
            PersonCrowdBook p = new PersonCrowdBook();
            p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            if (ModelState.IsValid)
            {
                if (bl.CloseGroup(idGroup, (long)p.Id, date))
                {
                    TempData["GlobalAlertMessage"] = Messages.GROUP_CLOSED;
                    TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_INFO"];
                    return RedirectToAction("Index", "MyGroups");
                }
                else
                {
                    TempData["GlobalAlertMessage"] = Messages.INTERNAL_ERROR;
                    TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
                    return RedirectToAction("Index", "Create");
                }
            }
            return View("Index");
        } 
        #endregion

        #region Saved
        [HttpGet]
        public ActionResult Created(long id)
        {
            GroupCreatedModel model = new GroupCreatedModel();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            Group group = bl.GetGroupById(id);
            if (group == null)
                return RedirectToAction("NotFound", "Error");
            if (!bl.ValidateGroupMemberPermission(id, (long)p.Id, ETC.BL.Common.GroupPermission.EDIT_GROUP))
                return RedirectToAction("NotFound", "Error");
            model.Person = p;
            model.Group = group;
            return View(model);
        }
        [HttpGet]
        public ActionResult Saved(long id)
        {
            GroupCreatedModel model = new GroupCreatedModel();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            Group group = bl.GetGroupById(id);
            if (group == null)
                return RedirectToAction("NotFound", "Error");
            if (!bl.ValidateGroupMemberPermission(id, (long)p.Id, ETC.BL.Common.GroupPermission.EDIT_GROUP))
                return RedirectToAction("NotFound", "Error");
            model.Person = p;
            model.Group = group;
            return View(model);
        }
        #endregion
        
        #region SaveGroupResource
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult SaveGroupResource(HttpPostedFileBase file, HttpPostedFileBase cover, int idGroup, string name, string description)
        //{
        //    PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
        //    string fileName = file.FileName;
        //    string fileExtension = System.IO.Path.GetExtension(fileName);
        //    if (!FileHandler.ALLOWED_RESOURCE_EXTENSIONS.Contains(fileExtension))
        //    {
        //        TempData["GlobalAlertMessage"] = string.Format(FileHandler.FILES_VALID, string.Join(", ", FileHandler.ALLOWED_RESOURCE_EXTENSIONS));
        //        TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
        //        return RedirectToAction("Index", "Group", new { id = idGroup });
        //    }
        //    if (cover != null)
        //    {
        //        string coverExtension = System.IO.Path.GetExtension(cover.FileName);
        //        if (!FileHandler.ALLOWED_IMAGE_EXTENSIONS.Contains(coverExtension))
        //        {
        //            TempData["GlobalAlertMessage"] = string.Format(FileHandler.FILES_VALID, string.Join(", ", FileHandler.ALLOWED_IMAGE_EXTENSIONS));
        //            TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
        //            return RedirectToAction("Index", "Group", new { id = idGroup });
        //        }
        //    }
        //    if (string.IsNullOrEmpty(name))
        //    {
        //        TempData["GlobalAlertMessage"] = Messages.RESOURCE_NAME_REQUIRED;
        //        TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
        //        return RedirectToAction("Index", "Group", new { id = idGroup });
        //    }
        //    if (ModelState.IsValid && file != null && idGroup != 0)
        //    {
        //        try
        //        {
        //            //Product product = new Product();
        //            //product.Description = description;
        //            System.IO.Stream coverStream = null;
        //            string coverName = "", coverMime = "";
        //            if (cover != null)
        //            {
        //                coverStream = cover.InputStream;
        //                coverName = cover.FileName;
        //                coverMime = cover.ContentType;
        //            }
        //            GroupProduct product = null;
        //            bl.SaveGroupProduct(idGroup, p, name, description, file.InputStream, fileName, file.ContentType, coverStream, coverName, coverMime, out product);
        //            TempData["GlobalAlertMessage"] = Messages.RESOURCE_CREATED;
        //            TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_SUCCESS"];
        //        }
        //        catch (Exception)
        //        {
        //            TempData["GlobalAlertMessage"] = Messages.INTERNAL_ERROR;
        //            TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
        //        }
        //    }
        //    else
        //    {
        //        TempData["GlobalAlertMessage"] = string.Format(FileHandler.FILE_REQUIRED, string.Join(", ", FileHandler.ALLOWED_RESOURCE_EXTENSIONS));
        //        TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
        //    }
        //    return RedirectToAction("Index", "Group", new { id = idGroup });
        //}
        #endregion

        #region GoToGroup
        ActionResult GoToGroup(long idGroup, string errors)
        {
            TempData["GlobalAlertMessage"] = errors;
            TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_ERROR"];
            return RedirectToAction("Index", "Group", new { id = idGroup });
        } 
        #endregion
    }
}