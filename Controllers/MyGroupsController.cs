﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ETC.Structures.Models;
using Web_CrowdBook.Utils;
using Web_CrowdBook.Models;
using ETC.BL.Groups;
using ETC.Structures.Models.Groups;

namespace Web_CrowdBook.Controllers
{
    [Authorize]
    public class MyGroupsController : Controller
    {
        IGroupBL bl = new GroupBL();

        #region GetMyGroups
        public ActionResult Index(int page = 1)
        {
            MyGroupsModel model = new MyGroupsModel();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Person = p;
            model.Groups = new Models.Views.GroupsModel();
            model.Groups.Groups = GetMyGroups(Convert.ToInt64(p.Id), ((page - 1) * Pagination.LIMIT_PAGE));
            model.Page = page;
            if (TempData["GlobalAlertMessage"] != null)
            {
                ViewBag.GlobalAlertMessage = TempData["GlobalAlertMessage"];
                ViewBag.GlobalAlertClass = TempData["GlobalAlertClass"];
            }
            return View(model);
        } 
        //[HttpPost]
        //public JsonResult GetMyGroups(int page)
        //{
        //    PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
        //    bool sucess = false;
        //    List<Group> groups = null;
        //    try
        //    {
        //        groups = GetMyGroups(Convert.ToInt64(p.Id), page);
        //    }
        //    catch (Exception) { }
        //    var result = new { sucess = sucess, result = groups, page = page + 1 };
        //    return Json(result);
        //}
        List<Group> GetMyGroups(long idPerson, int itemsCurrent)
        {
            List<Group> groups = new List<Group>();
            groups = bl.GetMyGroups(idPerson, itemsCurrent + 1, itemsCurrent + Pagination.LIMIT_PAGE);
            return groups;
        }
        #endregion

        #region Search
        public ActionResult SearchGroup(string search)
        {
            if (string.IsNullOrEmpty(search))
                return RedirectToAction("Index", "MyGroups");
            else
                return RedirectToAction("Search", "MyGroups", new { search = search.Trim() });
        }
        #endregion

        #region Groups
        public ActionResult Search(string search, int page = 1)
        {
            MyGroupsModel model = new MyGroupsModel();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Person = p;
            model.Groups = new Models.Views.GroupsModel();
            model.Groups.Groups = SearchGroups(search, Convert.ToInt64(p.Id), page);
            model.Search = search;
            model.Page = page;
            model.IsSearch = true;
            return View("Index", model);
        }
        //[HttpPost]
        //public JsonResult SearchGroups(string search = "", int page = 0)
        //{
        //    PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
        //    bool sucess = false;
        //    List<Group> groups = null;
        //    try
        //    {
        //        groups = SearchGroups(search, Convert.ToInt64(p.Id), page);
        //    }
        //    catch (Exception) { }
        //    var result = new { sucess = sucess, result = groups, page = page + 1 };
        //    return Json(result);
        //}
        List<Group> SearchGroups(string search, long idPerson, int page)
        {
            List<Group> groups = new List<Group>();
            groups = bl.SearchGroups(search, idPerson, page, Pagination.LIMIT_PAGE);
            return groups;
        }
        #endregion
    }
}