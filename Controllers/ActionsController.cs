﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Web_CrowdBook.Utils;

namespace Web_CrowdBook.Controllers
{
    public class ActionsController : Controller
    {
        public ActionResult LeaveGroup()
        {
            TempData["GlobalAlertMessage"] = Messages.GROUP_LEAVED;
            TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_SUCCESS"];
            return RedirectToAction("Index", "MyGroups");
        }
    }
}