﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Web_CrowdBook.Models;
using Web_CrowdBook.Utils;

namespace Web_CrowdBook.Controllers
{
    public class StartController : Controller
    {
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index", "MyGroups");
            StartModels model = new StartModels();
            model.EncriptionKey = UtilsContext.GenerateEncriptonKey(HttpContext);
            return View(model);
        }
    }
}