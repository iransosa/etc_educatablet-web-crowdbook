﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ETC.Structures.Models;
using Web_CrowdBook.Utils;
using Web_CrowdBook.Models;

namespace Web_CrowdBook.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index()
        {
            ErrorModels model = new ErrorModels();
            if(Request.IsAuthenticated)
                model.Person = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Description = Utils.Messages.INTERNAL_ERROR;
            return View("Index", model);
        }
        public ActionResult BadRequest()
        {
            ErrorModels model = new ErrorModels();
            if (Request.IsAuthenticated)
                model.Person = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Description = Utils.Messages.BAD_REQUEST;
            return View("Index", model);
        }
        public ActionResult NotFound()
        {
            ErrorModels model = new ErrorModels();
            if (Request.IsAuthenticated)
                model.Person = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Description = Utils.Messages.PAGE_NOT_FOUND;
            return View("Index", model);
        }
        public ActionResult ServerFull()
        {
            ErrorModels model = new ErrorModels();
            if (Request.IsAuthenticated)
                model.Person = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Description = Utils.Messages.SERVER_FULL;
            return View("Index", model);
        }
        public ActionResult TooLarge()
        {
            ErrorModels model = new ErrorModels();
            if (Request.IsAuthenticated)
                model.Person = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Description = Utils.Messages.REQUEST_TOO_LARGE;
            return View("Index", model);
        }
        public ActionResult ServiceUnavailable()
        {
            ErrorModels model = new ErrorModels();
            if (Request.IsAuthenticated)
                model.Person = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Description = Utils.Messages.SERVICE_UNAVAILABLE;
            return View("Index", model);
        }
        public ActionResult ResourceNotFound()
        {
            ErrorModels model = new ErrorModels();
            if (Request.IsAuthenticated)
                model.Person = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Description = Utils.Messages.RESOURCE_NOT_FOUND;
            return View("Index", model);
        }
    }
}