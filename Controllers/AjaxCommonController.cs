﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_CrowdBook.Utils;

namespace Web_CrowdBook.Controllers
{
    [Authorize]
    public class AjaxCommonController : Controller
    {
        #region GetInstitutionsByName
        [HttpPost]
        public JsonResult GetInstitutionsByName(string name, int offset = 1)
        {
            ETC.BL.Institutions.IInstitutionBL bl = new ETC.BL.Institutions.InstitutionBL();
            List<ETC.Structures.Models.Institution> result = bl.GetInstitutionsByName(name, offset, Pagination.LIMIT_PAGE_AUTOCOMPLETE);
            return Json(result);
        }
        #endregion
    }
}