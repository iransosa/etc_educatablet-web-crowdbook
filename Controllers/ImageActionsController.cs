﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ETC.Structures.Models;
using Web_CrowdBook.Utils;
using Web_CrowdBook.Models;

namespace Web_CrowdBook.Controllers
{
    [Authorize]
    [Web_CrowdBook.Filters.ValidateAntiForgeryTokenOnAllPosts]
    public class ImageActionsController : Controller
    {
        #region CropAvatarPerson Of Temporary
        [Authorize]
        [HttpPost]
        public JsonResult CropAvatarPerson(int x2, int y2, int x1, int y1, int originalWidth, int originalHeight)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            ETC.BL.Person.IPersonBL bl = new ETC.BL.Person.PersonBL();
            long idPerson = Convert.ToInt64(p.Id);
            bool success = false;
            string error = "";
            try
            {
                string avatarFileName = FileHandler.GetAvatarTemp(idPerson);
                string url = string.Format("{0}/{1}", FileHandler.GetUrlBase(this.HttpContext), avatarFileName);
                System.Drawing.Image image = null;
                image = FileHandler.GetImage(url);
                if (image != null)
                {
                    x1 = (image.Width * x1) / originalWidth;
                    y1 = (image.Height * y1) / originalHeight;
                    x2 = (image.Width * x2) / originalWidth;
                    y2 = (image.Height * y2) / originalHeight;
                    int w = x2 - x1;
                    int h = y2 - y1;
                    image = FileHandler.CropImage(image, w, h, x1, y1);
                    byte[] bytes = FileHandler.ConvertImageToBytes(image);
                    string sha1 = FileHandler.GetSHA1(avatarFileName, bytes.Length);
                    string base64File = Convert.ToBase64String(bytes);
                    bl.SavePersonAvatar(p.Email, base64File, sha1);
                    success = true;
                }
                else
                    error = Messages.IMAGE_NOT_FOUND;
            }
            catch (Exception)
            {
                error = Constants.ERROR_INTERNAL;
            }
            var result = Json(new { success = success, error = error });
            return result;
        }
        #endregion

        #region CropAvatarPerson
        //[Authorize]
        //[HttpPost]
        //public JsonResult CropAvatarPerson(int x2, int y2, int x1, int y1, int originalWidth, int originalHeight)
        //{
        //    PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
        //    ETC.BL.Person.IPersonBL bl = new ETC.BL.Person.PersonBL();
        //    long idPerson = Convert.ToInt64(p.Id);
        //    bool success = false;
        //    string error = "";
        //    try
        //    {
        //        ETC.Structures.Models.Avatar avatar = bl.GetPersonAvatar(Convert.ToInt32(p.Id));
        //        if (avatar != null)
        //        {
        //            System.Drawing.Image image = null;
        //            if (!string.IsNullOrEmpty(avatar.url))
        //            {
        //                //string url = string.Format("{0}/{1}", FileHandler.GetUrlBase(this.HttpContext), avatar.url);
        //                image = FileHandler.GetImage(avatar.url);
        //            }
        //            if (image != null)
        //            {
        //                x1 = (image.Width * x1) / originalWidth;
        //                y1 = (image.Height * y1) / originalHeight;
        //                x2 = (image.Width * x2) / originalWidth;
        //                y2 = (image.Height * y2) / originalHeight;
        //                int w = x2 - x1;
        //                int h = y2 - y1;
        //                image = FileHandler.CropImage(image, w, h, x1, y1);
        //                byte[] bytes = FileHandler.ConvertImageToBytes(image);
        //                string sha1 = FileHandler.GetSHA1(avatar.url, bytes.Length);
        //                string base64File = Convert.ToBase64String(bytes);
        //                bl.SavePersonAvatar(p.Email, base64File, sha1);
        //                success = true;
        //            }
        //            else
        //                error = Messages.IMAGE_NOT_FOUND;
        //        }
        //        else
        //            error = Messages.IMAGE_NOT_FOUND;
        //    }
        //    catch (Exception)
        //    {
        //        error = Constants.ERROR_INTERNAL;
        //    }
        //    var result = Json(new { success = success, error = error });
        //    return result;
        //}
        #endregion

        #region CropImageGroup Of Temporary
        [Authorize]
        [HttpPost]
        public JsonResult CropImageGroup(long group, int x2, int y2, int x1, int y1, int originalWidth, int originalHeight, bool isAvatar)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            ETC.BL.Groups.IGroupBL bl = new ETC.BL.Groups.GroupBL();
            long idPerson = Convert.ToInt64(p.Id);
            bool success = false;
            string error = "";
            ETC.BL.Common.FileType fileType;
            if (isAvatar)
                fileType = ETC.BL.Common.FileType.AVATAR;
            else
                fileType = ETC.BL.Common.FileType.COVER;
            try
            {
                string groupFileName = FileHandler.GetImageTemp(group, fileType);
                string url = string.Format("{0}/{1}", FileHandler.GetUrlBase(this.HttpContext), groupFileName);
                System.Drawing.Image image = null;
                image = FileHandler.GetImage(url);
                if (image != null)
                {
                    x1 = (image.Width * x1) / originalWidth;
                    y1 = (image.Height * y1) / originalHeight;
                    x2 = (image.Width * x2) / originalWidth;
                    y2 = (image.Height * y2) / originalHeight;
                    int w = x2 - x1;
                    int h = y2 - y1;
                    image = FileHandler.CropImage(image, w, h, x1, y1);
                    byte[] bytes = FileHandler.ConvertImageToBytes(image);
                    string sha1 = FileHandler.GetSHA1(groupFileName, bytes.Length);
                    string mimeType = FileHandler.GetMimeType(image);
                    bl.SaveGroupFile(group, bytes, sha1, mimeType, idPerson, fileType);
                    success = true;
                }
                else
                    error = Messages.IMAGE_NOT_FOUND;
            }
            catch (Exception)
            {
                error = Constants.ERROR_INTERNAL;
            }
            var result = Json(new { success = success, id = group, error = error });
            return result;
        }
        #endregion

        #region CropImageGroup
        //[Authorize]
        //[HttpPost]
        //public JsonResult CropImageGroup(long group, int x2, int y2, int x1, int y1, int originalWidth, int originalHeight, bool isAvatar)
        //{
        //    PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
        //    ETC.BL.Groups.IGroupBL bl = new ETC.BL.Groups.GroupBL();
        //    long idPerson = Convert.ToInt64(p.Id);
        //    bool success = false;
        //    string error = "";
        //    ETC.BL.Common.FileType fileType;
        //    if (isAvatar)
        //        fileType = ETC.BL.Common.FileType.AVATAR;
        //    else
        //        fileType = ETC.BL.Common.FileType.COVER;
        //    try
        //    {
        //        ETC.Structures.Models.Groups.GroupFile groupFile = bl.GetGroupFile(group, fileType);
        //        if (groupFile != null)
        //        {
        //            System.Drawing.Image image = null;
        //            if (!string.IsNullOrEmpty(groupFile.Name))
        //            {
        //                string url = string.Format("{0}/{1}", FileHandler.GetUrlBase(this.HttpContext), groupFile.Name);
        //                image = FileHandler.GetImage(url);
        //            }
        //            if (image != null)
        //            {
        //                x1 = (image.Width * x1) / originalWidth;
        //                y1 = (image.Height * y1) / originalHeight;
        //                x2 = (image.Width * x2) / originalWidth;
        //                y2 = (image.Height * y2) / originalHeight;
        //                int w = x2 - x1;
        //                int h = y2 - y1;
        //                image = FileHandler.CropImage(image, w, h, x1, y1);
        //                byte[] bytes = FileHandler.ConvertImageToBytes(image);
        //                string sha1 = FileHandler.GetSHA1(groupFile.Name, bytes.Length);
        //                bl.SaveGroupFile(group, bytes, sha1, groupFile.MimeTpe, idPerson, fileType);
        //                success = true;
        //            }
        //            else
        //                error = Messages.IMAGE_NOT_FOUND;
        //        }
        //        else
        //            error = Messages.IMAGE_NOT_FOUND;
        //    }
        //    catch (Exception)
        //    {
        //        error = Constants.ERROR_INTERNAL;
        //    }
        //    var result = Json(new { success = success, id = group, error = error });
        //    return result;
        //}
        #endregion
    }
}