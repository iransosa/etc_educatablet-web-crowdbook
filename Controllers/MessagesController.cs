﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Web_CrowdBook.Models;
using ETC.Structures.Models;
using Web_CrowdBook.Utils;

namespace Web_CrowdBook.Controllers
{
    public class MessagesController : Controller
    {
        public ActionResult Index()
        {
            BaseModel model = new BaseModel();
            if (Request.IsAuthenticated)
            {
                PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
                model.Person = p;
            }
            if (TempData["GlobalAlertMessage"] != null)
            {
                ViewBag.GlobalAlertMessage = TempData["GlobalAlertMessage"];
                ViewBag.GlobalAlertClass = TempData["GlobalAlertClass"];
            }
            return View(model);
        }
    }
}