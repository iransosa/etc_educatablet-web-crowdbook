﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Web_CrowdBook.Models;
using Web_CrowdBook.Utils;
using ETC.BL.Groups;
using ETC.Structures.Models;
using ETC.Structures.Models.Groups;

namespace Web_CrowdBook.Controllers
{
    public class GroupController : Controller
    {
        IGroupBL bl = new GroupBL();

        #region Index
        public ActionResult Index(long id)
        {
            GroupModels model = new GroupModels();
            PersonCrowdBook p = null;
            long idPerson = 0;
            Group group = bl.GetGroupById(id);
            if (group == null)
            {
                TempData["GlobalAlertMessage"] = Messages.GROUP_NOT_FOUND;
                TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_INFO"];
                return RedirectToAction("Index", "Messages");
            }
            model.Group = group;
            //If user can not access to Group, show de Preview view
            bool hasPermissionToSee = false;
            if (Request.IsAuthenticated)
            {
                p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
                model.Person = p;
                idPerson = Convert.ToInt64(p.Id);
                //Valid Permission
                model.IdPerson = idPerson;
                model.IsAdmin = bl.IsAdminGroup(id, idPerson);
                List<GroupPermissions> permissions = bl.GetPermissionsMemberGroup(id, idPerson);
                if (permissions != null && permissions.Count > 0)
                {
                    model.Editable = permissions.Find(x => x.IdGroupPermission == (long)ETC.BL.Common.GroupPermission.EDIT_GROUP) != null ? true : false;
                    model.SendMessage = permissions.Find(x => x.IdGroupPermission == (long)ETC.BL.Common.GroupPermission.SEND_MESSAGE) != null ? true : false;
                    model.PublishResource = permissions.Find(x => x.IdGroupPermission == (long)ETC.BL.Common.GroupPermission.PUBLISH_RESOURCE) != null ? true : false;
                    model.DeleteResource = permissions.Find(x => x.IdGroupPermission == (long)ETC.BL.Common.GroupPermission.DELETE_RESOURCE) != null ? true : false;
                }
                //Check is member group
                GroupMember member = bl.GetMemberGroup(id, idPerson);
                bool isMember = member != null && member.Status == ETC.BL.Common.GroupMemberStates.ACTIVE.ToString() ? true : false;
                if (!isMember)
                {
                    if (group.GroupMode.Key == ETC.BL.Common.GroupModeTypes.PUBLIC.ToString())
                        hasPermissionToSee = true;
                    else if (group.GroupMode.Key == ETC.BL.Common.GroupModeTypes.PRIVATE_VISIBLE.ToString())
                        hasPermissionToSee = false;
                    else if (group.GroupMode.Key == ETC.BL.Common.GroupModeTypes.PRIVATE_HIDDEN.ToString())
                    {
                        if (member != null && member.Status == ETC.BL.Common.GroupMemberStates.PENDING.ToString())
                            TempData["GlobalAlertMessage"] = Messages.REQUEST_MEMBER_PENDING;
                        else
                            TempData["GlobalAlertMessage"] = Messages.GROUP_PRIVATED;
                        TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_INFO"];
                        return RedirectToAction("Index", "Messages");
                    }
                }
                else
                {
                    hasPermissionToSee = true;
                    model.IsMember = true;
                    if (member.Role.Id == (long)ETC.BL.Common.RoleType.GROUP_OWNER)
                        model.IsOnwer = true;
                }
                if (member != null && member.Status == ETC.BL.Common.GroupMemberStates.PENDING.ToString())
                    model.MemberRequest = true;
            }
            if (hasPermissionToSee)
            {
                //Resources
                model.Products = bl.GetGroupProducts(id, idPerson, Constants.BRAND_ID, 1, Pagination.GROUP_RESOURCE_SUMMARY);
            }
            SetMetaTags(model.Group.IdGroup, model.Group.Name, model.Group.Description);
            if (TempData["GlobalAlertMessage"] != null)
            {
                ViewBag.GlobalAlertMessage = TempData["GlobalAlertMessage"];
                ViewBag.GlobalAlertClass = TempData["GlobalAlertClass"];
            }
            if (Request.IsAuthenticated && hasPermissionToSee)
                return View(model);
            else
                return View("Preview", model);
        }
        #endregion

        #region Preview

        #endregion

        #region SetMetaTags
        void SetMetaTags(long idGroup, string title, string description)
        {
            title = string.Format("{0} en CrowdBook", title);
            ViewBag.Title = title;
            string urlCanonical = "";
            if (Request.QueryString.Count > 0)
                urlCanonical = Request.Url.ToString().Split('?')[0];
            else
                urlCanonical = Request.Url.ToString();
            ViewBag.Meta_Title = title;
            ViewBag.Meta_Description = description + ". CrowdBook, Estudia a través de tu Red";
            ViewBag.Meta_Url = urlCanonical;
            ViewBag.Meta_UrlCanonical = urlCanonical;
            ViewBag.Meta_Image = Url.Action("Group", "Image", new { id = idGroup, width = 114, height = 114 }, this.Request.Url.Scheme);
        }
        #endregion
    }
}