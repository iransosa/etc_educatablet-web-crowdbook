﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ETC.BL.Groups;
using ETC.Structures.Models;
using Web_CrowdBook.Utils;
using ETC.Structures.Models.Groups;
using Web_CrowdBook.Models;

namespace Web_CrowdBook.Controllers
{
    public class LibraryController : Controller
    {
        IGroupBL bl = new GroupBL();

        #region GetLibrary
        public ActionResult Index(int page = 1)
        {
            LibraryModel model = new LibraryModel();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Person = p;
            model.Products.Products = GetPersonProducts(Convert.ToInt64(p.Id), page);
            model.Page = page;
            if (TempData["GlobalAlertMessage"] != null)
            {
                ViewBag.GlobalAlertMessage = TempData["GlobalAlertMessage"];
                ViewBag.GlobalAlertClass = TempData["GlobalAlertClass"];
            }
            return View(model);
        }
        List<GroupProduct> GetPersonProducts(long idPerson, int page)
        {
            List<GroupProduct> products = new List<GroupProduct>();
            products = bl.GetPersonProducts(idPerson, page, Pagination.LIMIT_PAGE);
            return products;
        }
        #endregion

        #region Search
        public ActionResult SearchProducts(string search)
        {
            if (string.IsNullOrEmpty(search))
                return RedirectToAction("Index", "Library");
            else
                return RedirectToAction("Search", "Library", new { search = search });
        }
        #endregion

        #region Groups
        public ActionResult Search(string search, int page = 1)
        {
            LibraryModel model = new LibraryModel();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Person = p;
            model.Products.Products = SearchProducts(search, Convert.ToInt64(p.Id), page);
            model.Search = search;
            model.Page = page;
            model.IsSearch = true;
            return View("Index", model);
        }
        List<GroupProduct> SearchProducts(string search, long idPerson, int page)
        {
            List<GroupProduct> products = new List<GroupProduct>();
            products = bl.SearchPersonProducts(idPerson, search, page, Pagination.LIMIT_PAGE);
            return products;
        }
        #endregion
    }
}