﻿using ETC.BL;
using ETC.Structures.Models;
using ETC.Structures.Models.Groups;
using ETC.Structures.Models.Products;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web_CrowdBook.Utils;

namespace Web_CrowdBook.Controllers
{
    public class DownloadsController : Controller
    {
        #region Actions
        public FileResult P(long id)
        {
            Web_CrowdBook.Models.BaseModel m = new Models.BaseModel();  
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            //Validate if product exist and don't have DRM
            GroupProduct product = GetGroupProduct(id, idPerson);
            if (product == null)
                throw new HttpException(404, "Product not found");
            if (product.HasDRM)
                throw new HttpException(404, "Product with DRM");
            //Get Url of a File
            string url = GetUrlFileProduct(this.ControllerContext, id, idPerson);
            if (string.IsNullOrEmpty(url))
                throw new HttpException(404, "File not found");
            byte[] fileBytes = GetFile(url);
            if (fileBytes != null)
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, System.IO.Path.GetFileName(url));
            else
                throw new HttpException(404, "File not exist");
        }
        #endregion

        #region GetProduct
        string GetUrlFileProduct(ControllerContext context, long idProduct, long idPerson)
        {
            IProductBL bl = new ProductBL();
            ProductFile productFile = bl.GetProductFileByProductAndPerson(idProduct, idPerson);
            string pathFile = "";
            if (productFile != null)
                pathFile = string.Format("{0}/{1}", GetUrlBase(context), productFile.Name);
            return pathFile;
        }
        GroupProduct GetGroupProduct(long idProduct, long idPerson)
        {
            ETC.BL.Groups.IGroupBL bl = new ETC.BL.Groups.GroupBL();
            GroupProduct p = bl.GetPersonProduct(idProduct, idPerson);
            return p;
        }
        #endregion

        #region GetUrlBase
        string GetUrlBase(ControllerContext context)
        {
            string url = "";
            if (context.HttpContext.Cache["GroupImageUrlBase"] == null)
            {
                ETC.BL.ProductBL bl = new ETC.BL.ProductBL();
                ETC.Structures.Models.Products.UrlDouwnload urlDown = bl.GetUrlDownload();
                url = urlDown.urlD;
                context.HttpContext.Cache["GroupImageUrlBase"] = url;
            }
            else
                url = context.HttpContext.Cache["GroupImageUrlBase"].ToString();
            return url;
        }
        #endregion

        #region GetFile
        byte[] GetFile(string url)
        {
            byte[] file = null;
            try
            {
                using (System.Net.WebClient webClient = new System.Net.WebClient())
                {
                    using (MemoryStream ms = new MemoryStream(webClient.DownloadData(url)))
                    {
                        file = ms.ToArray();
                    }
                }
                return file;

            }
            catch (Exception)
            {
            }
            return file;
        }
        #endregion
    }

    #region FileType
    public enum FileType
    {
        GROUP_PRODUCT
    }
    #endregion

    //public class WebRequestFileResult : FileResult
    //{
    //    private const int BufferSize = 32768; // 32 KB
    //    private const string DispositionHeader = "Content-Disposition";
    //    private const string LengthHeader = "Content-Length";
    //    private readonly string _url;

    //    public WebRequestFileResult(string url) : base("application/octet-stream")
    //    {
    //        if (String.IsNullOrWhiteSpace(url))
    //            throw new ArgumentNullException("url", "Url is required");
    //        _url = url;
    //    }
    //    protected override void WriteFile(HttpResponseBase localResponse)
    //    {
    //        var webRequest = WebRequest.Create(_url);
    //        using (var remoteResponse = webRequest.GetResponse())
    //        using (var remoteStream = remoteResponse.GetResponseStream())
    //        {
    //            if (remoteStream == null)
    //                throw new NullReferenceException("Request returned null stream: " + _url);
    //            var dispositionKey = remoteResponse.Headers.AllKeys.FirstOrDefault(
    //                k => k.Equals(DispositionHeader, StringComparison.InvariantCultureIgnoreCase));
    //            if (!String.IsNullOrWhiteSpace(dispositionKey))
    //                localResponse.AddHeader(DispositionHeader, remoteResponse.Headers[DispositionHeader]);
    //            else
    //                localResponse.AddHeader(DispositionHeader, "Hola");
    //            var contentLenthString = remoteResponse.ContentLength.ToString(CultureInfo.InvariantCulture);
    //            localResponse.ContentType = remoteResponse.ContentType;
    //            localResponse.AddHeader(LengthHeader, contentLenthString);
           
    //            var buffer = new byte[BufferSize];
    //            var loopCount = 0;
    //            while (true)
    //            {
    //                if (!localResponse.IsClientConnected)
    //                    break;
    //                var read = remoteStream.Read(buffer, 0, BufferSize);
    //                if (read <= 0)
    //                    break;
    //                localResponse.OutputStream.Write(buffer, 0, read);
    //                // Flush after 1 MB; note that this
    //                // will prevent server side caching
    //                loopCount++;
    //                if (loopCount % 32 == 0)
    //                    localResponse.Flush();
    //            }
    //        }
    //    }
    //}    
}