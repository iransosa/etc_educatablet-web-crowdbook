﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ETC.Structures.Models;
using Web_CrowdBook.Utils;
using ETC.BL.Groups;
using ETC.Structures.Models.Groups;

namespace Web_CrowdBook.Controllers
{
    [Authorize]
    [Web_CrowdBook.Filters.ValidateAntiForgeryTokenOnAllPosts]
    public class AjaxLibraryController : Controller
    {
        IGroupBL bl = new GroupBL();

        #region GetProducts
        [HttpPost]
        public JsonResult GetProducts(int page)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            bool sucess = false;
            List<GroupProduct> groups = null;
            try
            {
                groups = GetPersonProducts(Convert.ToInt64(p.Id), page);
            }
            catch (Exception) { }
            var result = new { sucess = sucess, result = groups, page = page + 1 };
            return Json(result);
        }
        List<GroupProduct> GetPersonProducts(long idPerson, int page)
        {
            List<GroupProduct> products = new List<GroupProduct>();
            products = bl.GetPersonProducts(idPerson, page, Pagination.LIMIT_PAGE);
            return products;
        }
        #endregion

        #region Search
        [HttpPost]
        public JsonResult SearchProducts(string search = "", int page = 0)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            bool sucess = false;
            List<GroupProduct> groups = null;
            try
            {
                groups = SearchProducts(search, Convert.ToInt64(p.Id), page);
            }
            catch (Exception) { }
            var result = new { sucess = sucess, result = groups, page = page + 1, search = search };
            return Json(result);
        }
        List<GroupProduct> SearchProducts(string search, long idPerson, int page)
        {
            List<GroupProduct> products = new List<GroupProduct>();
            products = bl.SearchPersonProducts(idPerson, search, page, Pagination.LIMIT_PAGE);
            return products;
        }
        #endregion
    }
}
