﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Web_CrowdBook.Models;
using ETC.Structures.Models;
using ETC.BL.Person;
using Web_CrowdBook.Utils;

namespace Web_CrowdBook.Controllers
{
    public class AccountController : Controller
    {
        #region Index
        public ActionResult Index(string returnUrl = "")
        {
            if (Request.IsAuthenticated)
                return GoToHomePage();
            AccountModel model = new AccountModel();
            model.EncriptionKey = UtilsContext.GenerateEncriptonKey(HttpContext);
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        } 
        #endregion

        #region LogIn
        public ActionResult LogIn(string returnUrl = "")
        {
            if (Request.IsAuthenticated)
                return GoToHomePage();
            ViewBag.ReturnUrl = returnUrl;
            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogIn(LoginModel login, string returnUrl = "")
        {
            if (Request.IsAuthenticated)
                return GoToHomePage();
            if (ModelState["login.LoginEmail"].Errors.Count == 0 && ModelState["login.LoginPassword"].Errors.Count == 0)
            {
                if (Utils.Utils.IsBase64String(login.LoginPassword))
                {
                    try
                    {
                        login.LoginPassword = UtilsContext.DecryptValue(login.LoginPassword, HttpContext);
                    }
                    catch (Exception) { }
                }
                if (!System.Text.RegularExpressions.Regex.IsMatch(login.LoginPassword, Utils.Constants.REGEX_PASSWORD))
                    ModelState["login.LoginPassword"].Errors.Add(Utils.Constants.MESSAGE_ERROR_PASSWORD);
            }
            if (ModelState["login.LoginEmail"].Errors.Count == 0 && ModelState["login.LoginPassword"].Errors.Count == 0)
            {
                ETC.BL.Users.UserBL blUser = new ETC.BL.Users.UserBL();
                string password = ETC.BL.Utilities.Utils.Encrypt(login.LoginPassword);
                if (blUser.Login(login.LoginEmail, password))
                {
                    PersonBL blPerson = new PersonBL();
                    PersonInformation personInfo = blPerson.GetPersonInformation(login.LoginEmail, Utils.Constants.BRAND, "");
                    AuthenticatePerson(personInfo, login.LoginEmail);
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        if (Url.IsLocalUrl(returnUrl))
                            return Redirect(returnUrl);
                    }
                    return RedirectToAction("Index", "MyGroups");
                }
            }
            List<string> errorList = new List<string>();
            if (string.IsNullOrEmpty(login.LoginEmail) || string.IsNullOrEmpty(login.LoginPassword))
                errorList.Add(Utils.Messages.EMAIL_OR_PASSWORD_MISSING);
            else
                errorList.Add(Utils.Messages.LOGIN_INVALID);
            ViewBag.ErrorsLogin = errorList;
            AccountModel model = new AccountModel();
            model.Login = login;
            model.EncriptionKey = UtilsContext.GenerateEncriptonKey(HttpContext);
            return View("Index", model);
        } 
        #endregion

        #region LogOut
        [Authorize]
        public ActionResult LogOut()
        {
            Session.RemoveAll();
            Session.Abandon();
            Session.Clear();
            FormsAuthentication.SignOut();
            return LogIn();
        } 
        #endregion

        #region Register
        public ActionResult Register()
        {
            if (Request.IsAuthenticated)
                return GoToHomePage();

            return RedirectToAction("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel Register)//Keep mayuscula Register para metener compatibilidad con javascript
        {
            if (Request.IsAuthenticated)
                return GoToHomePage();

            if (ModelState.IsValid)
            {
                try
                {
                    Register.Password = UtilsContext.DecryptValue(Register.Password, HttpContext);
                    if (!System.Text.RegularExpressions.Regex.IsMatch(Register.Password, Utils.Constants.REGEX_PASSWORD))
                        ModelState["Register.Password"].Errors.Add(Utils.Constants.MESSAGE_ERROR_PASSWORD);
                }
                catch (Exception)
                {
                    ModelState["Register.Password"].Errors.Add(Utils.Constants.MESSAGE_ERROR_PASSWORD_FORMAT);
                }
            }
            List<string> errorList = new List<string>();
            List<KeyValuePair<string, string>> listsErrors = new List<KeyValuePair<string, string>>();
            if (ModelState.IsValid)
            {
                PersonBL blPerson = new PersonBL();
                ETC.BL.Users.UserBL blUser = new ETC.BL.Users.UserBL();
                if (!blUser.ValidateExistUserByBrand(Register.Email, Utils.Constants.BRAND))
                {
                    ETC.Structures.Models.Users.UserRegisterETC userEtc = new ETC.Structures.Models.Users.UserRegisterETC();
                    userEtc.email = Register.Email;
                    userEtc.firstName = Register.FirstName;
                    userEtc.lastName = Register.LastName;
                    userEtc.password = ETC.BL.Utilities.Utils.Encrypt(Register.Password);
                    userEtc.auth = new ETC.Structures.Models.Users.User();
                    userEtc.auth.clientId = Utils.Constants.BRAND;
                    userEtc.phone = new UserPhoneETC();

                    if (blUser.RegisterUserETC(userEtc))
                    {
                        PersonInformation personInfo = blPerson.GetPersonInformation(Register.Email, Utils.Constants.BRAND, "");
                        AuthenticatePerson(personInfo, Register.Email);
                        return RedirectToAction("Index", "MyGroups");
                    }
                    else
                        errorList.Add(Utils.Messages.INTERNAL_ERROR);
                    //ModelState.AddModelError("Error", Utils.Messages.INTERNAL_ERROR);
                }
                else
                {
                    //errorList.Add(Utils.Messages.EMAIL_EXIST);
                    KeyValuePair<string, string> i = new KeyValuePair<string, string>("Register.Email", Utils.Messages.EMAIL_EXIST);
                    listsErrors.Add(i);
                    //ModelState.AddModelError("EmailExist", Utils.Messages.EMAIL_EXIST);
                }
            }
            else
            {
                foreach (var modelError in ModelState)
                {
                    foreach (var item2 in modelError.Value.Errors)
	                {
                        KeyValuePair<string, string> i = new KeyValuePair<string, string>(modelError.Key, item2.ErrorMessage);
                        listsErrors.Add(i);
	                }
                }
                //errorList = (from item in ModelState.Values
                //             from error in item.Errors
                //             select error.ErrorMessage).ToList();
            }
            if(errorList.Count > 0)
                ViewBag.ErrorsRegister = errorList;
            if (listsErrors.Count > 0)
                ViewBag.ErrorsRegisterList = listsErrors;
            ModelState.Clear();
            AccountModel model = new AccountModel();
            model.Register = Register;
            model.EncriptionKey = UtilsContext.GenerateEncriptonKey(HttpContext);
            return View("Index", model);
        } 
        #endregion

        #region Policy
        public ActionResult Policy()
        {
            return View();
        } 
        #endregion

        #region AuthenticatePerson
        void AuthenticatePerson(PersonInformation personEt, string userName)
        {
            PersonCrowdBook person = Utils.Utils.ConvertPersonEtToPersonSession(personEt, userName);
            string personJson = person.getJson();
            FormsAuthentication.SetAuthCookie(personJson, false);
            Session.Add(Sessions.PERSON_SESSION, person);
            UtilsContext.RemoveEncriptonKey(HttpContext);
            //Response.SetAuthCookie(personJson, true, s);
        } 
        #endregion

        #region GoToHomePage
        ActionResult GoToHomePage()
        {
            return RedirectToAction("Index", "MyGroups");
        } 
        #endregion
    }
}