﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ETC.Structures.Models;
using Web_CrowdBook.Utils;
using ETC.Structures.Models.Groups;
using ETC.BL.Groups;
using Web_CrowdBook.Helpers;
using Web_CrowdBook.ViewModels;

namespace Web_CrowdBook.Controllers
{
    [Authorize]
    [Web_CrowdBook.Filters.ValidateAntiForgeryTokenOnAllPosts]
    public class AjaxGroupController : Controller
    {
        IGroupBL bl = new GroupBL();

        #region GetMyGroups
        [HttpPost]
        public JsonResult GetMyGroups(int itemsCurrent)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            bool sucess = false;
            List<Group> groups = null;
            try
            {
                groups = GetMyGroups(Convert.ToInt64(p.Id), itemsCurrent);
            }
            catch (Exception) { }
            var result = new { sucess = sucess, result = groups, page = itemsCurrent + 1 };
            return Json(result);
        }
        List<Group> GetMyGroups(long idPerson, int itemsCurrent)
        {
            List<Group> groups = new List<Group>();
            groups = bl.GetMyGroups(idPerson, itemsCurrent + 1, itemsCurrent + Pagination.LIMIT_PAGE);
            return groups;
        } 
        #endregion

        #region SearchGroups
        [HttpPost]
        public JsonResult SearchGroups(string search = "", int page = 0)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            bool sucess = false;
            List<Group> groups = null;
            try
            {
                groups = SearchGroups(search, Convert.ToInt64(p.Id), page);
            }
            catch (Exception) { }
            var result = new { sucess = sucess, result = groups, page = page + 1 };
            return Json(result);
        }
        List<Group> SearchGroups(string search, long idPerson, int page)
        {
            List<Group> groups = new List<Group>();
            groups = bl.SearchGroups(search, idPerson, page, Pagination.LIMIT_PAGE);
            return groups;
        } 
        #endregion

        #region CreateGroup
        [HttpPost]
        public JsonResult CreateGroup(GroupVM model)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            bool success = false;
            string error = "";
            Group group = null;
            long idGroup = 0;
            int groupsCount = 0;
            List<string> listsErrors=null;
            if (ModelState.IsValid)
            {
                try
                {
                    Group g = Mappers.GroupVMToGroup(model);
                    //if (g.Institution != null)
                    //{
                    //    if (!string.IsNullOrEmpty(model.Group.Institution))
                    //    {
                    //        ETC.BL.Institutions.IInstitutionBL blInstitution = new ETC.BL.Institutions.InstitutionBL();
                    //        List<Institution> i = blInstitution.GetInstitutionsByName(model.Group.Institution, 1, 1);
                    //        if (i.Count > 0)
                    //            g.Institution = new Institution() { Id = i[0].Id };
                    //    }
                    //}
                    idGroup = bl.SaveGroup(g, (long)p.Id);
                    if (idGroup != 0)
                    {
                        success = true;
                        group = bl.GetGroupById(idGroup);
                        groupsCount = bl.CountGroupPerson((long)p.Id);
                        UtilsContext.CleanCookieSession(this.HttpContext);
                    }
                    else
                        error = Constants.ERROR_INTERNAL;
                }
                catch (Exception)
                {
                    error = Constants.ERROR_INTERNAL;
                }
            }
            else
            {
                listsErrors = Utils.Utils.GetErrorsModelStateList(ModelState);
            }
            //string pin = group != null ? group.Pin : "";
            //string name = group != null ? group.Name : "";
            //var result = new { success = success, id = idGroup, group = group, pin = pin, name = name, error = error, errors = listsErrors };
            var result = new { success = success, id = idGroup, group = group, groupsCount = groupsCount, error = error, errors = listsErrors };
            return Json(result);
        }
        #endregion

        #region EditGroup
        [HttpPost]
        public JsonResult EditGroup(GroupVM model)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            bool success = false;
            string message = "", error = "";
            Group group = null;
            long idGroup = 0;
            int groupsCount = 0;
            List<string> listsErrors = null;
            if (model.IdGroup != 0)
            {
                if (ModelState.IsValid)
                {
                    try
                    {
                        Group g = Mappers.GroupVMToGroup(model);
                        g.Institution = null;
                        g.Level = null;
                        if (model.IdInstitution != 0 && !string.IsNullOrEmpty(model.Institution))
                        {
                            int idInstitution = Convert.ToInt32(model.IdInstitution);
                            ETC.BL.Institutions.IInstitutionBL blInstitution = new ETC.BL.Institutions.InstitutionBL();
                            Institution i = blInstitution.GetInstitutionById(idInstitution);
                            if (i != null)
                            {
                                if (i.Name == model.Institution)
                                {
                                    g.Institution = new Institution() { Id = idInstitution };
                                    if (model.IdLevel != 0)
                                    {
                                        g.Level = new ETC.Structures.Models.Institutions.Level();
                                        g.Level.IDLevel = Convert.ToInt64(model.IdLevel);
                                    }
                                }
                            }
                        }
                        idGroup = bl.SaveGroup(g, (long)p.Id);
                        if (idGroup != 0)
                        {
                            success = true;
                            group = bl.GetGroupById(idGroup);
                            groupsCount = bl.CountGroupPerson((long)p.Id);
                            message = Messages.GROUP_CREATED;
                            UtilsContext.CleanCookieSession(this.HttpContext);
                        }
                        else
                            error = Constants.ERROR_INTERNAL;
                    }
                    catch (Exception)
                    {
                        error = Constants.ERROR_INTERNAL;
                    }
                }
                else
                {
                    listsErrors = Utils.Utils.GetErrorsModelStateList(ModelState);
                }
            }
            else
                error = "No se encontró el Grupo a modificar";
            var result = new { success = success, id = idGroup, group = group, groupsCount = groupsCount, message = message, error = error, errors = listsErrors };
            return Json(result);
        }
        #endregion
    }
}