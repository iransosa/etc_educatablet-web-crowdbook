﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ETC.Structures.Models;
using ETC.BL.Person;
using Web_CrowdBook.Models;
using Web_CrowdBook.Utils;
using ETC.BL.Gender;
using Web_CrowdBook.Helpers;
using System.Configuration;

namespace Web_CrowdBook.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        #region Profile
        public ActionResult Index()
        {
            ProfileModel model = new ProfileModel();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Person = p;
            PersonBL blPerson = new PersonBL();
            PersonInformation personInfo = blPerson.GetPersonInformation(p.Email, Utils.Constants.BRAND, "");
            if (personInfo == null)
                return RedirectToAction("NotFound", "Error");
            GenderBL blUser = new GenderBL();
            model.FirstName = personInfo.firstname;
            model.LastName = personInfo.lastname;
            model.GenderAbbreviature = personInfo.genderAbbreviature;
            model.GenderList = DataAccess.GetGenders();
            if (TempData["GlobalAlertMessage"] != null)
            {
                ViewBag.GlobalAlertMessage = TempData["GlobalAlertMessage"];
                ViewBag.GlobalAlertClass = TempData["GlobalAlertClass"];
            }
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ProfileModel model)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            if (ModelState.IsValid)
            {
                ETC.BL.Users.UserBL blUser = new ETC.BL.Users.UserBL();
                Person person = new Person();
                person.Email = p.Email;
                person.FirstName = model.FirstName;
                person.LastName = model.LastName;
                person.GenderAbbreviature = model.GenderAbbreviature;
                person.auth = new ETC.Structures.Models.Users.User();
                person.auth.clientId = Utils.Constants.BRAND;
                blUser.UpdatePersonalInfo(person);
                //long idInstitution = model.IdInstitution != null ? (long)model.IdInstitution : 0;
                //long idLevel = model.IdLevel != null ? (long)model.IdLevel : 0;
                //blUser.UpdateAcademicInfo(person, idInstitution, idLevel);
            }
            else
            {
                model.Person = p;
                model.GenderList = DataAccess.GetGenders();
                return View(model);
            }
            TempData["GlobalAlertMessage"] = Messages.PROFILE_UPDATED;
            TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_SUCCESS"];
            UtilsContext.CleanCookieSession(this.HttpContext);
            return RedirectToAction("Index", "Profile");
        }
        #endregion

        #region ChangePassword
        public ActionResult ChangePassword()
        {
            ChangePasswordModel model = new ChangePasswordModel();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            model.Person = p;
            model.EncriptionKey = UtilsContext.GenerateEncriptonKey(HttpContext);
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            if (ModelState.IsValid)
            {
                try
                {
                    model.CurrentPassword = UtilsContext.DecryptValue(model.CurrentPassword, HttpContext);
                    if (!System.Text.RegularExpressions.Regex.IsMatch(model.CurrentPassword, Utils.Constants.REGEX_PASSWORD))
                        ModelState["CurrentPassword"].Errors.Add(Utils.Constants.MESSAGE_CURRENT_PASSWORD_INVALID);
                    model.Password = UtilsContext.DecryptValue(model.Password, HttpContext);
                    if (!System.Text.RegularExpressions.Regex.IsMatch(model.Password, Utils.Constants.REGEX_PASSWORD))
                        ModelState["Password"].Errors.Add(Utils.Constants.MESSAGE_ERROR_PASSWORD);
                }
                catch (Exception)
                {
                    ModelState["Password"].Errors.Add(Utils.Constants.MESSAGE_ERROR_PASSWORD_FORMAT);
                }
            }
            if (ModelState.IsValid)
            {
                ETC.BL.Users.UserBL blUser = new ETC.BL.Users.UserBL();
                string currentPassword = ETC.BL.Utilities.Utils.Encrypt(model.CurrentPassword);
                string password = ETC.BL.Utilities.Utils.Encrypt(model.Password);
                if (blUser.ValidateUser(p.Email, currentPassword))
                {
                    if (blUser.EditPassword(currentPassword, password, p.Email, Utils.Constants.BRAND))
                    {
                        TempData["GlobalAlertMessage"] = Messages.PASSWORD_CHANGED;
                        TempData["GlobalAlertClass"] = ConfigurationManager.AppSettings["CLASS_ALERT_SUCCESS"];
                        return RedirectToAction("Index", "Profile");
                    }
                    else
                        return RedirectToAction("Index", "Error");
                }
                else
                    ModelState.AddModelError("CurrentPassword", Utils.Constants.MESSAGE_CURRENT_PASSWORD_INVALID);
            }
            model.Person = p;
            model.EncriptionKey = UtilsContext.GenerateEncriptonKey(HttpContext);
            return View(model);
        }
        #endregion

        #region SaveAvatarPersonJson
        [HttpPost]
        [ValidateAntiForgeryToken()]
        public WrappedJsonResult SaveAvatarPersonJson(ImageUploadModel model)
        {
            ETC.BL.Person.IPersonBL bl = new ETC.BL.Person.PersonBL();
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            if (ModelState.IsValid && model.ImageFile != null)
            {
                try
                {
                    string sha1 = "", mimetype = "";
                    HttpPostedFileBase file = model.ImageFile;
                    byte[] bytes = FileHandler.GetFileFromStream(file, ref sha1);
                    string base64File = Convert.ToBase64String(bytes);
                    if (FileHandler.ValidateImageExtension(bytes, out mimetype))
                    {
                        bl.SavePersonAvatarTemp(p.Email, base64File, sha1);
                        //bl.SavePersonAvatar(p.Email, base64File, sha1);
                        return new WrappedJsonResult
                        {
                            Data = new { IsValid = true, Message = Messages.AVATAR_CREATED, ImagePath = string.Empty }
                        };
                    }
                    else
                    {
                        return new WrappedJsonResult
                        {
                            Data = new { IsValid = false, Message = string.Format(FileHandler.FILES_VALID, FileHandler.GetAllowedImageExtensions()), ImagePath = string.Empty }
                        };
                    }
                }
                catch (Exception)
                {
                    return new WrappedJsonResult
                    {
                        Data = new { IsValid = false, Message = Utils.Messages.INTERNAL_ERROR, ImagePath = string.Empty }
                    };
                }
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                if (model.ImageFile == null)
                    sb.Append(string.Format(FileHandler.FILES_VALID, FileHandler.GetAllowedImageExtensions()));
                else
                {
                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            sb.Append(error.ErrorMessage);
                            sb.Append("<br/>");
                        }
                    }
                }
                return new WrappedJsonResult
                {
                    Data = new { IsValid = false, Message = sb.ToString(), ImagePath = string.Empty }
                };
            }
        }
        #endregion
    }
}