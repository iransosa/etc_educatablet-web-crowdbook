﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using Web_CrowdBook.Utils;
using ETC.BL.Utilities;
using System.IO;
using System.Drawing.Imaging;

namespace Web_CrowdBook.Controllers
{
    public class ImageController : Controller
    {
        #region Actions
        public ActionResult Group(long id, int width = 0, int height = 0)
        {
            return new ImageResult(id, ImageType.GROUP_AVATAR, width, height);
        }
        public ActionResult GroupCover(long id, int width = 0, int height = 0)
        {
            return new ImageResult(id, ImageType.GROUP_COVER, width, height);
        }
        public ActionResult Product(long id, int width = 0, int height = 0)
        {
            return new ImageResult(id, ImageType.PRODUCT, width, height);
        }
        public ActionResult Person(long id, int width = 0, int height = 0)
        {
            return new ImageResult(id, ImageType.PERSON, width, height);
        }
        public ActionResult PersonTemp(long id, int width = 0, int height = 0)
        {
            return new ImageResult(id, ImageType.PERSON_TEMP, width, height);
        }

        public ActionResult GroupTemp(long id, int width = 0, int height = 0)
        {
            return new ImageResult(id, ImageType.GROUP_AVATAR_TEMP, width, height);
        }
        public ActionResult GroupCoverTemp(long id, int width = 0, int height = 0)
        {
            return new ImageResult(id, ImageType.GROUP_COVER_TEMP, width, height);
        }
        #endregion
    }

    #region ImageType
    public enum ImageType
    {
        GROUP_AVATAR,
        GROUP_COVER,
        PRODUCT,
        PERSON,
        PERSON_TEMP,
        GROUP_AVATAR_TEMP,
        GROUP_COVER_TEMP
    }
    #endregion

    public class ImageResult : ViewResult
    {
        #region Constructor
        public ImageResult(long id, ImageType type, int width = 0, int height = 0)
        {
            Id = id;
            Type = type;
            if (width > 0 || height > 0)
            {
                Width = width;
                Height = height;
            }
        }
        #endregion

        public long Id { get; set; }
        public ImageType Type { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            Image img = null;
            string imageName = "";
            string imageType = "";
            string resourceExtension = "";
            string url = "";

            switch (Type)
            {
                case ImageType.GROUP_AVATAR:
                    GetImageGroup(context, ETC.BL.Common.FileType.AVATAR, ref img, ref imageName, ref imageType, ref url);
                    break;
                case ImageType.GROUP_COVER:
                    GetImageGroup(context, ETC.BL.Common.FileType.COVER, ref img, ref imageName, ref imageType, ref url);
                    break;
                case ImageType.PRODUCT:
                    GetImageProduct(context, ref img, ref imageName, ref imageType, ref url, ref resourceExtension);
                    break;
                case ImageType.PERSON:
                    GetAvatarPerson(ref img, ref imageName, ref imageType, ref url);
                    break;
                case ImageType.PERSON_TEMP:
                    GetAvatarPersonTemp(context, ref img, ref imageName, ref imageType, ref url);
                    break;

                case ImageType.GROUP_AVATAR_TEMP:
                    GetImageGroupTemp(context, ETC.BL.Common.FileType.AVATAR, ref img, ref imageName, ref imageType, ref url);
                    break;
                case ImageType.GROUP_COVER_TEMP:
                    GetImageGroupTemp(context, ETC.BL.Common.FileType.COVER, ref img, ref imageName, ref imageType, ref url);
                    break;
            }

            context.HttpContext.Response.Clear();
            context.HttpContext.Response.AddHeader("content-disposition", string.Format("filename={0}", "CrowdBook"));
            //context.HttpContext.Response.ContentType = imageType;

            if (img != null)
            {
                if (Width == 0 || Height == 0)
                    context.HttpContext.Response.BinaryWrite(FileHandler.ConvertImageToBytes(img));
                else
                    context.HttpContext.Response.BinaryWrite(ResizeImage(img, Width, Height));
            }
            else
            {
                switch (Type)
                {
                    case ImageType.GROUP_AVATAR:
                    case ImageType.GROUP_AVATAR_TEMP:
                        img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/group.png"));
                        break;
                    case ImageType.GROUP_COVER:
                        img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/cover.png"));
                        Width = 990;
                        break;
                    case ImageType.PRODUCT:
                        switch (resourceExtension)
	                    {
                            case ExtensionFilesType.PDF:
                                img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/Covers/pdf.jpg"));
                                break;
                            case ExtensionFilesType.EPUB:
                                img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/Covers/epub.jpg"));
                                break;
                            case ExtensionFilesType.WORD_2003:
                            case ExtensionFilesType.WORD:
                                img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/Covers/word.jpg"));
                                break;
                            case ExtensionFilesType.EXCEL_2003:
                            case ExtensionFilesType.EXCEL:
                                img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/Covers/excel.jpg"));
                                break;
                            case ExtensionFilesType.POWER_POINT:
                            case ExtensionFilesType.POWER_POINT_2003:
                                img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/Covers/power point.jpg"));
                                break;
                            case ExtensionFilesType.IMAGE_JPG_A:
                            case ExtensionFilesType.IMAGE_JPG_B:
                            case ExtensionFilesType.IMAGE_PNG:
                            case ExtensionFilesType.IMAGE_GIF:
                                img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/Covers/resource.jpg"));
                                break;
                            default:
                                img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/Covers/resource.jpg"));
                                break;
	                    }
                        break;
                    case ImageType.PERSON:
                    case ImageType.PERSON_TEMP:
                        img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/person.png"));
                        break;
                    default:
                        img = Image.FromFile(context.HttpContext.Server.MapPath("~/Images/default.png"));
                        break;
                }
                if (Width == 0 || Height == 0)
                    context.HttpContext.Response.BinaryWrite(FileHandler.ConvertImageToBytes(img));
                else
                    context.HttpContext.Response.BinaryWrite(ResizeImage(img, Width, Height));
            }
            context.HttpContext.Response.ContentType = "image/JPEG";
            //context.HttpContext.Response.ContentType = FileHandler.GetMimeType(img);
        }

        #region GetUrlBase
        string GetUrlBase(ControllerContext context)
        {
            string url = "";
            if (context.HttpContext.Cache["GroupImageUrlBase"] == null)
            {
                ETC.BL.ProductBL bl = new ETC.BL.ProductBL();
                ETC.Structures.Models.Products.UrlDouwnload urlDown = bl.GetUrlDownload();
                url = urlDown.urlD;
                context.HttpContext.Cache["GroupImageUrlBase"] = url;
            }
            else
                url = context.HttpContext.Cache["GroupImageUrlBase"].ToString();
            return url;
        }
        #endregion

        #region GetImage
        Image GetImage(string url)
        {
            Image img = null;
            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(url);
                using (System.Net.WebResponse response = req.GetResponse())
                {
                    using (System.IO.Stream stream = response.GetResponseStream())
                        img = System.Drawing.Image.FromStream(stream);
                }
            }
            catch (Exception)
            {
            }
            return img;
        }
        #endregion

        #region GetImageGroup
        void GetImageGroup(ControllerContext context, ETC.BL.Common.FileType fileType, ref Image img, ref string imageName, ref string imageType, ref string url)
        {
            ETC.BL.Groups.IGroupBL bl = new ETC.BL.Groups.GroupBL();
            ETC.Structures.Models.Groups.GroupFile groupFile = bl.GetGroupFile(Id, fileType);
            if (groupFile != null)
            {
                imageName = groupFile.Name;
                imageType = groupFile.MimeType;
            }
            if (!string.IsNullOrEmpty(imageName))
            {
                url = string.Format("{0}/{1}", GetUrlBase(context), imageName);
                img = GetImage(url);
            }
        }
        #endregion

        #region GetImageGroupTemp
        void GetImageGroupTemp(ControllerContext context, ETC.BL.Common.FileType fileType, ref Image img, ref string imageName, ref string imageType, ref string url)
        {
            imageName = FileHandler.GetImageTemp(Id, fileType);
            //imageName = System.Configuration.ConfigurationManager.AppSettings["BL.Group.DefaultPath"].Replace("{ID}", Id.ToString());
            //imageName = string.Format("{0}/{1}", imageName, System.Configuration.ConfigurationManager.AppSettings["BL.Group.DefaultPath"]);
            url = string.Format("{0}/{1}", GetUrlBase(context), imageName);
            img = GetImage(url);
        }
        #endregion

        #region GetImageProduct
        void GetImageProduct(ControllerContext context, ref Image img, ref string imageName, ref string imageType, ref string url, ref string resourceExtension)
        {
            ETC.BL.IProductBL bl = new ETC.BL.ProductBL();
            //List<ETC.Structures.Models.Products.Cover> images = bl.GetCoverInfoByProduct(Id);
            //if (images != null)
            //{
            //    if (images.Count > 0)
            //    {
            //        imageName = images[0].fileUrl;
            //        imageType = images[0].coverType.ToString();
            //        url = images[0].fileUrl;
            //        img = GetImage(url);
            //        //imageType = FileHandler.GetMimeType(img);
            //    }
            //}
            List<ETC.Structures.Models.Products.ProductFile> productFiles = bl.GetProductFileByProduct(Id);
            if (productFiles != null && productFiles.Count > 0)
            {
                ETC.Structures.Models.Products.ProductFile c = productFiles.Find(x => x.Type == 2);
                string urlBase = GetUrlBase(context);
                if (c != null)
                {
                    url = string.Format("{0}/{1}", urlBase, c.Name);
                    imageName = c.Name;
                    imageType = c.MimeType;
                    img = GetImage(url);
                }
                else
                {
                    c = productFiles.Find(x => x.Type == 3);
                    if (c != null)
                    {
                        resourceExtension = System.IO.Path.GetExtension(c.Name);
                        if (resourceExtension == ExtensionFilesType.IMAGE_JPG_A || resourceExtension == ExtensionFilesType.IMAGE_JPG_B || resourceExtension == ExtensionFilesType.IMAGE_PNG || resourceExtension == ExtensionFilesType.IMAGE_GIF)
                        {
                            url = string.Format("{0}/{1}", urlBase, c.Name);
                            imageName = c.Name;
                            imageType = c.MimeType;
                            img = GetImage(url);
                        }
                    }
                }
            }
        }
        #endregion

        #region GetAvatarPerson
        void GetAvatarPerson(ref Image img, ref string imageName, ref string imageType, ref string url)
        {
            ETC.BL.Person.IPersonBL bl = new ETC.BL.Person.PersonBL();
            ETC.Structures.Models.Avatar image = bl.GetPersonAvatar(Convert.ToInt32(Id));
            if (image != null)
            {
                imageName = image.url;
                url = image.url;
                img = GetImage(url);
                //imageType = FileHandler.GetMimeType(img);
            }
        }
        #endregion

        #region GetAvatarPersonTemp
        void GetAvatarPersonTemp(ControllerContext context, ref Image img, ref string imageName, ref string imageType, ref string url)
        {
            imageName = FileHandler.GetAvatarTemp(Convert.ToInt32(Id));
            url = string.Format("{0}/{1}", GetUrlBase(context), imageName);
            img = GetImage(url);
        }
        #endregion
        
        #region ResizeImage
        public static Image ConvertBytesToImage(byte[] arrays)
        {
            MemoryStream ms = new MemoryStream(arrays);
            ms.Position = 0;
            return Bitmap.FromStream(ms);
        }
        public static byte[] ConvertBitmapToBytes(Bitmap bitmap)
        {
            MemoryStream ms = new MemoryStream();
            bitmap.Save(ms, ImageFormat.Jpeg);
            return ms.ToArray();
        }
        public static byte[] ResizeImage(Image image, int newWidth, int newHeight)
        {
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;
            nPercentW = ((float)newWidth / (float)sourceWidth);
            nPercentH = ((float)newHeight / (float)sourceHeight);
            if (nPercentH < nPercentW)
                nPercent = nPercentH;
            else
                nPercent = nPercentW;
            newWidth = (int)(sourceWidth * nPercent);
            newHeight = (int)(sourceHeight * nPercent);

            Bitmap result = new Bitmap(newWidth, newHeight);
            using (Graphics g = Graphics.FromImage((Image)result))
            {
                g.DrawImage(image, 0, 0, newWidth, newHeight);
            }
            return ConvertBitmapToBytes(result);
        }
        #endregion
    }
}