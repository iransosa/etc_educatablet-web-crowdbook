﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Web_CrowdBook.Models;
using Web_CrowdBook.Utils;
using ETC.Structures.Models;

namespace Web_CrowdBook.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
                return RedirectToAction("Index", "MyGroups");
            else
                return RedirectToAction("Index", "Start");
            //HomeModel model = new HomeModel();
            //if (Request.IsAuthenticated)
            //    model.PersonCrowdBook = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            //return View(model);
        }
    }
}