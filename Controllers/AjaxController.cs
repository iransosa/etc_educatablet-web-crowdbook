﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ETC.Structures.Models;
using ETC.Structures.Models.Groups;
using ETC.BL.Groups;
using Web_CrowdBook.Utils;
using System.Linq;

namespace Web_CrowdBook.Controllers
{
    [Web_CrowdBook.Filters.ValidateAntiForgeryTokenOnAllPosts]
    public class AjaxController : Controller
    {
        #region GetInstitutionsByName
        [HttpPost]
        public JsonResult GetInstitutionsByName(string name, int offset = 1)
        {
            ETC.BL.Institutions.IInstitutionBL bl = new ETC.BL.Institutions.InstitutionBL();
            List<ETC.Structures.Models.Institution> result = bl.GetInstitutionsByName(name, offset, Pagination.LIMIT_PAGE_AUTOCOMPLETE);
            return Json(result);
        } 
        #endregion

        #region GetInstitutionLevel
        [HttpPost]
        public JsonResult GetInstitutionLevels(int institutionId)
        {
            ETC.BL.Institutions.IInstitutionBL bl = new ETC.BL.Institutions.InstitutionBL();
            List<ETC.Structures.Models.Institutions.Level> result = bl.GetFieldByInstitution(new ETC.Structures.Models.Institution() { Id = institutionId });
            return Json(result);
        }
        #endregion

        #region GetMuro
        [Authorize]
        [HttpPost]
        public JsonResult GetMuro(long group, int page)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            List<MessageGroup> muro = null;
            bool success = false;
            string error = "";
            try
            {
                ETC.BL.Message.IMessageBL bl = new ETC.BL.Message.MessageBL();
                muro = bl.GetMessagesByGroupId(group, page, Pagination.LIMIT_PAGE_MURO, Utils.Constants.BRAND, p.Email);
                success = true;
            }
            catch (Exception)
            {
                error = Constants.ERROR_INTERNAL;
            }
            var result = new { success = success, result = muro, page = page + 1, error = error };
            return Json(result);
        }
        #endregion

        #region WriteMuro
        [Authorize]
        //[ValidateAntiForgeryToken]
        [HttpPost]
        public JsonResult WriteMuro(long group, string message)
        {
            message = message.Trim();
            if (message == string.Empty)
                return Json(new { success = false, error = "Debe escribir un mensaje" });
            if (message.Length > Characters.MURO_MESSAGE_MAX)
                return Json(new { success = false, error = string.Format("El mensaje no puede exceder los {0} caracteres", Characters.MURO_MESSAGE_MAX) });

            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            MessageGroup messageGroup = new MessageGroup();
            bool success = false;
            string error = "";
            try
            {
                ETC.BL.Message.IMessageBL bl = new ETC.BL.Message.MessageBL();
                messageGroup.group = new Group();
                messageGroup.group.IdGroup = group;
                messageGroup.MessageText = message;
                messageGroup.IdMessageParent = 0;
                messageGroup.IdPerson = idPerson;
                messageGroup.auth = new ETC.Structures.Models.Users.User();
                messageGroup.auth.clientId = Utils.Constants.BRAND;
                messageGroup = bl.SendMessage(messageGroup, ETC.BL.Common.MessageType.GROUP_MESSAGE, p.Email);
                success = true;
            }
            catch (Exception) 
            {
                error = Constants.ERROR_INTERNAL;
            }
            var result = Json(new { success = success, result = messageGroup, error = error });
            return result;
        }
        #endregion

        #region DeleteMuro
        [Authorize]
        [HttpPost]
        public JsonResult DeleteMuro(long group, long id)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            bool success = false;
            string error = "";
            try
            {
                ETC.BL.Message.IMessageBL bl = new ETC.BL.Message.MessageBL();
                MessageGroup e = new MessageGroup();
                e.group = new Group();
                e.group.IdGroup = group;
                e.IdMessage = id;
                e.IdPerson = idPerson;
                e.auth = new ETC.Structures.Models.Users.User();
                e.auth.clientId = Utils.Constants.BRAND;
                e.auth.email = p.Email;
                if (bl.DeleteMessage(e))
                    success = true;
            }
            catch (Exception)
            {
                error = Constants.ERROR_INTERNAL;
            }
            var result = Json(new { success = success, id = id, error = error });
            return result;
        }
        #endregion

        #region JoinGroup
        [Authorize]
        [HttpPost]
        public JsonResult JoinGroup(long group, bool joinToGroup)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            bool success = false;
            GroupMember member = null;
            bool isMember = !joinToGroup;
            string message = "";
            try
            {
                IGroupBL bl = new GroupBL();
                if (joinToGroup)
                {
                    member = bl.JoinGroup(group, idPerson);
                    if (member != null)
                    {
                        success = true;
                        isMember = true;
                        message = "Abandonar Grupo";
                    }
                }
                else
                {
                    success = bl.LeaveGroup(group, idPerson);
                    if (success)
                    {
                        isMember = false;
                        message = "Unirse al Grupo";
                    }
                }
                CleanCookieSession();
            }
            catch (Exception)
            {
                message = Constants.ERROR_INTERNAL;
            }
            var result = new { success = success, isMember = joinToGroup, member = member, message = message };
            return Json(result);
        }
        #endregion

        #region GetGroupProducts
        [Authorize]
        [HttpPost]
        public JsonResult GetGroupProducts(long id, int itemsCurrent)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            if (!ValidPermissionGroup(id, idPerson))
            {
                var errorPermission = new { success = false, error = Messages.ACCESS_DENIED };
                return Json(errorPermission);
            }

            List<GroupProduct> resources = null;
            bool success = false;
            string error = "";
            try
            {
                IGroupBL bl = new GroupBL();
                resources = bl.GetGroupProducts(id, idPerson, Constants.BRAND_ID, itemsCurrent + 1, itemsCurrent + Pagination.GROUP_RESOURCE);
                success = true;
            }
            catch (Exception)
            {
                error = Constants.ERROR_INTERNAL;
            }
            var result = new { success = success, result = resources, error = error };
            return Json(result);
        }
        #endregion

        #region RefreshGroupProducts
        [Authorize]
        [HttpPost]
        public JsonResult RefreshGroupProducts(long id, int itemsCurrent)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            if (!ValidPermissionGroup(id, idPerson))
            {
                var errorPermission = new { success = false, error = Messages.ACCESS_DENIED };
                return Json(errorPermission);
            }
            List<GroupProduct> resourcesSummary = null;
            List<GroupProduct> resourcesList = null;
            bool success = false;
            string error = "";
            try
            {
                IGroupBL bl = new GroupBL();
                resourcesSummary = bl.GetGroupProducts(id, idPerson, Constants.BRAND_ID, Pagination.GROUP_RESOURCE_SUMMARY, Pagination.GROUP_RESOURCE_SUMMARY);
                resourcesList = bl.GetGroupProducts(id, idPerson, Constants.BRAND_ID, itemsCurrent + 1, itemsCurrent + 1);
                success = true;
            }
            catch (Exception)
            {
                error = Constants.ERROR_INTERNAL;
            }
            var result = new { success = success, resultSummary = resourcesSummary, resultList = resourcesList, error = error };
            return Json(result);
        }
        #endregion

        #region GetMembersGroup
        [Authorize]
        [HttpPost]
        public JsonResult GetMembersGroup(long group, int page)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            if (!ValidPermissionGroup(group, idPerson))
            {
                var errorPermission = new { success = false, page = page + 1, error = Messages.ACCESS_DENIED };
                return Json(errorPermission);
            }

            List<GroupMember> members = null;
            bool success = false;
            string error = "";
            try
            {
                IGroupBL bl = new GroupBL();
                members = bl.GetMembersGroup(Pagination.LIMIT_PAGE_MEMBERS, page, group);
                success = true;
            }
            catch (Exception)
            {
                error = Constants.ERROR_INTERNAL;
            }
            var result = new { success = success, result = members, page = page + 1, error = error };
            return Json(result);
        }
        #endregion

        #region ValidPermissionGroup
        bool ValidPermissionGroup(long idGroup, long idPerson)
        {
            IGroupBL bl = new GroupBL();
            GroupMode g = bl.GetGroupMode(idGroup);
            GroupMember member = bl.GetMemberGroup(idGroup, idPerson);
            bool isMember = member != null && member.Status == ETC.BL.Common.GroupMemberStates.ACTIVE.ToString() ? true : false;
            if (!isMember)
            {
                if (g.Key == ETC.BL.Common.GroupModeTypes.PRIVATE_VISIBLE.ToString() || g.Key == ETC.BL.Common.GroupModeTypes.PRIVATE_HIDDEN.ToString())
                {
                    return false;
                }
            }
            return true;
        } 
        #endregion

        #region CleanCookieSession
        void CleanCookieSession()
        {
            HttpCookie cookie = new HttpCookie(Sessions.PERSON_SESSION);
            cookie.Expires = DateTime.Now.AddMinutes(-1);
            Response.Cookies.Add(cookie);
        }
        #endregion
        
        #region SaveGroupResourceAjax
        [HttpPost]
        public JsonResult SaveGroupResourceAjax(HttpPostedFileBase file, HttpPostedFileBase cover, int idGroup, string name, string description)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            string errors = "";
            if (!FileHandler.ValidateResourceType(file, ref errors))
                return Json(new { success = false, message = errors });
            if (!MimeTypes.ValidMimeTypeFromFile(file.InputStream, ref errors))
                return Json(new { success = false, message = errors });
            if (FileHandler.ValidateImageType(file, ref errors))
                cover = null;
            else
            {
                if (cover != null)
                {
                    if (!FileHandler.ValidateImageType(cover, ref errors))
                        return Json(new { success = false, message = errors });
                }
            }
            if (string.IsNullOrEmpty(name))
                return Json(new { success = false, message = Messages.RESOURCE_NAME_REQUIRED });
            name = name.Trim();
            if (name.Length > Characters.RESOURCE_NAME_MAX)
                return Json(new { success = false, message = Messages.RESOURCE_NAME_CHARS });
            if (!string.IsNullOrEmpty(description))
            {
                description = description.Trim();
                if (description.Length > Characters.RESOURCE_DESCRIPTION_MAX)
                    return Json(new { success = false, message = Messages.RESOURCE_DESCRIPTION_CHARS });
            }
            if (ModelState.IsValid && file != null && idGroup != 0)
            {
                try
                {
                    System.IO.Stream coverStream = null;
                    string coverName = "", coverMime = "";
                    if (cover != null)
                    {
                        //Valid cover Image
                        if (!FileHandler.ValidateImage(cover, ref coverStream))
                        {
                            return Json(new { success = false, message = string.Format(FileHandler.FILES_VALID, FileHandler.GetAllowedResourceExtensions()) });
                        }
                        //coverStream = cover.InputStream;
                        coverName = cover.FileName;
                        coverMime = cover.ContentType;
                    }
                    GroupProduct product = null;
                    IGroupBL bl = new GroupBL();
                    bl.SaveGroupProduct(idGroup, p, name, description, file.InputStream, file.FileName, file.ContentType, coverStream, coverName, coverMime, out product);
                    UtilsContext.CleanCookieSession(this.HttpContext);
                    int products = bl.CountGroupProduct(idGroup);
                    return Json(new { success = true, message = Messages.RESOURCE_CREATED, product = product, productsCount = products });
                }
                catch (Exception)
                {
                    return Json(new { success = false, message = Messages.INTERNAL_ERROR });
                }
            }
            else
            {
                return Json(new { success = false, message = string.Format(FileHandler.FILE_REQUIRED, FileHandler.GetAllowedResourceExtensions()) });
            }
        }
        #endregion

        #region DeleteGroupResource
        [HttpPost]
        public JsonResult DeleteGroupResource(int group, int id)
        {
            PersonCrowdBook p = (PersonCrowdBook)Session[Sessions.PERSON_SESSION];
            long idPerson = Convert.ToInt64(p.Id);
            bool success = false;
            int productsCount = 0; 
            string error = "";
            try
            {
                ETC.BL.Groups.IGroupBL bl = new ETC.BL.Groups.GroupBL();
                if (bl.RemoveGroupResource(group, id, p.Email))
                {
                    success = true;
                    UtilsContext.CleanCookieSession(this.HttpContext);
                    productsCount = bl.CountGroupProduct(group);
                }
                else
                    error = Constants.ERROR_INTERNAL;
            }
            catch (Exception)
            {
                error = Constants.ERROR_INTERNAL;
            }
            var result = Json(new { success = success, id = id, error = error, productsCount = productsCount });
            return result;
        }
        #endregion
    }
}