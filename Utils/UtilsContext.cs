﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_CrowdBook.Utils
{
    public class UtilsContext
    {
        public const string DECRYPTING_KEY = "decryptingKey";
        public const string ENCRYPTION_KEY = "publicKey";

        #region CleanCookieSession
        public static void CleanCookieSession(HttpContextBase context)
        {
            HttpCookie cookie = new HttpCookie(Sessions.PERSON_SESSION);
            cookie.Expires = DateTime.Now.AddMinutes(-10);
            context.Response.Cookies.Add(cookie);
        }
        #endregion

        #region GenerateEncriptonKey
        public static string GenerateEncriptonKey(HttpContextBase context)
        {
            if (context.Session[UtilsContext.DECRYPTING_KEY] == null || context.Session[UtilsContext.ENCRYPTION_KEY] == null)
            {
                CustomClientEncryption encryption = new CustomClientEncryption();
                string descriptionKey = "";
                string encryptionKey = encryption.GetKeyEncryptor(out descriptionKey);
                context.Session[UtilsContext.DECRYPTING_KEY] = descriptionKey;
                context.Session[UtilsContext.ENCRYPTION_KEY] = encryptionKey;
                return encryptionKey;
            }
            else
            {
                return context.Session[UtilsContext.ENCRYPTION_KEY].ToString();
            }
        } 
        #endregion

        #region RemoveEncriptonKey
        public static void RemoveEncriptonKey(HttpContextBase context)
        {
            context.Session.Remove(UtilsContext.DECRYPTING_KEY);
            context.Session.Remove(UtilsContext.ENCRYPTION_KEY);
        } 
        #endregion

        #region EncrypValue
        public static string DecryptValue(string value, HttpContextBase context)
        {
            CustomClientEncryption encryptation = new CustomClientEncryption();
            string domainKey = context.Session[UtilsContext.DECRYPTING_KEY].ToString();
            return encryptation.DecryptText(value, domainKey);
        }
        #endregion
    }
}