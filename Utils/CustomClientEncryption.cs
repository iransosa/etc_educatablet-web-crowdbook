﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Web_CrowdBook.Utils
{
    public class CustomClientEncryption
    {
        internal const string KEY_PRIVATE = "560A18CD-6346-4CF0-A2E8-671F9B6B9EA9";
        internal const string KEY_USER = "671F9B6B9EA9-560A18CD";

        #region GetPublicKey
        public byte[] GetPublicKey(out string decryptingKey)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            //Return Key Pair (Public + Private Key) to be used by ValidateUser
            decryptingKey = rsa.ToXmlString(true);
            //HttpRuntime.Cache["KeyPair"] = rsa.ToXmlString(true);
            RSAParameters param = rsa.ExportParameters(false);
            string keyToSend = ToHexString(param.Exponent) + "," + ToHexString(param.Modulus);

            // Encrypting public key to block Man-in-the-Middle attack
            byte[] encrypted;
            using (RijndaelManaged myRijndael = NewRijndaelManaged(KEY_USER))
            {
                ICryptoTransform encryptor = myRijndael.CreateEncryptor(myRijndael.Key, myRijndael.IV);
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {
                            swEncrypt.Write(keyToSend);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }
            return encrypted;
        }
        #endregion

        #region NewRijndaelManaged
        RijndaelManaged NewRijndaelManaged(string salt)
        {
            if (salt == null)
                throw new ArgumentNullException("salt");

            var saltBytes = Encoding.ASCII.GetBytes(salt);
            var key = new Rfc2898DeriveBytes(KEY_PRIVATE, saltBytes);
            RijndaelManaged aesAlg = new RijndaelManaged();
            aesAlg.Key = key.GetBytes(aesAlg.KeySize / 8);
            aesAlg.IV = key.GetBytes(aesAlg.BlockSize / 8);
            return aesAlg;
        }
        #endregion

        #region GetKeyEncryptor
        public string GetKeyEncryptor(out string decryptingKey)
        {
            byte[] data = GetPublicKey(out decryptingKey);
            string key = "";
            using (RijndaelManaged rijAlg = NewRijndaelManaged(KEY_USER))
            {
                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);
                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(data))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        // Read the decrypted bytes from the decrypting stream and place them in a string.
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                            key = srDecrypt.ReadToEnd();
                    }
                }
                return key;
            }
        }
        #endregion

        #region ToHexString and ToHexByte
        public static string ToHexString(byte[] byteValue)
        {
            char[] lookup = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
            int i = 0, p = 0, l = byteValue.Length;
            char[] c = new char[l * 2];
            while (i < l)
            {
                byte d = byteValue[i++];
                c[p++] = lookup[d / 0x10];
                c[p++] = lookup[d % 0x10];
            }
            return new string(c, 0, c.Length);
        }
        public static byte[] ToHexByte(string str)
        {
            byte[] b = new byte[str.Length / 2];
            for (int y = 0, x = 0; x < str.Length; ++y, x++)
            {
                byte c1 = (byte)str[x];
                if (c1 > 0x60) c1 -= 0x57;
                else if (c1 > 0x40) c1 -= 0x37;
                else c1 -= 0x30;
                byte c2 = (byte)str[++x];
                if (c2 > 0x60) c2 -= 0x57;
                else if (c2 > 0x40) c2 -= 0x37;
                else c2 -= 0x30;
                b[y] = (byte)((c1 << 4) + c2);
            }
            return b;
        }
        #endregion

        #region DecryptText
        public string DecryptText(string encriptedText, string domainKey)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(domainKey);
            return Encoding.UTF8.GetString(rsa.Decrypt(ToHexByte(encriptedText), false));
        }
        #endregion
    }
}