﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Web_CrowdBook.Utils
{
    public class Sessions
    {
        public const string PERSON_SESSION = "Person";
        //public const string CROWDBOOK_SESSION = "ck_session";
    }
    public class MyCookies
    {
        public const string PERSON_SESSION = "Person";
        //public const string CROWDBOOK_SESSION = "ck_session";
    }
    public class Messages
    {
        public const string GROUP_CREATED = "Grupo guardado exitosamente";
        public const string AVATAR_CREATED = "El avatar ha sido guardado exitosamente";
        public const string RESOURCE_CREATED = "El recurso ha sido guardado exitosamente";
        public const string GROUP_CLOSED = "Operación realizada exitosamente. El grupo será cerrado en 1 día para permitir a sus miembros la descarga de contenido.";

        public const string INTERNAL_ERROR = "El request no pudo ser procesado";
        public const string ACCESS_DENIED = "Acceso negado";
        public const string PAGE_NOT_FOUND = "La página o dirección que intentas acceder no existe o ha sido eliminada";
        public const string SERVER_FULL = "Nuestros servidores se encuentran full";
        public const string REQUEST_TOO_LARGE = "La información que intenta subir es muy pesada y ha excedido el límite permitido";
        public const string BAD_REQUEST = "Información o request no válido";
        public const string SERVICE_UNAVAILABLE = "Nuestros servicios no se encuentran disponibles";
        public const string RESOURCE_NOT_FOUND = "El Recurso que intentas acceder no existe o ha sido eliminado";

        public const string RESOURCE_NAME_REQUIRED = "El Nombre del Recurso es obligatorio";
        public const string RESOURCE_NAME_CHARS = "El nombre del Recurso permite un máximo de 100 caracteres";
        public const string RESOURCE_DESCRIPTION_CHARS = "La descripción del Recurso permite un máximo de 200 caracteres";

        public const string PROFILE_UPDATED = "Su Perfil ha sido actualizado";
        public const string PASSWORD_CHANGED = "Su contraseña ha sido modificada";

        public const string GROUP_NOT_FOUND = "El no existe o ha sido removido";
        public const string GROUP_PRIVATED = "El Grupo no ha sido encontrado o no tienes permiso para visualizarlo";
        public const string REQUEST_MEMBER_PENDING = "Debe esperar que los administradores del grupo lo acepten como miembro";
        public const string GROUP_LEAVED = "Se ha salido del grupo exitosamente";

        public const string EMAIL_EXIST = "El Email ya se encuentra en uso";

        public const string LOGIN_INVALID = "Email o contraseña inválida";
        public const string EMAIL_OR_PASSWORD_MISSING = "Debe ingresar el Email y la Contraseña";

        public const string IMAGE_NOT_FOUND = "No se encontró la imagen";
    }
    public class Characters
    {
        public const int RESOURCE_NAME_MAX = 100;
        public const int RESOURCE_DESCRIPTION_MAX = 200;
        public const int MURO_MESSAGE_MAX = 2000;
    }
}