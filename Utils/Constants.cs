﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Web_CrowdBook.Utils
{
    public class Constants
    {
        public const string BRAND = "CrowdBook";
        public const long BRAND_ID = 4;

        public const string ERROR_INTERNAL = "Ha ocurrido un error interno";

        public const string REGEX_PASSWORD = @"^[\Wa-zA-Z0-9]{8,20}$";
        public const string REGEX_PASSWORD_ENCRYPTED = @"^[\Wa-zA-Z0-9]{8,500}$";

        public const string MESSAGE_ERROR_PASSWORD = "La contraseña debe tener entre 8 y 20 caracteres y contener letras y número";
        public const string MESSAGE_ERROR_PASSWORD_FORMAT = "La contraseña no tiene el formato correcto";
        public const string MESSAGE_CURRENT_PASSWORD_INVALID = "Contraseña actual incorrecta";
    }
}