﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Data;
using ETC.Structures.Models;
using System.IO;
using System.Web.Mvc;

namespace Web_CrowdBook.Utils
{
    public class RegularExpresion
    {
        //const string HTML_TAG_PATTERN = "<.*?>";
        public const string HTML_TAGS = "<[^>]*>";
    }
    public class Utils
    {
        #region InitializeLogger
        public static void InitializeLogger()
        {
            LogWriterFactory logWriterFactory = new LogWriterFactory();
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
            Logger.SetLogWriter(logWriterFactory.Create());
        } 
        #endregion

        #region ConvertPersonEtToPersonSession
        public static PersonCrowdBook ConvertPersonEtToPersonSession(PersonInformation personEt, string userName)
        {
            long idPerson = Convert.ToInt64(personEt.id);
            PersonCrowdBook person = new PersonCrowdBook();
            person.Id = Convert.ToDecimal(idPerson);
            person.UserNameET = userName;
            person.FirstName = personEt.firstname;
            person.LastName = personEt.lastname;
            person.Fullname = personEt.firstname + " " + personEt.lastname;
            person.Email = personEt.email;
            person.StoreCountryId = personEt.country.id;
            person.Institution = personEt.institution_name;
            person.InstitutionAlias = personEt.institutionAlias;
            ETC.BL.Groups.GroupBL blGroup = new ETC.BL.Groups.GroupBL();
            person.MyGroups = blGroup.CountGroupPerson(idPerson);
            person.Products = blGroup.CountPersonProduct(idPerson);
            return person;
        }
        #endregion

        #region RemoveHtmlTags
        public static string RemoveHtmlTags(string inputString)
        {
            return System.Text.RegularExpressions.Regex.Replace(inputString, RegularExpresion.HTML_TAGS, string.Empty);
        } 
        #endregion

        #region FormatUrl
        public static string FormatUrl(string url)
        {
            // Replace invalid characters with empty strings.
            try
            {
                return System.Text.RegularExpressions.Regex.Replace(url, @"[^\w\-]", " ", System.Text.RegularExpressions.RegexOptions.None, TimeSpan.FromSeconds(1.5)).Replace(' ', '-');
            }
            // If we timeout when replacing invalid characters, 
            // we should return Empty.
            catch (System.Text.RegularExpressions.RegexMatchTimeoutException)
            {
                return url;
            }
        } 
        #endregion

        #region IsBase64String
        public static bool IsBase64String(string base64String)
        {
            base64String = base64String.Trim();
            return (base64String.Length % 4 == 0) &&
                   System.Text.RegularExpressions.Regex.IsMatch(base64String, @"^[a-zA-Z0-9\+/]*={0,3}$", System.Text.RegularExpressions.RegexOptions.None);
        }
        #endregion

        #region CopyToMemory
        public static MemoryStream CopyToMemory(Stream input)
        {
            // It won't matter if we throw an exception during this method;
            // we don't *really* need to dispose of the MemoryStream, and the
            // caller should dispose of the input stream
            MemoryStream ret = new MemoryStream();
            byte[] buffer = new byte[8192];
            int bytesRead;
            while ((bytesRead = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                ret.Write(buffer, 0, bytesRead);
            }
            // Rewind ready for reading (typical scenario)
            ret.Position = 0;
            return ret;
        } 
        #endregion

        #region GetErrorsModelState
        public static List<KeyValuePair<string, string>> GetErrorsModelState(ModelStateDictionary modelState)
        {
            List<KeyValuePair<string, string>> listsErrors = new List<KeyValuePair<string, string>>();
            foreach (var modelError in modelState)
            {
                foreach (var item2 in modelError.Value.Errors)
                {
                    KeyValuePair<string, string> i = new KeyValuePair<string, string>(modelError.Key, item2.ErrorMessage);
                    listsErrors.Add(i);
                }
            }
            return listsErrors;
        }
        public static List<string> GetErrorsModelStateList(ModelStateDictionary modelState)
        {
            List<string> listsErrors = new List<string>();
            foreach (var modelError in modelState)
            {
                foreach (var item2 in modelError.Value.Errors)
                {
                    listsErrors.Add(item2.ErrorMessage);
                }
            }
            return listsErrors;
        } 
        #endregion
    }
}