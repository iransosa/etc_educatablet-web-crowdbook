﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_CrowdBook.Utils
{
    public class Pagination
    {
        public const int LIMIT_PAGE = 30;
        public const int GROUP_RESOURCE = 30;
        public const int GROUP_RESOURCE_SUMMARY = 5;
        public const int LIMIT_PAGE_MURO = 20;
        public const int LIMIT_PAGE_MEMBERS = 20;
        public const int LIMIT_PAGE_AUTOCOMPLETE = 6;
    }
}