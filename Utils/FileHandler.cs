﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing;
using System.Web;

namespace Web_CrowdBook.Utils
{
    public class FileHandler
    {
        public const string FILES_VALID = "Solo se permiten archivos en los formatos: {0}";
        public const string FILE_REQUIRED = "Debe subir un archivo, se permiten {0}";
        public const string FILES_INVALID = "El archivo parece ser inválido o estar dañado";
        public const string MAX_FILE_SIZE = "La imagen no puede pesar mas de {0}MB";
        public const string ERROR_UPLOAD = "Error cargando archivo";
        public static readonly string[] ALLOWED_IMAGE_EXTENSIONS = { ".jpg", ".jpeg", ".gif", ".png" };
        public static readonly string[] ALLOWED_RESOURCE_EXTENSIONS = { ".pdf", ".epub", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx", ".jpg", ".jpeg", ".png" };
        //public static readonly string[] ALLOWED_RESOURCE_MIME = { "application/pdf", "application/epub+zip", "application/octet-stream", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", "image/png", "image/jpeg" };

        #region ValidateImageExtension
        public static bool ValidateImageExtension(Image image)
        {
            if (image.RawFormat.Equals(ImageFormat.Jpeg) || image.RawFormat.Equals(ImageFormat.Gif) || image.RawFormat.Equals(ImageFormat.Png))
                return true;
            else
                return false;
        }
        public static bool ValidateImageExtension(byte[] bytes)
        {
            Image image = ConvertBytesToImage(bytes);
            if (image.RawFormat.Equals(ImageFormat.Jpeg) || image.RawFormat.Equals(ImageFormat.Gif) || image.RawFormat.Equals(ImageFormat.Png))
                return true;
            else
                return false;
        }
        public static bool ValidateImageExtension(byte[] bytes, out string mimetype)
        {
            Image image = ConvertBytesToImage(bytes);
            mimetype = GetMimeType(image);
            if (image.RawFormat.Equals(ImageFormat.Jpeg) || image.RawFormat.Equals(ImageFormat.Bmp) || image.RawFormat.Equals(ImageFormat.Gif) || image.RawFormat.Equals(ImageFormat.Png))
                return true;
            else
                return false;
        }
        public static string GetMimeType(Image i)
        {
            foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageDecoders())
            {
                if (codec.FormatID == i.RawFormat.Guid)
                    return codec.MimeType;
            }
            return "image/unknown";
        }
        #endregion

        #region ConvertBytesToImage
        public static Image ConvertBytesToImage(byte[] bytes)
        {
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                ms.Position = 0;
                return Image.FromStream(ms);
            }
        }
        #endregion

        #region ConvertImageToBytes
        public static byte[] ConvertImageToBytes(Image img)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }
        #endregion

        #region GetFileFromStream
        public static byte[] GetFileFromStream(HttpPostedFileBase file)
        {
            using (System.IO.BinaryReader reader = new System.IO.BinaryReader(file.InputStream))
            {
                return reader.ReadBytes(file.ContentLength);
            }
        }
        #endregion

        #region GetFileFromStream
        public static byte[] GetFileFromStream(HttpPostedFileBase file, ref string sha1)
        {
            using (System.IO.BinaryReader reader = new System.IO.BinaryReader(file.InputStream))
            {
                sha1 = ETC.BL.Utilities.Utils.GetSHA1HashData(file.FileName + file.ContentLength);
                return reader.ReadBytes(file.ContentLength);
            }
        }
        #endregion

        #region GetSHA1
        public static string GetSHA1(string fileName, int contentLength)
        {
           return ETC.BL.Utilities.Utils.GetSHA1HashData(fileName + contentLength);
        }
        #endregion

        #region ValidateImage
        public static bool ValidateImage(HttpPostedFileBase file, ref string errorMsg)
        {
            int maxContentLength = 1024 * 1024 * 2;
            string errorFormat = string.Format(FILES_VALID, FileHandler.GetAllowedImageExtensions());
            if (file == null)
            {
                errorMsg = string.Format(FILE_REQUIRED, FileHandler.GetAllowedImageExtensions());
                return false;
            }
            //Se valida el peso de la imagen
            if (file.ContentLength > maxContentLength)
            {
                errorMsg =  string.Format(MAX_FILE_SIZE, (maxContentLength / 1024).ToString());
                return false;
            }            
            try
            {
                //string extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
                string extension = System.IO.Path.GetExtension(file.FileName).ToLower();
                //Se valida que realmente sea una imagen
                if (!ALLOWED_IMAGE_EXTENSIONS.Contains(extension))
                {
                    errorMsg = errorFormat;
                    return false;
                }
                return true;
            }
            catch (Exception)
            {
                errorMsg = ERROR_UPLOAD;
                return false;
            }
        }
        #endregion

        #region GetUrlBase
        public static string GetUrlBase(HttpContextBase context)
        {
            string url = "";
            if (context.Cache["GroupImageUrlBase"] == null)
            {
                ETC.BL.ProductBL bl = new ETC.BL.ProductBL();
                ETC.Structures.Models.Products.UrlDouwnload urlDown = bl.GetUrlDownload();
                url = urlDown.urlD;
                context.Cache["GroupImageUrlBase"] = url;
            }
            else
                url = context.Cache["GroupImageUrlBase"].ToString();
            return url;
        } 
        #endregion

        #region GetImage
        public static Image GetImage(string url)
        {
            Image img = null;
            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(url);
                using (System.Net.WebResponse response = req.GetResponse())
                {
                    using (System.IO.Stream stream = response.GetResponseStream())
                        img = System.Drawing.Image.FromStream(stream);
                }
            }
            catch (Exception)
            {
            }
            return img;
        } 
        #endregion

        #region CropImage
        public static Bitmap CropImage(Image image, int width, int height, int x, int y)
        {
            Rectangle CropArea = new Rectangle(x, y, width, height);
            Bitmap bitMap = new Bitmap(CropArea.Width, CropArea.Height);
            using (Graphics g = Graphics.FromImage(bitMap))
            {
                g.DrawImage(image, new Rectangle(0, 0, bitMap.Width, bitMap.Height), CropArea, GraphicsUnit.Pixel);
            }
            return bitMap;
        }
        #endregion

        #region GetImageTemp
        public static string GetImageTemp(long idGroup, ETC.BL.Common.FileType fileType)
        {
            string imageName = System.Configuration.ConfigurationManager.AppSettings["BL.Group.DefaultPath"].Replace("{ID}", idGroup.ToString());
            if (fileType == ETC.BL.Common.FileType.AVATAR)
                return imageName + System.Configuration.ConfigurationManager.AppSettings["BL.Group.FileAvatarNameTemp"];
            else
                return imageName + System.Configuration.ConfigurationManager.AppSettings["BL.Group.FileCoverNameTemp"];
        }
        #endregion

        #region GetAvatarTemp
        public static string GetAvatarTemp(long idPerson)
        {
            string imageName = System.Configuration.ConfigurationManager.AppSettings["BL.Person.DefaultPath"].Replace("{ID}", idPerson.ToString());
            return imageName + System.Configuration.ConfigurationManager.AppSettings["BL.Person.FileAvatarNameTemp"];
        }
        #endregion

        #region GetAllowedResourceExtensions
        public static string GetAllowedResourceExtensions()
        {
            return string.Join(", ", FileHandler.ALLOWED_RESOURCE_EXTENSIONS);
        }
        #endregion

        #region GetAllowedImageExtensions
        public static string GetAllowedImageExtensions()
        {
            return string.Join(", ", FileHandler.ALLOWED_IMAGE_EXTENSIONS);
        }
        #endregion

        #region ValidateImage
        public static bool ValidateImage(HttpPostedFileBase file, ref System.IO.Stream stream)
        {
            string sha1 = "", mimetype = "";
            byte[] bytes = FileHandler.GetFileFromStream(file, ref sha1);
            if (FileHandler.ValidateImageExtension(bytes, out mimetype))
            {
                stream = new System.IO.MemoryStream(bytes);
                return true;
            }
            else
            {
                return false;
            }
        } 
        #endregion

        #region ValidateResourceType
        public static bool ValidateResourceType(HttpPostedFileBase file, ref string error)
        {
            string extension = System.IO.Path.GetExtension(file.FileName).ToLower();
            string mime = file.ContentType.ToLower();
            if (FileHandler.ALLOWED_RESOURCE_EXTENSIONS.Contains(extension))
                return true;
            else
            {
                error = string.Format(FILES_VALID, FileHandler.GetAllowedResourceExtensions());
                return false;
            }
        } 
        #endregion

        #region ValidateImageType
        public static bool ValidateImageType(HttpPostedFileBase file, ref string error)
        {
            string extension = System.IO.Path.GetExtension(file.FileName).ToLower();
            if (FileHandler.ALLOWED_IMAGE_EXTENSIONS.Contains(extension))
            {
                return true;
            }
            else
                error = string.Format(FILES_VALID, FileHandler.GetAllowedImageExtensions());
            return false;
        }
        #endregion
    }
    #region MiMeFilesType
    public class MiMeFilesType
    {
        public const string PDF = "application/pdf";
        public const string EPUB = "application/epub+zip";
        public const string WORD_2003 = "application/msword";
        public const string WORD = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        public const string EXCEL_2003 = "application/vnd.ms-excel";
        public const string EXCE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        public const string POWER_POINT_2003 = "application/vnd.ms-powerpoint";
        public const string POWER_POINT = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
        public const string IMAGE_PNG = "image/png";
        public const string IMAGE_JPG = "image/jpeg";
        public const string IMAGE_GIF = "image/gif";
    } 
    #endregion

    #region ExtensionFilesType
    public class ExtensionFilesType
    {
        public const string PDF = ".pdf";
        public const string EPUB = ".epub";
        public const string WORD_2003 = ".doc";
        public const string WORD = ".docx";
        public const string EXCEL_2003 = ".xls";
        public const string EXCEL = ".xlsx";
        public const string POWER_POINT_2003 = ".ppt";
        public const string POWER_POINT = ".pptx";
        public const string IMAGE_JPG_A = ".jpg";
        public const string IMAGE_JPG_B = ".jpeg";
        public const string IMAGE_PNG = ".png";
        public const string IMAGE_GIF = ".gif";
    } 
    #endregion
}