﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Web_CrowdBook.CustomAttributes
{
    /// <summary>
    /// ustomized data annotation validator for uploading file
    /// </summary>
    public class ValidateImageFileAttribute : ValidationAttribute
    {
        public bool IsRequired { get; set; }
        /// <summary>
        /// Validate the image
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            if (!IsRequired && value == null)
                return true;
            else
            {
                var file = value as HttpPostedFileBase;
                string msgError = "";
                if (Web_CrowdBook.Utils.FileHandler.ValidateImage(file, ref msgError))
                    return true;
                else
                {
                    ErrorMessage = msgError;
                    return false;
                }
            }
        }
    }
}