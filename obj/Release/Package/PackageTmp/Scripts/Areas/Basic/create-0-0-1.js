﻿var createGroup = {
    createLoadAjax: function () {
        createGroup.validation_create();
        $("#btnSaveGroup").click(function () {
            createGroup.create_group();
            return false;
        });
    },
    createLoadAjaxModal: function () {
        createGroup.validation_create();
    },
    editLoadAjax: function () {
        createGroup.validation_create();
        createGroup.institutionAutocompleteWithLevel();
        $("#btnEditGroup").click(function () {
            createGroup.edit_group();
            return false;
        });
    },
    editLoadAjaxMoodal: function () {
        createGroup.validation_create();
        createGroup.institutionAutocompleteWithLevel();
    },
    validation_create: function () {
        $("#formCreate").validate({
            rules: {
                "Group.Name": {
                    required: true,
                    minlength: 2,
                    maxlength: 75,
                },
                "Group.Description": {
                    required: true,
                    minlength: 4,
                    maxlength: 150
                },
                "Group.IdGroupType": {
                    required: true,
                    range: [1, 9999999]
                },
                "Group.IdGroupMode": {
                    required: true,
                    range: [1, 9999999]
                }
            },
            messages: {
                "Group.Name": {
                    required: "El Nombre es requerido",
                    minlength: "El Nombre debe tener entre 2 y 75 caracteres",
                    maxlength: "El Nombre debe tener entre 2 y 75 caracteres"
                },
                "Group.Description": {
                    required: "La Descripción es requerida",
                    minlength: "La Descripción debe tener entre 4 y 150 caracteres",
                    maxlength: "La Descripción debe tener entre 4 y 150 caracteres"
                },
                "Group.IdGroupType": {
                    required: "El Tipo de grupo es requerido",
                    range: "El Tipo de grupo es requerido"
                },
                "Group.IdGroupMode": {
                    required: "La Modalidad de grupo es requerida",
                    range: "El Tipo de grupo es requerido"
                }
            },
            errorPlacement: function (error, element) {
                if (1 == 1 || element.attr("name") == "Group.IdGroupType" || element.attr("name") == "Group.IdGroupMode") {
                    var name = $(element).attr("name");
                    $(error).appendTo("#lstErrors");
                }
                else {
                    error.insertAfter(element);
                }
            },
            ignore: []
        });
    },
    valid_create: function () {
        var form = $("#formCreate");
        form.validate({
            rules: {
                "Group.Name": {
                    required: true,
                    minlength: 2,
                    maxlength: 75,
                },
                "Group.Description": {
                    required: true,
                    minlength: 4,
                    maxlength: 150
                },
                "Group.IdGroupType": {
                    required: true,
                    range: [1, 9999999]
                },
                "Group.IdGroupMode": {
                    required: true,
                    range: [1, 9999999]
                }
            },
            messages: {
                "Group.Name": {
                    required: "El Nombre es requerido",
                    minlength: "El Nombre debe tener entre 2 y 75 caracteres",
                    maxlength: "El Nombre debe tener entre 2 y 75 caracteres"
                },
                "Group.Description": {
                    required: "La Descripción es requerida",
                    minlength: "La Descripción debe tener entre 4 y 150 caracteres",
                    maxlength: "La Descripción debe tener entre 4 y 150 caracteres"
                },
                "Group.IdGroupType": {
                    required: "El Tipo de grupo es requerido",
                    range: "El Tipo de grupo es requerido"
                },
                "Group.IdGroupMode": {
                    required: "La Modalidad de grupo es requerida",
                    range: "El Tipo de grupo es requerido"
                }
            },
            errorPlacement: function (error, element) {
                if (1 == 1 || element.attr("name") == "Group.IdGroupType" || element.attr("name") == "Group.IdGroupMode") {
                    var name = $(element).attr("name");
                    $(error).appendTo("#lstErrors");
                }
                else {
                    error.insertAfter(element);
                }
            },
            ignore: []
        });
        return form.valid();
    },
    valid_create_form: function () {
        var isValid = true;
        if (utils.validate_form($("#txName").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txDescription").val()) == false) {
            isValid = false;
        }
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            $("#div-validations").html("");
            return true;
        }
    },
    valid_edit_form: function () {
        var isValid = true;
        if (utils.validate_form($("#txName").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txDescription").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txInstitution").val()) == false) {
            isValid = false;
        }
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            $("#div-validations").html("");
            if ($("#txInstitution").val().trim() != $("#txInstitutionH").val().trim()) {
                $("#txtInstitutionId").val("");
                $("#txInstitution").val("");
                $("#txInstitutionH").val("");
            }
            return true;
        }
    },
    institutionAutocompleteWithLevel: function () {
        $("#txInstitution").autoCompleteCustom("/AjaxCommon/GetInstitutionsByName", { params: "'offset':" + 1, elementId: "#txInstitutionId", elementText: "#txInstitutionH" });
        $("#txInstitution").focusout(function () {
            if ($("#txInstitutionId").val() == "") {
                $("#ddlLevels").html("");
            }
            else {
                utils.ajax('/Ajax/GetInstitutionLevels', "{'institutionId':" + $("#txInstitutionId").val() + "}", function (data) {
                    data = data.hasOwnProperty("d") ? data.d : data;
                    $("#ddlLevels").html("");
                    $(data).each(function () {
                        $("#ddlLevels").append("<option value='" + this.IDLevel + "'>" + this.Description + "</option>");
                    });
                    $('#ddlLevels').trigger("chosen:updated")
                }, null);
            }
        });
    },
    create_group: function () {
        if (createGroup.valid_create() == false) {
            return false;
        }
        if (createGroup.valid_create_form() == false)
            return false;
        else {
            $("#create-g-loadin").show();
            try {
                var values = new Object();
                values.Name = $("#txName").val();
                values.Description = $("#txDescription").val();
                values.IdGroupMode = $("input:radio[name='Group.IdGroupMode']:checked").val();
                values.IdGroupType = $("input:radio[name='Group.IdGroupType']:checked").val();
                var cls = new Object();
                cls.model = values;
                cls = JSON.stringify(cls);
                utils.ajax('/AjaxGroup/CreateGroup', cls, function (result) {
                    $("#create-g-loadin").hide();
                    result = result.hasOwnProperty("d") ? result.d : result;
                    if (result.success == true) {
                        var data = result.group;
                        $("#create").hide();
                        $("#created").show();
                        $("#created-pin").text(data.Pin);
                        var url = "/Group/{1}/{0}".format(data.IdGroup, data.Name.removeSymbols());
                        $("#btn-go-group").attr("href", url);
                        utils.addMenuMyGroupsValue(result.groupsCount);
                        //Render group if it be  in MyGroups
                        if ($("#my-groups").size() > 0) {
                            var type = $("#my-groups").attr("data-type");
                            if (typeof type != "undefined" && type == "1") {
                                newGroupHtml = templatesHTML.my_groups.format(data.IdGroup, data.Name, data.Name.removeSymbols(), data.Owner, data.Members, data.Resources);
                                $(newGroupHtml).hide().prependTo("#list-groups-main").fadeIn("slow");
                            }
                        }
                    }
                    else {
                        if (result.errors != null) {
                            alert(result.errors.join("\n"));
                        }
                        else {
                            alert(result.error);
                        }
                    }
                }, function () {
                    $("#create-g-loadin").hide();
                    utils.errorInternal();
                });
            } catch (e) {
                $("#create-g-loadin").hide();
            }
        }
    },
    edit_group_callback: null,
    edit_group: function () {
        if (createGroup.valid_create() == false) {
            return false;
        }
        if (createGroup.valid_edit_form() == false)
            return false;
        else {
            $("#create-g-loadin").show();
            try {
                var values = new Object();
                values.IdGroup = $("#txIdGroup").val();
                values.Name = $("#txName").val();
                values.Description = $("#txDescription").val();
                values.IdInstitution = $("#txInstitutionId").val();
                values.Institution = $("#txInstitution").val();
                values.IdLevel = $("#ddlLevels").val();
                values.IdGroupMode = $("input:radio[name='Group.IdGroupMode']:checked").val();
                values.IdGroupType = $("#txIdGroupType").val();
                var cls = new Object();
                cls.model = values;
                cls = JSON.stringify(cls);
                utils.ajax('/AjaxGroup/EditGroup', cls, function (result) {
                    $("#create-g-loadin").hide();
                    result = result.hasOwnProperty("d") ? result.d : result;
                    if (result.success == true) {
                        $("#edit").hide();
                        $("#edited").show();
                        if (createGroup.edit_group_callback != null) {
                            if (typeof createGroup.edit_group_callback == "function") {
                                createGroup.edit_group_callback();
                            }
                        }
                    }
                    else {
                        if (result.errors != null) {
                            alert(result.errors.join("\n"));
                        }
                        else {
                            alert(result.error);
                        }
                    }
                }, function () {
                    $("#create-g-loadin").hide();
                    utils.errorInternal();
                });
            } catch (e) {
                $("#create-g-loadin").hide();
            }
        }
    }
}