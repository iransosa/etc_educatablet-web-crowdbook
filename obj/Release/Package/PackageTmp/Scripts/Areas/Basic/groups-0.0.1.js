﻿$(document).ready(function () {
    $("#list-groups-main").attr("data-infinity-method", "groups.searchGroups");
    $("#list-groups-main").addClass("infinityWindow");
    groups.attachEventRequest();
    $("#btnRequestJoin").click(function () {
        alert("Functionality not Available");
    });
    $("#txSearchHeader").val($("#txtSearch").val());
    $("#list-groups-main").attr("data-page", $("#txtPage").val());
    $(window).scrollInfinity(".infinityWindow", { addClassInfinity: false });
});
var groups = {
    searchGroups: function (callback) {
        $("#list-items").addClass("loading-items");
        var element = $("#list-groups-main");
        if (typeof element.attr("data-page") == "undefined")
            element.attr("data-page", 1);
        page = element.attr("data-page");
        var values = "{'page':" + page + ", 'search': '" + $("#txtSearch").val() + "'}";
        utils.ajax('../../AjaxGroup/SearchGroups', values, function (result) {
            try {
                result = result.hasOwnProperty("d") ? result.d : result;
                element.attr("data-page", result.page);
                var data = result.result;
                if (data == null || data.length == 0)
                    $.fn.scrollInfinity.unbind("#list-groups-main");
                else {
                    var items = [];
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].IsMember == false && data[i].GroupMode.Key == groupModeTypes.VISIBLE_PRIVATE)
                            items.push(templatesHTML.my_groups_private.format(data[i].IdGroup, data[i].Name, data[i].Name.removeSymbols(), data[i].Owner, data[i].Members, data[i].Resources));
                        else
                            items.push(templatesHTML.my_groups.format(data[i].IdGroup, data[i].Name, data[i].Name.removeSymbols(), data[i].Owner, data[i].Members, data[i].Resources));
                    }
                    $(items.join("")).hide().appendTo("#list-groups-main").fadeIn("slow");
                    groups.attachEventRequest();
                }
            } catch (e) { }
            $("#list-items").removeClass("loading-items");
            $.fn.scrollInfinity.callBack("#list-groups-main");
        },
        function (result) {
            $("#list-items").removeClass("loading-items");
            $.fn.scrollInfinity.callBack("#list-groups-main");
        });
    },
    groupPrivate: function (id, name) {
        //alert("¡Grupo Privado, debes ser miembro para visualizarlo!");
        $("#txtRequestId").val(id);
        $("#sRequestName").text('"{0}"'.format(name));
        $('#modal-request').modal();
        return false;
    },
    attachEventRequest: function () {
        $(".group.inserted", ".list-groups").each(function () {
            var li = this;
            var id = $(this).attr("data-group");
            var name = $(".dName", this).text();
            $(".send-request", this).click(function () {
                groups.groupPrivate(id, name, this);
                return false;
            });
            $(li).removeClass("inserted");
        });
    },
}