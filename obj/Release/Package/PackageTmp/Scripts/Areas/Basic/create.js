﻿var createGroup = {
    editLoad: function () {
        createGroup.institutionAutocompleteWithLevel();
        createGroup.validation_create();
        $("#uploadAvatar").change(function () {
            if (createGroup.validateImage($(this).val()) == true) {
                utils.showModalLoading(true);
                $("#formFileAvatar").submit();
            }
        });
        $("#imageCropped").load(function () {
            utils.hideModalLoading();
            cropImage.load();
        });
        $("#btnCrop").click(function () {
            createGroup.saveCrop();
        });
        $("#btnCancelCrop").click(function () {
            cropImage.instance.cancelSelection();
            $("#modal-image-crop").modal("hide");
        });
        menuDropEvents.load();
    },
    createLoad: function () {
        createGroup.institutionAutocompleteWithLevel();
        createGroup.validation_create();
        $('#btnDialogImg').click(function () {
            $('#modalImg').modal({ show: true, backdrop: 'static' });
        });
        $("#btnAcceptImage").click(function () {
            var fileName = $("#uploadAvatar").val();
            if (fileName == "") {
                alert("¡Debe cargar una imagen!");
                return false;
            }
            else {
                if (!utils.validImageExension(fileName))
                    return false;
                else
                    $("#image-group").addClass("ico_check");
            }
        });
        $("#btnCancelImage").click(function () {
            $("#uploadAvatar").val("");
            $("#image-group").removeClass("ico_check");
        });
    },
    createLoadAjax: function () {
        createGroup.validation_create();
        $("#btnCreateGroup").click(function () {
            createGroup.save_group();
            return false;
        });
    },
    institutionAutocompleteWithLevel: function () {
        $("#txtInstitution").autoCompleteCustom("../../AjaxCommon/GetInstitutionsByName", { params: "'offset':" + 1, elementId: "#txtInstitutionId", elementText: "#txtInstitutionH" });
        $("#txtInstitution").focusout(function () {
            if ($("#txtInstitutionId").val() == "") {
                $("#ddlLevels").html("");
            }
            else {
                utils.ajax('../../Ajax/GetInstitutionLevels', "{'institutionId':" + $("#txtInstitutionId").val() + "}", function (data) {
                    data = data.hasOwnProperty("d") ? data.d : data;
                    $("#ddlLevels").html("");
                    $(data).each(function () {
                        $("#ddlLevels").append("<option value='" + this.IDLevel + "'>" + this.Description + "</option>");
                    });
                    $('#ddlLevels').trigger("chosen:updated")
                }, null);
            }
        });
    },
    validateImage: function (fileName) {
        if (fileName == "") {
            alert("¡Debe cargar una imagen!");
            return false;
        }
        else {
            if (utils.validImageExension(fileName))
                return true;
            else
                return false;
        }
    },
    isFirstLoad: true,
    uploadAvatar_complete: function () {
        if (this.isFirstLoad == true) {
            this.isFirstLoad = false;
            return;
        }
        var d = new Date();
        $("#imageCropped").attr("data-crop-type", "1");
        $("#imageCropped").attr("src", "/Image/GroupTemp/{0}?width=400&height=400&{1}".format($("#txtIdGroup").val(), d.getTime()));
        utils.hideModalLoading();
        utils.showModalLoading(false);
        document.getElementById("formFileAvatar").reset();
        var newImg = $.parseJSON($("#pUploadAvaTarget").contents().find("#jsonResult")[0].innerHTML);
        if (newImg.IsValid == false) {
            alert(newImg.Message);
            return;
        }
    },
    saveCrop: function () {
        var values = new Object();
        values.group = $("#txtIdGroup").val();
        values.x1 = parseInt($("#cropX1").val());
        values.y1 = parseInt($("#cropY1").val());
        values.x2 = parseInt($("#cropX2").val());
        values.y2 = parseInt($("#cropY2").val());
        values.originalWidth = $("#imageCropped").outerWidth();
        values.originalHeight = $("#imageCropped").outerHeight();
        values.isAvatar = true;
        values = JSON.stringify(values);
        utils.showModalLoading(true);
        utils.ajaxSync('../../ImageActions/CropImageGroup', values, function (result) {
            result = result.hasOwnProperty("d") ? result.d : result;
            if (result.success == true) {
                $("#modal-image-crop").modal("hide");
                var d = new Date();
                $("#imgAvatar").attr("src", $("#imgAvatar").attr("src") + "&" + d.getTime());
                cropImage.instance.cancelSelection();
                $("#imageCropped").attr("src", "");
            }
            else {
                alert(result.error);
            }
        },
        function () {
            utils.errorInternal();
        });
        utils.hideModalLoading();
    },
    validate_form: function () {
        var isValid = true;
        if (utils.validate_form($("#txName").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txDescription").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txtInstitution").val()) == false) {
            isValid = false;
        }
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            $("#div-validations").html("");
            if ($("#txtInstitution").val().trim() != $("#txtInstitutionH").val().trim()) {
                $("#txtInstitutionId").val("");
                $("#txtInstitution").val("");
                $("#txtInstitutionH").val("");
            }
            return true;
        }
    },
    validate_form_create: function () {
        var isValid = true;
        if (utils.validate_form($("#txName").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txDescription").val()) == false) {
            isValid = false;
        }
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            $("#div-validations").html("");
            return true;
        }
    },
    validation_create: function () {
        $("#formCreate").validate({
            rules: {
                "Group.Name": {
                    required: true,
                    minlength: 2,
                    maxlength: 75,
                },
                "Group.Description": {
                    required: true,
                    minlength: 4,
                    maxlength: 150
                },
                "Group.IdGroupType": {
                    required: true,
                    range: [1, 9999999]
                },
                "Group.IdGroupMode": {
                    required: true,
                    range: [1, 9999999]
                }
            },
            messages: {
                "Group.Name": {
                    required: "El Nombre es requerido",
                    minlength: "El Nombre debe tener entre 2 y 75 caracteres",
                    maxlength: "El Nombre debe tener entre 2 y 75 caracteres"
                },
                "Group.Description": {
                    required: "La Descripción es requerida",
                    minlength: "La Descripción debe tener entre 4 y 150 caracteres",
                    maxlength: "La Descripción debe tener entre 4 y 150 caracteres"
                },
                "Group.IdGroupType": {
                    required: "El Tipo de grupo es requerido",
                    range: "El Tipo de grupo es requerido"
                },
                "Group.IdGroupMode": {
                    required: "La Modalidad de grupo es requerida",
                    range: "El Tipo de grupo es requerido"
                }
            },
            errorPlacement: function (error, element) {
                if (1 == 1 || element.attr("name") == "Group.IdGroupType" || element.attr("name") == "Group.IdGroupMode") {
                    var name = $(element).attr("name");
                    $(error).appendTo("#lstErrors");
                }
                else {
                    error.insertAfter(element);
                }
            },
            ignore: []
        });
    },






    valid_create: function () {
        var form = $("#formCreate");
        form.validate({
            rules: {
                "Group.Name": {
                    required: true,
                    minlength: 2,
                    maxlength: 75,
                },
                "Group.Description": {
                    required: true,
                    minlength: 4,
                    maxlength: 150
                },
                "Group.IdGroupType": {
                    required: true,
                    range: [1, 9999999]
                },
                "Group.IdGroupMode": {
                    required: true,
                    range: [1, 9999999]
                }
            },
            messages: {
                "Group.Name": {
                    required: "El Nombre es requerido",
                    minlength: "El Nombre debe tener entre 2 y 75 caracteres",
                    maxlength: "El Nombre debe tener entre 2 y 75 caracteres"
                },
                "Group.Description": {
                    required: "La Descripción es requerida",
                    minlength: "La Descripción debe tener entre 4 y 150 caracteres",
                    maxlength: "La Descripción debe tener entre 4 y 150 caracteres"
                },
                "Group.IdGroupType": {
                    required: "El Tipo de grupo es requerido",
                    range: "El Tipo de grupo es requerido"
                },
                "Group.IdGroupMode": {
                    required: "La Modalidad de grupo es requerida",
                    range: "El Tipo de grupo es requerido"
                }
            },
            errorPlacement: function (error, element) {
                if (1 == 1 || element.attr("name") == "Group.IdGroupType" || element.attr("name") == "Group.IdGroupMode") {
                    var name = $(element).attr("name");
                    $(error).appendTo("#lstErrors");
                }
                else {
                    error.insertAfter(element);
                }
            },
            ignore: []
        });
        return form.valid();
    },
    valid_create_form: function () {
        var isValid = true;
        if (utils.validate_form($("#txName").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txDescription").val()) == false) {
            isValid = false;
        }
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            $("#div-validations").html("");
            return true;
        }
    },
    save_group: function () {
        if (createGroup.valid_create() == false) {
            return false;
        }
        if (createGroup.valid_create_form() == false)
            return false;
        else {
            var values = new Object();
            values.Name = $("#txName").val();
            values.Description = $("#txDescription").val();
            values.IdGroupMode = $("input:radio[name='Group.IdGroupMode']:checked").val();
            values.IdGroupType = $("input:radio[name='Group.IdGroupType']:checked").val();
            var cls = new Object();
            cls.model = values;
            cls = JSON.stringify(cls);
            utils.ajax('../../AjaxGroup/SaveGroup', cls, function (result) {
                result = result.hasOwnProperty("d") ? result.d : result;
                if (result.success == true) {
                    var data = result.result;
                    alert("Id: " + data.id + " Pin:" + data.pin);
                }
                else {
                    if (result.errors != null) {
                        alert(result.errors.join("\n"));
                    }
                    else {
                        alert(result.error);
                    }
                }
            }, function () {
                utils.errorInternal();
            });
        }
    }
}