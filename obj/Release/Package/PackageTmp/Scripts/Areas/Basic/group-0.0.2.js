﻿$(document).ready(function () {
    $("#uploadAvatar").change(function () {
        $(".edit-group", "#avatar-group").hide();
        if (group.validateImage($(this).val()) == true) {
            utils.showModalLoading(true);
            $("#formFileAvatar").submit();
        }
    });
    $("#uploadCover").change(function () {
        if (group.validateImage($(this).val()) == true) {
            utils.showModalLoading(true);
            $("#formFileCover").submit();
        }
    });
    $("#btnDialogResource").click(function () {
        $('#modalResource').modal({ show: true, backdrop: 'static' });
        return false;
    });
    $("#btnDialogResourceMore").click(function () {
        group.getResources();
        $("#modal-resource-more").modal();
        return false;
    });
    $("#btnMuroMessage").click(function () {
        var message = $("#txtMuroMessage").val().trim();
        if (message != "" && message != null) {
            group.writeMuroHandler();
        }
        else
            $("#txtMuroMessage").select();
    });
    $("#txtMuroMessage").unbind("keyup");
    $("#txtMuroMessage").keyup(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            group.writeMuroHandler();
        }
        var charsCurrent = $(this).val().length;
        if (charsCurrent == 0)
            $("#sCharMuro").html("");
        else
            $("#sCharMuro").text((charsCurrent - 2000) * -1);
    });
    $("#txtMuroMessage").focus(function () {
        $(this).addClass("active");
        return false;
    });
    $('#btnJoinGroup').click(function () {
        if ($("#btnJoinGroup").attr("data-join") == "1") {
            group.joinGroup(false);
        }
        else {
            group.joinGroup(true);
        }
        return false;
    });
    $("#btnAcceptResource").on('click', function () {
        group.save_product();
    });
    $("#imageCropped").load(function () {
        utils.hideModalLoading();
        cropImage.load();
    });
    $("#btnCrop").click(function () {
        group.saveCrop();
    });
    $("#avatar-group").mouseover(function () {
        $(".edit-group", "#avatar-group").show();
    });
    $("#avatar-group").mouseleave(function () {
        $(".edit-group", "#avatar-group").hide();
    });
    $("#btnCancelCrop").click(function () {
        cropImage.instance.cancelSelection();
        $("#modal-image-crop").modal("hide");
    });
    $("#fileResource").change(function () {
        var fileName = $(this).val();
        var extension = fileName.split('.').pop().toLowerCase();
        if (utils.allowed_image.indexOf(extension) < 0) {
            $("#resource-cover").show();
        }
        else {
            $("#resource-cover").hide();
            $("#coverResource").val("");
        }
    });
    group.getMuro();
    group.getMembersGroup();
    group.getResources();
    uploadFilesValid.eventHandler("fileResource");
    //Edit
    resourcePreview.eventHandler();
    group.editGroup_eventHandler();
    $("#list-muro").attr("data-infinity-method", "group.getMuro");
    $("#list-members").attr("data-infinity-method", "group.getMembersGroup");
    $("#list-resource").attr("data-infinity-method", "group.getResources");
    $(window).scrollInfinity(".infinityWindow", { addClassInfinity: false });
    $("#modal-resource-more").scrollInfinity(".infinityResources", { addClassInfinity: false });
});
var group = {
    validLogin: function(){
        if ($("#txtIdPerson").val == "" || $("#txtIdPerson").val == "0")
            document.location = "/MyAccount/login/login?ReturnUrl=" + document.location;
    },
    validateImage: function (fileName) {
        if (fileName == "") {
            alert("¡Debe cargar una imagen!");
            return false;
        }
        else {
            if (utils.validImageExension(fileName))
                return true;
            else
                return false;
        }
    },
    //allowed_resource_extensions: [ ".pdf", ".epub", ".doc", ".docx", ".xls", ".xlsx", ".ppt", ".pptx" ],
    validateResource: function(){
        var fileName = $("#fileResource").val();
        if (fileName == "") {
            alert("¡Debe cargar el archivo!");
            return false;
        }
        if ($("#txtNameResource").val() == "") {
            alert("¡Debe ingresar el nombre del recurso!");
            return false;
        }
        if (utils.validResourceExension(fileName) == false) {
            return false;
        }
        var coverName = $("#coverResource").val();
        if (coverName != "") {
            if (utils.validCoverExension(coverName) == false) {
                return false;
            }
        }
        //Validate Inputs
        var isValid = true;
        if (utils.validate_form($("#txtNameResource").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txtDescResource").val()) == false) {
            isValid = false;
        }
        if (isValid == false) {
            $("#div-validations-res").html(utils.htmlNotLet());
            return false;
        }
        else {
            $("#div-validations-res").html("");
            return true;
        }
        return true;
    },
    getResources: function () {
        var element = $("#list-resource");
        if (typeof element.attr("data-work") == "undefined")
            element.attr("data-work", 1);
        else
            return;
        page = $("li.resource", "#list-resource").size();
        var values = new Object();
        values.id = $("#txtIdGroup").val();
        values.itemsCurrent = page;
        values = JSON.stringify(values);
        utils.ajax('../../Ajax/GetGroupProducts', values, function (result) {
            try {
                result = result.hasOwnProperty("d") ? result.d : result;
                element.removeAttr("data-work");
                if (result.success == true) {
                    //element.attr("data-page", result.page);
                    var data = result.result;
                    if (data == null || data.length == 0) {
                        $.fn.scrollInfinity.unbind("#list-resource");
                    }
                    else {
                        var items = [];
                        for (var i = 0; i < data.length; i++) {
                            if (data[i].HasDRM == true) {
                                items.push(templatesHTML.group_resource_drm.format(data[i].IdProduct, data[i].Name, data[i].Name.removeSymbols(), data[i].Description));
                            }
                            else {
                                items.push(templatesHTML.group_resource.format(data[i].IdProduct, data[i].Name, data[i].Name.removeSymbols(), data[i].Description));
                            }
                        }
                        $(items.join("")).hide().appendTo("#list-resource").fadeIn("slow");
                        resourcePreview.eventHandler();
                        $("#modal-resource-more").removeClass("data-zero");
                    }
                }
            } catch (e) {
            }
            $.fn.scrollInfinity.callBack("#list-resource");
        },
        function (result) {
            element.removeAttr("data-work");
            $.fn.scrollInfinity.callBack("#list-resource");
        });
    },
    refreshResources: function () {
        var element = $("#list-resource");       
        page = $("li.resource", "#list-resource").size();
        var values = new Object();
        values.id = $("#txtIdGroup").val();
        values.itemsCurrent = page;
        values = JSON.stringify(values);
        utils.ajax('../../Ajax/RefreshGroupProducts', values, function (result) {
            try {
                result = result.hasOwnProperty("d") ? result.d : result;
                if (result.success == true) {
                    if($("li.resource", "#list-resource-min").size() < 3){
                        var dataSummary = result.resultSummary;
                        if (dataSummary != null && dataSummary.length > 0) {
                            var items = [];
                            for (var i = 0; i < dataSummary.length; i++) {
                                if (dataSummary[i].HasDRM == true) {
                                    items.push(templatesHTML.group_resource_drm.format(dataSummary[i].IdProduct, dataSummary[i].Name, dataSummary[i].Name.removeSymbols(), dataSummary[i].Description));
                                }
                                else {
                                    items.push(templatesHTML.group_resource.format(dataSummary[i].IdProduct, dataSummary[i].Name, dataSummary[i].Name.removeSymbols(), dataSummary[i].Description));
                                }
                            }
                            $(items.join("")).hide().appendTo("#list-resource-min").fadeIn("slow");
                            resourcePreview.eventHandler();
                        }
                    }
                    var dataList = result.resultList;
                    if (dataList != null && dataList.length > 0) {
                        var items = [];
                        for (var i = 0; i < dataList.length; i++) {
                            if (dataList[i].HasDRM == true) {
                                items.push(templatesHTML.group_resource_drm.format(dataList[i].IdProduct, dataList[i].Name, dataList[i].Name.removeSymbols(), dataList[i].Description));
                            }
                            else {
                                items.push(templatesHTML.group_resource.format(dataList[i].IdProduct, dataList[i].Name, dataList[i].Name.removeSymbols(), dataList[i].Description));
                            }
                        }
                        $(items.join("")).hide().appendTo("#list-resource").fadeIn("slow");
                        resourcePreview.eventHandler();
                        $("#modal-resource-more").removeClass("data-zero");
                    }
                }
            } catch (e) {
            }
        }, null);
    },
    getMuro: function () {
        var element = $("#list-muro");
        if (typeof element.attr("data-work") == "undefined")
            element.attr("data-work", 1);
        else
            return;
        if (typeof element.attr("data-page") == "undefined")
            element.attr("data-page", 1);
        page = element.attr("data-page");
        var values = new Object();
        values.group = $("#txtIdGroup").val();
        values.page = page;
        values = JSON.stringify(values);
        utils.ajax('../../Ajax/GetMuro', values, function (result) {
            try {
                result = result.hasOwnProperty("d") ? result.d : result;
                element.removeAttr("data-work");
                if (result.success) {
                    element.attr("data-page", result.page);
                    var data = result.result;
                    if (data == null || data.length == 0)
                        $.fn.scrollInfinity.unbind("#list-muro");
                    else {
                        var items = [];
                        for (var i = 0; i < data.length; i++) {
                            var htmlElement = "";
                            var videoYoutube = utils.getUrlYoutube(data[i].MessageText);
                            if (data[i].isRemovable == true) {
                                htmlElement = templatesHTML.muro_editable.format(data[i].IdMessage, data[i].sender.id, (data[i].sender.firstName + (data[i].sender.lastName != null ? " " + data[i].sender.lastName : "")), data[i].createdAtFormat, data[i].MessageText.formatHtml(), videoYoutube);
                            }
                            else {
                                htmlElement = templatesHTML.muro.format(data[i].IdMessage, data[i].sender.id, (data[i].sender.firstName + (data[i].sender.lastName != null ? " " + data[i].sender.lastName : "")), data[i].createdAtFormat, data[i].MessageText.formatHtml(), videoYoutube);
                            }
                            try {
                                var htmlElementTemp = utils.formatUrlWithLink(htmlElement);
                                htmlElement = htmlElementTemp != null && htmlElementTemp != "" ? htmlElementTemp : htmlElement;
                            } catch (e) { }
                            items.push(htmlElement);
                        }
                        $(items.join("")).hide().appendTo("#list-muro").fadeIn("slow");
                        group.attachEventMuro();
                    }
                }
            } catch (e) {
                element.removeAttr("data-work");
            }
            $.fn.scrollInfinity.callBack("#list-muro");
        },
        function (result) {
            element.removeAttr("data-work");
            $.fn.scrollInfinity.callBack("#list-muro");
        });
    },
    writeMuroHandler: function () {
        var message = $("#txtMuroMessage").val().trim();
        if (message != "" && message != null) {
            if (message.length <= 2000) {
                $("#txtMuroMessage").val(message);
                group.writeMuro();
                return false;
            }
            else {
                alert("El mensaje no puede exceder los 2000 caracteres");
            }
        }
    },
    writeMuro: function () {
        $("#txtMuroMessage").attr("disabled", "disabled");
        var values = new Object();
        values.group = $("#txtIdGroup").val();
        values.message = $("#txtMuroMessage").val();
        values = JSON.stringify(values);
        utils.ajax('../../Ajax/WriteMuro', values, function (result) {
            result = result.hasOwnProperty("d") ? result.d : result;
            if (result.success == true) {
                var data = result.result;
                var videoYoutube = utils.getUrlYoutube(data.MessageText);
                var htmlElement = templatesHTML.muro_editable.format(data.IdMessage, data.sender.id, (data.sender.firstName + (data.sender.lastName != null ? " " + data.sender.lastName : "")), data.createdAtFormat, data.MessageText.formatHtml(), videoYoutube);
                try {
                    var htmlElementTemp = utils.formatUrlWithLink(htmlElement);
                    htmlElement = htmlElementTemp != null && htmlElementTemp != "" ? htmlElementTemp : htmlElement;
                } catch (e) { }
                $("#txtMuroMessage").val("");
                if($('#list-muro li').size() > 0)
                    $('#list-muro li:first').before(htmlElement).hide().fadeIn("slow");
                else
                    $('#list-muro').append(htmlElement).hide().fadeIn("slow");
                group.attachEventMuro();
                $("#sCharMuro").html("");
            }
            else {
                alert(result.error);
            }
            $("#txtMuroMessage").removeAttr("disabled");
        }, function () {
            $("#txtMuroMessage").removeAttr("disabled");
            utils.errorInternal();
        });
    },
    deleteMuro: function (id, e) {
        $(e).unbind("click");
        $(".muro[data-msg='{0}']".format(id), "#list-muro").addClass("loading");
        var values = new Object();
        values.group = $("#txtIdGroup").val();
        values.id = id;
        values = JSON.stringify(values);
        utils.ajax('../../Ajax/DeleteMuro', values, function (result) {
            result = result.hasOwnProperty("d") ? result.d : result;
            var element = ".muro[data-msg='{0}']".format(result.id);
            element = $(element, "#list-muro");
            if (result.success == true) {
                $(element).fadeOutAndRemove('slow');
            }
            else {
                utils.errorInternal();
                $(element).removeClass("loading");
                group.attachEventMuroDelete(id, $(".delete", element));
            }
        }, function () {
            group.attachEventMuroDelete(id, e);
            $(".muro[data-msg='{0}']".format(id), "#list-muro").addClass("loading");
            utils.errorInternal();
        });
    },
    attachEventMuro: function () {
        $(".muro.inserted", "#list-muro").each(function () {
            var li = this;
            var idElement = $(this).attr("data-msg");
            $(".delete", this).click(function () {
                group.deleteMuro(idElement, this);
                return false;
            });
            $(li).removeClass("inserted");
        });
    },
    attachEventMuroDelete: function (idElement, e) {
        $(e).click(function () {
            group.deleteMuro(idElement, this);
        });
    },
    getMembersGroup: function () {
        var element = $("#list-members");
        if (typeof element.attr("data-work") == "undefined")
            element.attr("data-work", 1);
        else
            return;
        if (typeof element.attr("data-page") == "undefined")
            element.attr("data-page", 1);
        page = element.attr("data-page");
        var values = new Object();
        values.group = $("#txtIdGroup").val();
        values.page = page;
        values = JSON.stringify(values);
        utils.ajax('../../Ajax/GetMembersGroup', values, function (result) {
            try {
                result = result.hasOwnProperty("d") ? result.d : result;
                element.attr("data-page", result.page);
                element.removeAttr("data-work");
                var data = result.result;
                if (data == null || data.length == 0)
                    $.fn.scrollInfinity.unbind("#list-members");
                else {
                    var items = [];
                    for (var i = 0; i < data.length; i++) {
                        items.push(templatesHTML.group_member.format(data[i].Id, data[i].FirstName, data[i].LastName, data[i].Institution));
                    }
                    $(items.join("")).hide().appendTo("#list-members").fadeIn("slow");
                }
            } catch (e) {
                element.removeAttr("data-work");
            }
            $.fn.scrollInfinity.callBack("#list-members");
        },
        function (result) {
            element.removeAttr("data-work");
            $.fn.scrollInfinity.callBack("#list-members");
        });
    },
    joinGroup: function (joinToGroup) {
        group.validLogin();
        var values = new Object();
        values.group = $("#txtIdGroup").val();
        values.joinToGroup = joinToGroup;
        values = JSON.stringify(values);
        $("#btnJoinGroup").unbind("click");
        if(joinToGroup == true)
            $("#modal-joining").modal({ show: true, backdrop: 'static' });
        else
            utils.showModalLoading(true);
        utils.ajax('../../Ajax/JoinGroup', values, function (result) {
            result = result.hasOwnProperty("d") ? result.d : result;
            if (result.success == true) {
                $("#btnJoinGroup").text(result.message);
                if (result.isMember) {
                    location.reload();
                    $("#btnJoinGroup").click(function () {
                        group.joinGroup(false);
                        return false;
                    });
                }
                else {
                    document.location = "../../Actions/LeaveGroup";
                }
            }
            else {
                utils.errorInternal();
            }
            $("#modal-joining").modal("hide");
            utils.hideModalLoading();
        },
        function () {
            $("#modal-joining").modal("hide");
            utils.hideModalLoading();
            utils.errorInternal();
        });
    },
    isFirstLoadAvatar: true,
    isFirstLoadCover: true,
    uploadAvatar_complete: function () {
        if (this.isFirstLoadAvatar == true) {
            this.isFirstLoadAvatar = false;
            return;
        }
        var d = new Date();
        $("#imageCropped").attr("data-crop-type", "1");
        $("#imageCropped").attr("src", "../../Image/GroupTemp/{0}?width=400&height=400&{1}".format($("#txtIdGroup").val(), d.getTime()));
        utils.hideModalLoading();
        utils.showModalLoading(false);
        document.getElementById("formFileAvatar").reset();
        var newImg = $.parseJSON($("#pUploadAvaTarget").contents().find("#jsonResult")[0].innerHTML);
        if (newImg.IsValid == false) {
            alert(newImg.Message);
            return;
        }
    },
    uploadCover_complete: function () {
        if (this.isFirstLoadCover == true) {
            this.isFirstLoadCover = false;
            return;
        }
        d = new Date();
        $("#imageCropped").attr("data-crop-type", "0");
        $("#imageCropped").attr("src", "../../Image/GroupCoverTemp/{0}?width=630&height=240&{1}".format($("#txtIdGroup").val(), d.getTime()));
        utils.hideModalLoading();
        utils.showModalLoading(false);
        document.getElementById("formFileCover").reset();
        var newImg = $.parseJSON($("#pUploadCovTarget").contents().find("#jsonResult")[0].innerHTML);
        if (newImg.IsValid == false) {
            alert(newImg.Message);
            return;
        }
    },
    save_product: function () {
        try {
            var data = new FormData();
            var files = $("#fileResource").get(0).files;
            var covers = $("#coverResource").get(0).files;
            var name = $("#txtNameResource").val();
            var description = $("#txtDescResource").val();
            // Add the uploaded image content to the form data collection
            if (group.validateResource() == false)
                return false;
            utils.showModalLoading(true);
            //$('#modal-loading').modal({ show: true, backdrop: 'static' });
            if (files.length > 0) {
                data.append("idGroup", $("#txtIdGroup").val());
                data.append("file", files[0]);
                if (covers.length > 0)
                    data.append("cover", covers[0]);
                data.append("name", name);
                data.append("description", description);
            }
            // Make Ajax request with the contentType = false, and procesDate = false
            var ajaxRequest = $.ajax({
                type: "POST",
                url: "../../Ajax/SaveGroupResourceAjax/",
                headers: utils.getHeadersAntiForgetTonek(),
                contentType: false,
                processData: false,
                data: data
            });
            ajaxRequest.done(function (responseData, textStatus) {
                if (textStatus == 'success') {
                    if (responseData != null) {
                        if (responseData.success) {
                            $("#fileResource").val("");
                            $("#coverResource").val("");
                            $("#txtNameResource").val("");
                            $("#txtDescResource").val("");
                            $("#modalResource").modal("hide");
                            var data = responseData.product;
                            var listResources = $("li.resource", "#list-resource-min");
                            var item = templatesHTML.group_resource.format(data.IdProduct, data.Name, data.Name.removeSymbols(), data.Description);
                            if (listResources.size() >= 3)
                                $("li.resource", "#list-resource-min").last().remove();
                            $(item).hide().prependTo("#list-resource-min").fadeIn("slow");
                            //Render in Modal Resources
                            var itemsModal = templatesHTML.group_resource.format(data.IdProduct, data.Name, data.Name.removeSymbols(), data.Description);                           
                            $(itemsModal).hide().prependTo("#list-resource").fadeIn("slow");
                            $("#stGroups").text(responseData.productsCount);
                            utils.addMenuProduct();
                            utils.info(responseData.message);
                        } else {
                            if (typeof responseData.message != "undefined") {
                                alert(responseData.message);
                            }
                            else {
                                $("#formNewResource").submit();
                                return true;
                            }
                        }
                        utils.hideModalLoading();
                    }
                    else {
                        $("#formNewResource").submit();
                        return true;
                    }
                }
                else {
                    $("#formNewResource").submit();
                    return true;
                }
            });
            ajaxRequest.fail(function (responseData, textStatus) {
                $("#formNewResource").submit();
                return true;
            });
        } catch (e) {
            $("#formNewResource").submit();
            return true;
        }
        return true;
    },
    saveCrop: function () {
        var isAvatar = cropImage.isAvatar();
        if (isAvatar == null)
            utils.errorInternal();
        var values = new Object();
        values.group = $("#txtIdGroup").val();
        values.x1 = parseInt($("#cropX1").val());
        values.y1 = parseInt($("#cropY1").val());
        values.x2 = parseInt($("#cropX2").val());
        values.y2 = parseInt($("#cropY2").val());
        values.originalWidth = $("#imageCropped").outerWidth();
        values.originalHeight = $("#imageCropped").outerHeight();
        values.isAvatar = isAvatar;
        values = JSON.stringify(values);
        utils.showModalLoading(true);
        utils.ajaxSync('../../ImageActions/CropImageGroup', values, function (result) {
            result = result.hasOwnProperty("d") ? result.d : result;
            if (result.success == true) {
                $("#modal-image-crop").modal("hide");
                var d = new Date();
                if (isAvatar) {
                    $("#imgAvatar").attr("src", $("#imgAvatar").attr("src") + "&" + d.getTime());
                }
                else
                    $("#imgCover").attr("src", $("#imgCover").attr("src") + "&" + d.getTime());
                cropImage.instance.cancelSelection();
                $("#imageCropped").attr("src", "");
            }
            else {
                alert(result.error);
            }
        },
        function () {
            utils.errorInternal();
        });
        utils.hideModalLoading();
    },
    editGroup_eventHandler:function(){
        $("#btn-edit-g").click(function () {
            if ($("#modal-edit-group").size() > 0) {
                $("#modal-edit-group").modal({ show: true, backdrop: 'static' });
            }
            else {
                $('<div id="modal-edit-group" class="modal modal-dym"></div>').appendTo("body");
                $('<div class="modal-loading-s"></div>').appendTo("#modal-edit-group");
                $("#modal-edit-group").modal({ show: true, backdrop: 'static' });
                $.when(
                $.getScript("/Scripts/jquery.validate.min.js"),
                $.Deferred(function (deferred) {
                    $(deferred.resolve);
                })
                ).done(function (script, textStatus) {
                    var url = "/Create/Edit/" + $("#txtIdGroup").val();
                    try {
                        $("#modal-edit-group").load(url + " #create-group", function () {
                            createGroup.edit_group_callback = group.editGroup_Callback;
                            createGroup.editLoadAjaxMoodal();
                        }).delegate("#btnEditGroup", "click", function () {
                            createGroup.edit_group();
                            return false;
                        }).delegate(".close-modal", "click", function () {
                            $("#modal-edit-group").modal("hide");
                            $(".modal-dym").remove();
                            return false;
                        }).delegate("#txInstitution", "focusin", function (e) {
                            $("#txInstitution").autoCompleteCustom("/AjaxCommon/GetInstitutionsByName", { params: "'offset':" + 1, elementId: "#txInstitutionId", elementText: "#txInstitutionH" });
                        });
                    } catch (e) {
                        document.location = url;
                    }
                });
            }
            return false;
        });
    },
    editGroup_Callback: function (result) {
        var group = result.group;
        $("#gName").text(group.Name);
        $("#gDescr").text(group.Description);
        utils.showMessageSuccess(result.message);
    }
};