﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_CrowdBook.ViewModels
{
    public class GroupVM
    {
        [Required(ErrorMessage = "El Grupo es requerido")]
        public long IdGroup { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        [Display(Name = "Nombre")]
        [StringLength(75, MinimumLength = 2, ErrorMessage = "El nombre debe tener entre 2 y 75 caracteres")]
        //[RegularExpression(Web_CrowdBook.Utils.RegularExpresion.HTML_TAGS, ErrorMessage = "El nombre no puede contener caracteres como '<,>'.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "La descripción es requerida")]
        [Display(Name = "Descripción")]
        [StringLength(150, MinimumLength = 2, ErrorMessage = "La descripción debe tener entre 2 y 150 caracteres")]
        //[RegularExpression(Web_CrowdBook.Utils.RegularExpresion.HTML_TAGS, ErrorMessage = "La descripción no puede contener caracteres como '<,>'.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "El Tipo de Grupo es requerido")]
        [Range(1, int.MaxValue, ErrorMessage = "El Tipo de Grupo es requerido")]
        [Display(Name = "Tipo")]
        public long IdGroupType { get; set; }
        [Required(ErrorMessage = "La Modalidad de Grupo es requerida")]
        [Range(1, int.MaxValue, ErrorMessage = "La Modalidad de Grupo es requerida")]
        [Display(Name = "Modalidad")]
        public long IdGroupMode { get; set; }

        [Display(Name = "Información Académica")]
        public int? IdInstitution { get; set; }
        public string Institution { get; set; }

        [Display(Name = "Carrera")]
        public long? IdLevel { get; set; }
        [Display(Name = "Carrera")]
        public string Level { get; set; }

        [Display(Name = "Pin")]
        public string Pin { get; set; }

        [Display(Name = "Fecha de Cierre")]
        public DateTime? ClosingDate { get; set; }
        [Display(Name = "Fecha de Expiración")]
        public DateTime? ExpirationDate { get; set; }
    }
}