﻿using System.Web;
using System.Web.Mvc;
using Web_CrowdBook.Filters;

namespace Web_CrowdBook
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new VerifyAuthFilterAttribute());
        }
    }
}