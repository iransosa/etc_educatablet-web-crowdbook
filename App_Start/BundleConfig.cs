﻿using System.Web;
using System.Web.Optimization;

namespace Web_CrowdBook
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            //Jquery-validate
            bundles.Add(new ScriptBundle("~/bundles/jqueryvalidate").Include(
                        "~/Scripts/jquery.validate.js"));

            ///CropImage            
            bundles.Add(new ScriptBundle("~/bundles/jqueryimgareaselect").Include(
                        "~/jquery.imgareaselect-0.9.10/scripts/jquery.imgareaselect.js"));
            bundles.Add(new StyleBundle("~/Content/jqueryimgareaselect").Include(
                "~/jquery.imgareaselect-0.9.10/css/imgareaselect-default.css"));

            //Main
            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/utils-{version}.js"));
            //Global
            bundles.Add(new ScriptBundle("~/bundles/global").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/utils-{version}.js",
                        "~/Scripts/global-{version}.js"));
            //Account
            bundles.Add(new ScriptBundle("~/bundles/account").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/utils-{version}.js",
                        "~/Scripts/RSA.js",
                        "~/Scripts/Areas/Basic/account-{version}.js"));
            //Start
            bundles.Add(new ScriptBundle("~/bundles/start").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/utils-{version}.js",
                        "~/Scripts/RSA.js",
                        "~/Scripts/Areas/Basic/start-{version}.js"));
            ////Group
            bundles.Add(new ScriptBundle("~/bundles/group").Include(
                        "~/Scripts/Areas/Basic/group-{version}.js"));
            ////MyGroups
            bundles.Add(new ScriptBundle("~/bundles/myGroups").Include(
                        "~/Scripts/Areas/Basic/myGroups-{version}.js"));
            ////Groups
            bundles.Add(new ScriptBundle("~/bundles/groups").Include(
                        "~/Scripts/Areas/Basic/groups-{version}.js"));
            ////Library
            bundles.Add(new ScriptBundle("~/bundles/library").Include(
                        "~/Scripts/Areas/Basic/library-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/site-{version}.css"));

            bundles.Add(new StyleBundle("~/Content/bootstrap/css").Include(
                        "~/Content/flatapp/bootstrap.css"));
        }
    }
}