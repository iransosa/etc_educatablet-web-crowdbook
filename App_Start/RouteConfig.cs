﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Web_CrowdBook
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            //LocalHost/MyGroups
            //LocalHost/Groups/Elnombre/id
            routes.MapRoute(
                 name: "Search",
                 url: "MyGroups/Search/{search}",
                 defaults: new
                 {
                     controller = "MyGroups",
                     action = "Search"
                 }
             );
            routes.MapRoute(
                 name: "Group",
                 url: "Group/{name}/{id}",
                 defaults: new
                 {
                     controller = "Group",
                     action = "Index",
                     name = UrlParameter.Optional
                 }
             );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}