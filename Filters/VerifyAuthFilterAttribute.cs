﻿using System;
using System.Collections.Generic;
using System.Web;
using ETC.Structures.Models;
using System.Web.Mvc;
using Web_CrowdBook.Utils;

namespace Web_CrowdBook.Filters
{
    /// <summary>
    /// This class is intended to execute before calling any action of the 
    /// project. It verifiy if the user has the permissions to access and
    /// warranty the object person is loaded in session
    /// </summary>
    public class VerifyAuthFilterAttribute : ActionFilterAttribute
    {
        [Authorize]
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.IsAuthenticated)
                return;
            PersonCrowdBook person = new PersonCrowdBook();
            //bool cookieExpired = false;
            //if (filterContext.HttpContext.Request.Cookies[MyCookies.PERSON_SESSION] == null)
            //    cookieExpired = true;
            //load the person session object
            if (filterContext.HttpContext.Session[Sessions.PERSON_SESSION] == null)// || cookieExpired)
            {
                //load the person object for the session
                PersonSession personTemp = PersonSession.jsonToObject(filterContext.HttpContext.User.Identity.Name);
                //adds the person object to sessions
                ETC.BL.Person.PersonBL blPerson = new ETC.BL.Person.PersonBL();
                PersonInformation personInfo = blPerson.GetPersonInformation(personTemp.Email, Utils.Constants.BRAND, "");
                person = Utils.Utils.ConvertPersonEtToPersonSession(personInfo, personInfo.email);
                filterContext.HttpContext.Session.Add(Sessions.PERSON_SESSION, person);
                //HttpCookie cookie = new HttpCookie(Sessions.PERSON_SESSION);
                //cookie.Expires = DateTime.Now.AddMinutes(3);
                //filterContext.HttpContext.Response.Cookies.Add(cookie);
            }
            //else
            //{
            //    /*
            //     * Verify the user accessing is the same as the
            //     * previous user. This prevents logging out from another application
            //     * and logging in thru myaccount with another user, and because of
            //     * maintanance of session for this application (it passes the authorize because the user
            //     * was logged thru myaccount), if user types url for this,
            //     * it will open with the user previously logged out because of the session.
            //     * */
            //    //obtain the actually user in session
            //    person = (PersonCrowdBook)filterContext.HttpContext.Session[Sessions.PERSON_SESSION];
            //    //obtain the user accessing the app
            //    PersonSession personTemp = PersonSession.jsonToObject(filterContext.HttpContext.User.Identity.Name);
            //    //if they are not the same, kick out
            //    if (person.Email != personTemp.Email)
            //    {
            //        filterContext.HttpContext.Session.RemoveAll();
            //        filterContext.HttpContext.Session.Add(Sessions.PERSON_SESSION, personTemp);
            //        //filterContext.HttpContext.Response.RedirectPermanent("/myaccount", true);
            //    }
            //}
            ////Begin CrowdoBookSession
            ////if (filterContext.HttpContext.Session[Sessions.CROWDBOOK_SESSION] == null)
            ////{
            ////    ETC.BL.Person.PersonBL blPerson = new ETC.BL.Person.PersonBL();
            ////    ETC.BL.Groups.GroupBL blGroup = new ETC.BL.Groups.GroupBL();
            ////    PersonInformation personInfo = blPerson.GetPersonInformation(person.Email, Utils.Constants.BRAND, "");
            ////    SessionCrowdbook session = new SessionCrowdbook();
            ////    session.Id = Convert.ToInt64(personInfo.id);
            ////    session.Institution = person.CrowdBookInstitution;
            ////    session.InstitutionAlias = person.CrowdBookInstitution;
            ////    session.MyGroups = blGroup.CountGroupPerson(Convert.ToInt64(person.Id));
            ////    filterContext.HttpContext.Session.Add(Sessions.CROWDBOOK_SESSION, session);
            ////}
            ////else
            ////{
            ////    SessionCrowdbook s = (SessionCrowdbook)filterContext.HttpContext.Session[Sessions.CROWDBOOK_SESSION];
            ////    if (person.Id != s.Id)
            ////    {
            ////        filterContext.HttpContext.Session.RemoveAll();
            ////    }
            ////}
            ////End
            base.OnActionExecuting(filterContext);
        }
    }
}