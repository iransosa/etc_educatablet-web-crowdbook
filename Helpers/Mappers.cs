﻿using System;
using System.Collections.Generic;
using System.Web;
using ETC.Structures.Models;
using ETC.Structures.Models.Groups;
using Web_CrowdBook.ViewModels;
using ETC.Structures.Models.Institutions;

namespace Web_CrowdBook.Helpers
{
    public class Mappers
    {
        #region Group & NewGroupModel
        public static GroupVM GroupToGroupVM(Group source)
        {
            GroupVM result = new GroupVM();
            result.IdGroup = source.IdGroup;
            result.Name = source.Name;
            result.Description = source.Description;
            result.IdGroupType = source.GroupType.IdGroupType;
            result.IdGroupMode = source.GroupMode.IdGroupMode;
            if(source.Institution != null)
            {
                result.IdInstitution = source.Institution.Id;
                result.Institution = source.Institution.Name;
            }
            if (source.Level != null)
            {
                result.IdLevel = source.Level.IDLevel;
                result.Level = source.Level.Description;
            }
            result.Pin = source.Pin;
            result.ClosingDate = source.ClosingDate;
            result.ExpirationDate = source.ExpirationDate;
            return result;
        }
        public static Group GroupVMToGroup(GroupVM source)
        {
            Group result = new Group();
            result.IdGroup = source.IdGroup;
            result.Name = source.Name;
            result.Description = source.Description;
            result.GroupType = new GroupType();
            result.GroupType.IdGroupType = source.IdGroupType;
            result.GroupMode = new GroupMode();
            result.GroupMode.IdGroupMode = source.IdGroupMode;
            if (source.IdInstitution != 0)
            {
                result.Institution = new Institution();
                result.Institution.Id = Convert.ToInt32(source.IdInstitution);
                result.Institution.Name = source.Institution;
            }
            if (source.IdLevel != 0)
            {
                result.Level = new Level();
                result.Level.IDLevel = Convert.ToInt64(source.IdLevel);
            }
            result.Pin = source.Pin;
            result.ClosingDate = source.ClosingDate;
            result.ExpirationDate = source.ExpirationDate;
            return result;
        }
        #endregion
    }
}