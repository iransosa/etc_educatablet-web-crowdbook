﻿using ETC.Structures.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web_CrowdBook.Helpers
{
    public class DataAccess
    {
        #region GetInstitutionLevels
        public static List<System.Web.Mvc.SelectListItem> GetInstitutionLevels(int id)
        {
            ETC.BL.Institutions.IInstitutionBL bl = new ETC.BL.Institutions.InstitutionBL();
            List<ETC.Structures.Models.Institutions.Level> result = bl.GetFieldByInstitution(new Institution() { Id = id });
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var level in result)
            {
                SelectListItem item = new SelectListItem();
                item.Value = level.IDLevel.ToString();
                item.Text = level.Description;
                items.Add(item);
            }
            return items;
        }
        #endregion

        #region GetGenders
        public static List<System.Web.Mvc.SelectListItem> GetGenders()
        {
            ETC.BL.Gender.GenderBL bl = new ETC.BL.Gender.GenderBL();
            List<ETC.Structures.Models.Users.Gender> result = bl.GetGenders();
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var level in result)
            {
                SelectListItem item = new SelectListItem();
                item.Value = level.Abbreviature.ToString();
                item.Text = level.Description;
                items.Add(item);
            }
            return items;
        }
        #endregion

        //public static void GetGroupFile(long idGroup, ETC.BL.Common.FileType fileType)
        //{
        //    System.Drawing.Image img = null;
        //    string imageName = "";
        //    string imageType = "";
        //    string url = "";

        //    ETC.BL.Groups.IGroupBL bl = new ETC.BL.Groups.GroupBL();
        //    ETC.Structures.Models.Groups.GroupFile groupFile = bl.GetGroupFile(idGroup, fileType);
        //    if (groupFile != null)
        //    {
        //        imageName = groupFile.Name;
        //        imageType = groupFile.MimeTpe;
        //    }
        //    if (!string.IsNullOrEmpty(imageName))
        //    {
        //        url = string.Format("{0}/{1}", GetUrlBase(context), imageName);
        //        img = GetImage(url);
        //    }
        //} 
    }
}