﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web_CrowdBook.Helpers
{
    public class DateTimeModelBinder : IModelBinder
    {
        #region Attributes
            public string Date { get; set; }
            public string Time { get; set; }

            public string Month { get; set; }
            public string Day { get; set; }
            public string Year { get; set; }

            public string Hour { get; set; }
            public string Minute { get; set; }
            public string Second { get; set; }

            public bool DateSet { get { return !String.IsNullOrEmpty(Date); } }
            public bool MonthDayYearSet { get { return !(String.IsNullOrEmpty(Month) && String.IsNullOrEmpty(Day) && String.IsNullOrEmpty(Year)); } }

            public bool TimeSet { get { return !String.IsNullOrEmpty(Time); } }
            public bool HourMinuteSecondSet { get { return !(String.IsNullOrEmpty(Hour) && String.IsNullOrEmpty(Minute) && String.IsNullOrEmpty(Second)); } }
        #endregion

        #region Methods
            /// <summary>
            /// Contructor of the class
            /// </summary>
            public DateTimeModelBinder()
            {
            }

            /// <summary>
            /// Serialize the Data in this case a Datetime
            /// </summary>
            /// <param name="controllerContext"></param>
            /// <param name="bindingContext"></param>
            /// <returns></returns>
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                if (bindingContext == null)
                {
                    throw new ArgumentNullException("bindingContext");
                }


                //If the datetime was configured to be in one value
                DateTime? dateTimeAttempt = GetA<DateTime>(bindingContext, "DateTime");
                if (dateTimeAttempt != null)
                {
                    return dateTimeAttempt.Value;
                }

                if (this.MonthDayYearSet == false && this.DateSet == false)
                {
                    this.Date = "Date";
                }

                //If they haven't set Hour, Minute, Second OR Time, set "time" and get ready for an attempt
                if (this.HourMinuteSecondSet == false && this.TimeSet == false)
                {
                    this.Time = "Time";
                }

                //Did they want the Date *and* Time?
                DateTime? dateAttempt = GetA<DateTime>(bindingContext, this.Date);
                DateTime? timeAttempt = GetA<DateTime>(bindingContext, this.Time);

                //build a Time via parts (minutes, second, day)
                if (this.HourMinuteSecondSet)
                {
                    //timeAttempt = new DateTime(
                    //    DateTime.MinValue.Year, DateTime.MinValue.Month, DateTime.MinValue.Day,
                    //    GetA<int>(bindingContext, this.Hour).Value,
                    //    GetA<int>(bindingContext, this.Minute).Value,
                    //    GetA<int>(bindingContext, this.Second).Value);
                    timeAttempt = new DateTime(
                        DateTime.MinValue.Year, DateTime.MinValue.Month, DateTime.MinValue.Day,
                        GetA<int>(bindingContext, this.Hour).Value,
                        GetA<int>(bindingContext, this.Minute).Value,
                        0);
                }

                //builds the datetime with year, month and day
                if (this.MonthDayYearSet)
                {
                    int? year = GetA<int>(bindingContext, this.Year);
                    int? month = GetA<int>(bindingContext, this.Month);
                    int? day = GetA<int>(bindingContext, this.Day);
                    if (year.HasValue && month.HasValue && day.HasValue && year != 0 && month != 0 && day != 0)//Add validation for nulls values
                    {
                        try
                        {
                            dateAttempt = new DateTime(
                                GetA<int>(bindingContext, this.Year).Value,
                                GetA<int>(bindingContext, this.Month).Value,
                                GetA<int>(bindingContext, this.Day).Value,
                                DateTime.MinValue.Hour, DateTime.MinValue.Minute, DateTime.MinValue.Second);
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            string error = string.Concat(bindingContext.ModelMetadata.DisplayName, " ", "invalido");
                            bindingContext.ModelState.AddModelError(bindingContext.ModelName, error);
                        }
                    }
                    else
                        dateAttempt = null;
                }

                //build the two parts of the datetime 
                if (dateAttempt != null && timeAttempt != null)
                {
                    return new DateTime(dateAttempt.Value.Year,
                                dateAttempt.Value.Month,
                                dateAttempt.Value.Day,
                                timeAttempt.Value.Hour,
                                timeAttempt.Value.Minute,
                                timeAttempt.Value.Second);
                }
                //Only got one half? Return as much as we have!
                return dateAttempt ?? timeAttempt;
            }

            /// <summary>
            /// Returns the value of the datatype already casted
            /// </summary>
            /// <typeparam name="T">DataType</typeparam>
            /// <param name="bindingContext">object in the context</param>
            /// <param name="key">String with the datatype</param>
            /// <returns></returns>
            private Nullable<T> GetA<T>(ModelBindingContext bindingContext, string key) where T : struct
            {
                if (bindingContext.ValueProvider.ContainsPrefix(bindingContext.ModelName))
                {
                    if (string.IsNullOrEmpty(key))
                        key = bindingContext.ModelName;
                    else if (Nullable.GetUnderlyingType(bindingContext.ModelType) != null)
                        key = bindingContext.ModelName + ".Value." + key;
                    else
                        key = bindingContext.ModelName + "." + key;
                }
                if (string.IsNullOrEmpty(key)) return null;

                ValueProviderResult value;

                value = bindingContext.ValueProvider.GetValue(key);
                bindingContext.ModelState.SetModelValue(key, value);

                if (value == null)
                {
                    return null;
                }

                return (Nullable<T>)value.ConvertTo(typeof(T));
            }
        #endregion
    }
}