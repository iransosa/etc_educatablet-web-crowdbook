﻿$(document).ready(function () {
    $(".a-dropdown").on("click", function (e) {
        $(this).next(".menu-drop").toggle();
        e.stopPropagation();
    });
    $("body").on("click", function () {
        $('.menu-drop').hide();
    });
    $("#btn-create-g").click(function () {
        $(".menu-drop").hide();
        if ($("#modal-create-group").size() > 0) {
            $("#modal-create-group").modal({ show: true, backdrop: 'static' });
        }
        else {
            $('<div id="modal-create-group" class="modal modal-dym"></div>').appendTo("body");
            $('<div class="modal-loading-s"></div>').appendTo("#modal-create-group");
            $("#modal-create-group").modal({ show: true, backdrop: 'static' });
            $.when(
            $.getScript("/Scripts/jquery.validate.min.js"),
            $.Deferred(function (deferred) {
                $(deferred.resolve);
            })
            ).done(function (script, textStatus) {
                try {
                    $("#modal-create-group").load("/Create/Index #create-group", function () {
                        createGroup.executeCallback = true;
                        createGroup.createLoadAjax();
                    }).delegate(".close-modal", "click", function () {
                        $("#modal-create-group").modal("hide");
                        $(".modal-dym").remove();
                        return false;
                    });
                } catch (e) {
                    document.location = "/Create/Index";
                }
            });
        }
        return false;
    });
});
var createGroup = {
    createLoadAjax: function () {
        createGroup.validation_create();
        $("#btnSaveGroup").click(function () {
            createGroup.create_group();
            return false;
        });
        $(".sh-group").click(function () {
            createGroup.share_group(this);
        });
    },
    createLoadAjaxModal: function () {
        createGroup.validation_create();
    },
    editLoadAjax: function () {
        createGroup.validation_create();
        createGroup.institutionAutocompleteWithLevel();
        $("#btnEditGroup").click(function () {
            createGroup.edit_group();
            return false;
        });
    },
    editLoadAjaxModal: function () {
        createGroup.validation_create();
        createGroup.institutionAutocompleteWithLevel();
    },
    validation_create: function () {
        $("#formCreate").validate({
            rules: {
                "Group.Name": {
                    required: true,
                    minlength: 2,
                    maxlength: 75,
                },
                "Group.Description": {
                    required: true,
                    minlength: 4,
                    maxlength: 150
                },
                "Group.IdGroupType": {
                    required: true,
                    range: [1, 9999999]
                },
                "Group.IdGroupMode": {
                    required: true,
                    range: [1, 9999999]
                }
            },
            messages: {
                "Group.Name": {
                    required: "El Nombre es requerido",
                    minlength: "El Nombre debe tener entre 2 y 75 caracteres",
                    maxlength: "El Nombre debe tener entre 2 y 75 caracteres"
                },
                "Group.Description": {
                    required: "La Descripción es requerida",
                    minlength: "La Descripción debe tener entre 4 y 150 caracteres",
                    maxlength: "La Descripción debe tener entre 4 y 150 caracteres"
                },
                "Group.IdGroupType": {
                    required: "El Tipo de grupo es requerido",
                    range: "El Tipo de grupo es requerido"
                },
                "Group.IdGroupMode": {
                    required: "La Modalidad de grupo es requerida",
                    range: "El Tipo de grupo es requerido"
                }
            },
            errorPlacement: function (error, element) {
                if (1 == 1 || element.attr("name") == "Group.IdGroupType" || element.attr("name") == "Group.IdGroupMode") {
                    var name = $(element).attr("name");
                    $(error).appendTo("#lstErrors");
                }
                else {
                    error.insertAfter(element);
                }
            },
            ignore: []
        });
    },
    valid_create: function () {
        var form = $("#formCreate");
        form.validate({
            rules: {
                "Group.Name": {
                    required: true,
                    minlength: 2,
                    maxlength: 75,
                },
                "Group.Description": {
                    required: true,
                    minlength: 4,
                    maxlength: 150
                },
                "Group.IdGroupType": {
                    required: true,
                    range: [1, 9999999]
                },
                "Group.IdGroupMode": {
                    required: true,
                    range: [1, 9999999]
                }
            },
            messages: {
                "Group.Name": {
                    required: "El Nombre es requerido",
                    minlength: "El Nombre debe tener entre 2 y 75 caracteres",
                    maxlength: "El Nombre debe tener entre 2 y 75 caracteres"
                },
                "Group.Description": {
                    required: "La Descripción es requerida",
                    minlength: "La Descripción debe tener entre 4 y 150 caracteres",
                    maxlength: "La Descripción debe tener entre 4 y 150 caracteres"
                },
                "Group.IdGroupType": {
                    required: "El Tipo de grupo es requerido",
                    range: "El Tipo de grupo es requerido"
                },
                "Group.IdGroupMode": {
                    required: "La Modalidad de grupo es requerida",
                    range: "El Tipo de grupo es requerido"
                }
            },
            errorPlacement: function (error, element) {
                if (1 == 1 || element.attr("name") == "Group.IdGroupType" || element.attr("name") == "Group.IdGroupMode") {
                    var name = $(element).attr("name");
                    $(error).appendTo("#lstErrors");
                }
                else {
                    error.insertAfter(element);
                }
            },
            ignore: []
        });
        return form.valid();
    },
    valid_create_form: function () {
        var isValid = true;
        if (utils.validate_form($("#txName").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txDescription").val()) == false) {
            isValid = false;
        }
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            $("#div-validations").html("");
            return true;
        }
    },
    valid_edit_form: function () {
        var isValid = true;
        if (utils.validate_form($("#txName").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txDescription").val()) == false) {
            isValid = false;
        }
        if (utils.validate_form($("#txInstitution").val()) == false) {
            isValid = false;
        }
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            $("#div-validations").html("");
            if ($("#txInstitution").val().trim() != $("#txInstitutionH").val().trim()) {
                $("#txtInstitutionId").val("");
                $("#txInstitution").val("");
                $("#txInstitutionH").val("");
            }
            return true;
        }
    },
    institutionAutocompleteWithLevel: function () {
        $("#txInstitution").autoCompleteCustom("/AjaxCommon/GetInstitutionsByName", { params: "'offset':" + 1, elementId: "#txInstitutionId", elementText: "#txInstitutionH" });
        $("#txInstitution").focusout(function () {
            createGroup.institutionAutocompleteEvent();
        });
    },
    institutionAutocompleteEvent: function(){
        if ($("#txInstitutionId").val() == "") {
            $("#ddlLevels").html("");
        }
        else {
            utils.ajax('/Ajax/GetInstitutionLevels', "{'institutionId':" + $("#txInstitutionId").val() + "}", function (data) {
                data = data.hasOwnProperty("d") ? data.d : data;
                $("#ddlLevels").html("");
                $(data).each(function () {
                    $("#ddlLevels").append("<option value='" + this.IDLevel + "'>" + this.Description + "</option>");
                });
                $('#ddlLevels').trigger("chosen:updated")
            }, null);
        }
    },
    executeCallback: false,
    create_group_callback: function (result) {
        var data = result.group;
        utils.addMenuMyGroupsValue(result.groupsCount);
        //Render group if it be  in MyGroups
        if ($("#my-groups").size() > 0) {
            var type = $("#my-groups").attr("data-type");
            if (typeof type != "undefined" && type == "1") {
                newGroupHtml = templatesHTML.my_groups.format(data.IdGroup, data.Name, data.Name.removeSymbols(), data.Owner, data.Members, data.Resources);
                $(newGroupHtml).hide().prependTo("#list-groups-main").fadeIn("slow");
            }
        }
    },
    create_group: function () {
        if (createGroup.valid_create() == false) {
            return false;
        }
        if (createGroup.valid_create_form() == false)
            return false;
        else {
            $("#create-g-loadin").show();
            try {
                var values = new Object();
                values.Name = $("#txName").val();
                values.Description = $("#txDescription").val();
                values.IdGroupMode = $("input:radio[name='Group.IdGroupMode']:checked").val();
                values.IdGroupType = $("input:radio[name='Group.IdGroupType']:checked").val();
                var cls = new Object();
                cls.model = values;
                cls = JSON.stringify(cls);
                utils.ajax('/AjaxGroup/CreateGroup', cls, function (result) {
                    $("#create-g-loadin").hide();
                    result = result.hasOwnProperty("d") ? result.d : result;
                    if (result.success == true) {
                        var data = result.group;
                        $("#create").hide();
                        $("#created").show();
                        $("#created-pin").text(data.Pin);
                        var url = "/Group/{1}/{0}".format(data.IdGroup, data.Name.removeSymbols());
                        $("#btn-go-group").attr("href", url);
                        $("#created").attr("data-id", data.IdGroup);
                        $("#created").attr("data-name", data.Name);
                        $("#created").attr("data-description", data.Description);
                        if (createGroup.executeCallback == true) {
                            createGroup.create_group_callback(result);
                        }
                    }
                    else {
                        if (result.errors != null) {
                            alert(result.errors.join("\n"));
                        }
                        else {
                            alert(result.error);
                        }
                    }
                }, function () {
                    $("#create-g-loadin").hide();
                    utils.errorInternal();
                });
            } catch (e) {
                $("#create-g-loadin").hide();
            }
        }
    },
    edit_group_callback: null,
    edit_group: function () {
        if (createGroup.valid_create() == false) {
            return false;
        }
        if (createGroup.valid_edit_form() == false)
            return false;
        else {
            $("#create-g-loadin").show();
            try {
                var values = new Object();
                values.IdGroup = $("#txIdGroup").val();
                values.Name = $("#txName").val();
                values.Description = $("#txDescription").val();
                values.IdInstitution = $("#txInstitutionId").val();
                values.Institution = $("#txInstitution").val();
                values.IdLevel = $("#ddlLevels").val();
                values.IdGroupMode = $("input:radio[name='Group.IdGroupMode']:checked").val();
                values.IdGroupType = $("#txIdGroupType").val();
                var cls = new Object();
                cls.model = values;
                cls = JSON.stringify(cls);
                utils.ajax('/AjaxGroup/EditGroup', cls, function (result) {
                    $("#create-g-loadin").hide();
                    result = result.hasOwnProperty("d") ? result.d : result;
                    if (result.success == true) {
                        $("#edit").hide();
                        $("#edited").show();
                        if (createGroup.edit_group_callback != null) {
                            if (typeof createGroup.edit_group_callback == "function") {
                                createGroup.edit_group_callback(result);
                            }
                        }
                    }
                    else {
                        if (result.errors != null) {
                            alert(result.errors.join("\n"));
                        }
                        else {
                            alert(result.error);
                        }
                    }
                }, function () {
                    $("#create-g-loadin").hide();
                    utils.errorInternal();
                });
            } catch (e) {
                $("#create-g-loadin").hide();
            }
        }
    },
    share_group: function (button) {
        var id = $("#created").attr("data-id");
        var name = $("#created").attr("data-name");
        var description = $("#created").attr("data-description");
        var image = "{0}/Image/Group/{1}?width=220&height=220".format(utils.domain, id);
        var url = "{0}/Group/{1}/{2}".format(utils.domain, id, description.removeSymbols());
        var titleShare = 'He creado un grupo sobre "{0}" en CrowdBook.com, visítalo y únete'.format(name);
        switch ($(button).attr("data-sh")) {
            case "facebook":
                createGroup.share.sh_facebook(url, name, titleShare, description, image);
                break;
            case "twitter":
                createGroup.share.sh_twitter(url, titleShare);
                break;
            case "gplus":
                createGroup.share.sh_gPlus(url);
                break;

        }
    },
    share: {
        sh_facebook: function (url, name, title, description, image) {
            var a = 'https://www.facebook.com/dialog/feed?app_id=145634995501895&display=popup&link={0}&redirect_uri=https://developers.facebook.com/tools/explorer&name={1}&caption={2}&description={3}&picture={4}'.format(url, name, title, description, image);
            utils.openModal(a, 580, 325);
        },
        sh_twitter: function (url, title) {
            utils.openModal('https://twitter.com/share?url=' + url + '&text=' + title, 580, 325);
        },
        sh_gPlus: function (url) {
            utils.openModal('https://plus.google.com/share?url=' + url, 600, 600);
        }
    }
}