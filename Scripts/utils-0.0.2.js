﻿var menuDropEvents = {
    resources: function () {
        $(".dwnRes").on("click", function (e) {
            var menu = $(this).next(".mnDnRes");
            if (menu.is(":visible") == true) {
                $('.menu-drop').hide();
            }
            else {
                $('.menu-drop').hide();
                menu.show("1000");
            }
            e.stopPropagation();
        });
    }
};
var templatesHTML = {
    //IdGroup = 0, Name = 1, Name_Format = 2, Owner = 3, Members = 4, Resources = 5
    my_groups: '<li class="group">' +
                    '<div class="avatar"><a href="/Group/{2}/{0}"><img src="/Image/Group/{0}?width=373&height=373" /></a></div>' +
                    '<div class="detail-group transparent"><h3 class="dName"><a href="/Group/{2}/{0}">{1}</a></h3><strong class="overflow-ellips">{3}</strong><div class="members counters"><b>{4}</b><span>Participantes</span></div><div class="resources counters"><b>{5}</b><span>Recursos</span></div></div><a class="l-down-fill" href="/Group/{2}/{0}"></a></li>',
    my_groups_private: '<li class="group inserted">' +
                    '<div class="avatar"><a href="#"><img src="/Image/Group/{0}?width=373&height=373" /></a></div>' +
                    '<div class="detail-group transparent"><h3 class="dName"><a class="send-request dName" href="#">{1}</a></h3><strong class="overflow-ellips">{3}</strong><div class="members counters"><b>{4}</b><span>Participantes</span></div><div class="resources counters"><b>{5}</b><span>Recursos</span></div><i class="ico_lock"></i></div><a class="send-request dName l-down-fill" href="#"></a></li>',
    //IdProduct = 0, Name = 1, Name_Format = 2, Description = 3
    group_resource: '<li class="resource inserted" data-res="{0}" title="{1}" data-desc="{3}" data-drm="0">' +
                        '<div class="avatar"><img alt="{1}" src="../../Image/Product/{0}?width=150&height=155" /></div>' +
                        '<div class="detail block-transaprent"><h3 class="overflow-ellips">{1}</h3></div><a class="l-down-fill show-res" href="#" title="{1}" rel="ignore" role="button"></a>' +
                    '</li>',
    group_resource_drm: '<li class="resource inserted" data-res="{0}" title="{1}" data-desc="{3}" data-drm="1">' +
                        '<div class="avatar"><img alt="{1}" src="../../Image/Product/{0}?width=150&height=155" /></div>' +
                        '<div class="detail block-transaprent"><h3 class="overflow-ellips">{1}</h3></div><a class="l-down-fill show-res" href="#" title="{1}" rel="ignore" role="button"></a><i class="ico_lock"></i>' +
                    '</li>',
    //IdProduct = 0, Name = 3, Name_Format = 2, Description = 1, Image
    group_resource_preview: {
        normal: '<div id="mod-res-prev" data-id="{0}" class="modal fade"><div class="modal-dialog"><div class="modal-content">' +
                                    '<h4 class="text-center">{1}</h4><div class="form-horizontal">' +
                                    '<div class="avatar"><a href="/Downloads/P/{0}" target="_blank"><img alt="{1}" src="{3}"></a></div>' +
                                    '<p class="description">{2}<br/></p>' +
                                    '<div class="form-buttons"><a href="/Downloads/P/{0}" target="_blank" class="btn btn-success">Descargar</a>' +
                                '</div></div><a class="c-point" data-dismiss="modal"><i class="ico-close"></i></a></div></div></div>',
        with_delete: '<div id="mod-res-prev" data-id="{0}" class="modal fade"><div class="modal-dialog"><div class="modal-content">' +
                                    '<h4 class="text-center">{1}</h4><div class="form-horizontal">' +
                                    '<div class="avatar"><a href="/Downloads/P/{0}" target="_blank"><img alt="{1}" src="{3}"></a></div>' +
                                    '<p class="description">{2}<br/></p>' +
                                    '<div class="form-buttons"><input id="btn-del-res" type="button" class="btn btn-warning" value="Archivar"/>' +
                                    '<a href="/Downloads/P/{0}" target="_blank" class="btn btn-success">Descargar</a>' +
                                '</div></div><a class="c-point" data-dismiss="modal"><i class="ico-close"></i></a></div></div></div>',
        with_drm: '<div id="mod-res-prev" data-id="{0}" class="modal fade"><div class="modal-dialog"><div class="modal-content">' +
                                    '<h4 class="text-center">{1}</h4><div class="form-horizontal">' +
                                    '<div class="avatar"><img alt="{1}" src="{3}"></div>' +
                                    '<p class="description">{2}<br/></p>' +
                                    '<div class="form-buttons"><div class="msg-drm text-center">Producto protegido por derecho de autor. Solo puede ser visualizado desde nuestras aplicaciones móviles para Android o iOS.</div>' +
                                '</div></div><a class="c-point" data-dismiss="modal"><i class="ico-close"></i></a></div></div></div>',
        image: "/Image/Product/{0}?width=150&height=155",
        image_big: "/Image/Product/{0}?width=350&height=155"
    },
    //IdMessage = 0, sender.id = 1, (sender.firstName + sender.fullName) = 2, CreationDate = 3, MessageText = 4, Embed = 5
    muro_editable: '<li class="muro inserted" data-msg="{0}">' +
                        '<div class="avatar round-img"><span class="round-img-inner"><img alt="{2}" src="../../Image/Person/{1}?width=80&height=80" /></span></div>' +
                        '<div class="detail">' +
                            '<h4 class="person">{2}</h4>' +
                            '<div class="message"><p>{4}</p></div><div class="embed">{5}</div>' +
                            '<div class="buttons"><span class="date">{3}</span><a class="delete"><i class="ico-bin"></i></a></div>' +
                        '</div></li>',
    muro: '<li class="muro inserted" data-msg="{0}">' +
                        '<div class="avatar round-img"><span class="round-img-inner"><img alt="{2}" src="../../Image/Person/{1}?width=80&height=80" /></span></div>' +
                        '<div class="detail">' +
                            '<h4 class="person">{2}</h4>' +
                            '<div class="message"><p>{4}</p></div><div class="embed">{5}</div>' +
                            '<div class="buttons"><span class="date">{3}</span></div>' +
                        '</div></li>',
    //ID_PERSON = 0, FirstName = 1, LastName = 2, Institution = 3
    group_member: '<li class="member" data-member="{0}">' +
                        '<div class="detail"><div class="avatar round-img"><span class="round-img-inner"><img alt="{1}" src="../../Image/Person/{0}?width=58&height=58" /></span></div>' +
                            '<div class="person"><h4>{1} {2}</h4><strong>{3}</strong></div></div></li>'
}
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
              ? args[number]
              : match
            ;
        });
    };
}
if (!String.prototype.formatHtml) {
    String.prototype.formatHtml = function () {
        return $('<div/>').text(this).html();
    };
}
if (!String.prototype.removeSymbols) {
    String.prototype.removeSymbols = function () {
        var text = this;        
        var sb = "";
        var charInserted = false;
        for (var i = 0; i < text.length; i++) {
            var caracter = text.charCodeAt(i);
            if ((caracter == 32) || (caracter > 96 && caracter < 123) || (caracter > 47 && caracter < 58) || (caracter > 64 && caracter < 91) || (caracter == 241 || caracter == 209)) {
                if (caracter == 32) {
                    if (!charInserted) {
                        sb = sb + "-";
                        charInserted = true;
                    }
                }
                else {
                    sb = sb + text[i];
                    charInserted = false;
                }
            }
            else {
                if (!charInserted) {
                    sb = sb + "-";
                    charInserted = true;
                }
            }
        }
        return sb;
    };
}
(function ($) {
    $.fn.autoCompleteCustom = function (url, opc_user) {
        options = $.extend($.fn.autoCompleteCustom.opc_default, opc_user);
        var element = this;
        var items = new Array;
        items = [""];
        $(element).typeahead({
            property: options.name,
            minLength: options.minLength,
            items: options.itemsCount,
            source: function (query, process) {
                return $.ajax({
                    url: url,
                    type: "POST",
                    data: options.params != null ? "{'name':'" + query + "'}" : "{'name':'" + query + "'," + options.params + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (options.elementId != null) {
                            $(options.elementId).val("");
                        }
                        if (options.elementText != null) {
                            $(options.elementText).val("");
                        }
                        items = [""];
                        data = data.hasOwnProperty("d") ? data.d : data;
                        $.map(data, function (data) {
                            var group;
                            group = {
                                Id: data[options.id],
                                Name: data[options.name],
                                toString: function () {
                                    return $.fn.autoCompleteCustom.toString(this);
                                },
                                toLowerCase: function () {
                                    return $.fn.autoCompleteCustom.toLowerCase(this);
                                },
                                indexOf: function (string) {
                                    return $.fn.autoCompleteCustom.indexOf(this);
                                },
                                replace: function (string) {
                                    return $.fn.autoCompleteCustom.replace(this, element);
                                }
                            };
                            items.push(group);
                        });
                        process(items);
                    }
                });
            },
            onselect: function (obj) {
                alert('Selected ' + obj);
            },
            updater: function (item) {
                return $.fn.autoCompleteCustom.updater(item, element);
            }
        });
    };
    $.fn.autoCompleteCustom.opc_default = {
        id: "Id",
        name: "Name",
        minLength: 2,
        itemsCount: 7,
        paramName: "name",
        params: new Object(),
        elementId: null,
        elementText: null
    };
    $.fn.autoCompleteCustom.updater = function (item, element) {
        item = JSON.parse(item);
        if (options.elementId != null) {
            $(options.elementId).val(item[options.id]);
        }
        if (options.elementText != null) {
            $(options.elementText).val(item[options.name]);
        }
        return item[options.name];
    };
    $.fn.autoCompleteCustom.toString = function (obj) {
        return JSON.stringify(obj);
    };
    $.fn.autoCompleteCustom.toLowerCase = function (obj) {
        return obj[options.name].toLowerCase();
    };
    $.fn.autoCompleteCustom.indexOf = function (obj) {
        return String.prototype.indexOf.apply(obj[options.name], arguments);
    };
    $.fn.autoCompleteCustom.replace = function (obj, input) {
        var wrapItem = $("<div></div>").css({ width: input.innerWidth() + "px" });
        wrapItem.html(obj[options.name]);
        return String.prototype.replace.apply(wrapItem.get(0).outerHTML, arguments);
    };
})(jQuery);
/* ------------------- ScrollInfinity ------------------- */
(function ($) {
    var executeBefore = 200;//Los metodos se ejecutan tantos pixeles antes de llegar a su fin segun este valor
    $.fn.scrollInfinity = function (classElements, options_user) {
        sender = this;
        options = $.extend($.fn.scrollInfinity.options, options_user);
        $(classElements).addClass("isInfinity");
        if (options.addClassInfinity == true) {
            $("<style type='text/css'> .infinity-loading{ background: url('/Images/loading_large.gif') no-repeat bottom right; } </style>").appendTo("head");
        }
        $(sender).bind("scroll.scrollInfinity", function () {
            $.fn.scrollInfinity_scroll(this, classElements);
        });
    };
    $.fn.scrollInfinity_scroll = function (elementParent, classElements) {
        var elementParentHeight = $(elementParent).height();
        $(classElements).each(function (index, currentElement) {
            if ($(currentElement).hasClass("isInfinity") == false) {
                return;
            }
            if (typeof $(currentElement).attr("data-infinity") != "undefined") {
                return;
            }
            else {
                var wrapPositionTop = $.fn.scrollInfinity.setPositionScroll(currentElement);
                if (($(document).scrollTop() + elementParentHeight + executeBefore) >= wrapPositionTop) {
                    var funct = $(currentElement).attr("data-infinity-method");
                    utils.executeFunctionByName(funct, window);
                    $(currentElement).attr("data-infinity", 1);
                    $(currentElement).addClass("infinity-loading");
                }
            }
        });
    }
    $.fn.scrollInfinity.options = {
        attrExecuting: "data-infinity",
        addClassInfinity: true
    };
    $.fn.scrollInfinity.setPositionScroll = function (element) {
        return $(element).offset().top + $(element).height();
    };
    $.fn.scrollInfinity.callBack = function (element) {
        $(element).removeAttr("data-infinity");
        $(element).removeClass("infinity-loading");
    };
    $.fn.scrollInfinity.unbind = function (element) {
        $(element).removeClass("isInfinity");
    },
    $.fn.scrollInfinity.getSelector = function (element) {
        var selector = $(element).selector;
        if (typeof selector !== undefined) {
            return selector != "" ? selector : "0";
        }
        return "0";
    }
})(jQuery);
/* ----------------------- Utils ----------------------- */
var groupModeTypes = {
    PUBLIC: "PUBLIC",
    VISIBLE_PRIVATE: "PRIVATE_VISIBLE",
    HIDDEN_PRIVATE: "PRIVATE_HIDDEN"
};
var groupMemberStates = {
    ACTIVE: "ACTIVE",
    PENDING: "PENDING",
    DELETED: "DELETED"
};
var fileType = {
    PUBLISHED_BOOK: 1,
    COVER: 2,
    STUDY_GUIDE: 3,
    AVATAR: 4
};
var utils = {
    domain: "http://www.crowdbook.com",
    allowed_image: ["jpg", "jpeg", "gif", "png"],
    allowed_resource_extensions: ["pdf", "epub", "doc", "docx", "xls", "xlsx", "ppt", "pptx", "png", "jpg", "jpeg"],
    validImageExension: function (fileName) {
        var extension = fileName.split('.').pop().toLowerCase();
        if (utils.allowed_image.indexOf(extension) < 0) {
            alert("¡Solo se permiten imagenes en formato " + utils.allowed_image.join(", "));
            return false;
        }
        else
            return true;
    },
    validCoverExension: function (fileName) {
        var extension = fileName.split('.').pop().toLowerCase();
        if (utils.allowed_image.indexOf(extension) < 0) {
            alert("¡El Cover debe ser en formato " + utils.allowed_image.join(", "));
            return false;
        }
        else
            return true;
    },
    validResourceExension: function (fileName) {
        var extension = fileName.split('.').pop().toLowerCase();
        if (utils.allowed_resource_extensions.indexOf(extension) < 0) {
            alert("¡Solo se permiten recursos con formato " + utils.allowed_resource_extensions.join(", "));
            return false;
        }
        else
            return true;
    },
    info: function (message) {
        if ($("#dMessageInfo").size() == 0) {
            $(document).append('<div id="dMessageInfo" class="alert alert-info"><button type="button" class="close" onclick="$(\"#dMessageInfo\").hide(\"slow\")">&times;</button>{0}</div>'.format(message));
        }
        else {
            $(document).append('<button type="button" class="close" data-dismiss="alert">&times;</button>{0}'.format(message));
        }
    },
    errorInternal: function () {
        alert("Ha ocurrido un error interno");
    },
    resourceWithDRM: function () {
        alert("Este recurso está protegido por derechos de autor y solo puede ser visualizado desde nuestras aplicaciones móviles. \nDescárgate la versión para Android o Iphone");
        return false;
    },
    executeFunctionByName: function (functionName, context /*, args */) {
        var args = [].slice.call(arguments).splice(2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(this, args);
        //window["functionName"](arguments);
        //window["My"]["Namespace"]["functionName"](arguments);
    },
    showModalLoading: function (isStatic) {
        if (isStatic == true) {
            $("#modal-loading").modal({ show: true, backdrop: 'static' });
        }
        else {
            $("#modal-loading").modal();
        }
    },
    hideModalLoading: function () {
        $("#modal-loading").modal("hide");
    },
    showMessageSuccess: function (message) {
        if ($("#dMessageInfo").size() > 0) {
            var m = $('<div id="dMessageInfo" class="alert alert-success">');
            m.appendTo("body");
        }
        $("#dMessageInfo").html('<button type="button" class="close" onclick="$(\'#dMessageInfo\').hide(\'slow\')">&times;</button>' + message);
    },
    scrollTo: function (element) {
        $('html,body').animate({
            scrollTop: $(element).offset().top - 5
        }, 'slow');
    },
    validate_form: function (str) {
        if (str.match(/<|>/) == null)
            //if (str.match(/([\<])([^\>]{1,})*([\>])/i) == null)
            return true;
        else
            return false;
    },
    getHeadersAntiForgetTonek: function () {
        var token = $('[name=__RequestVerificationToken]').val();
        var headers = {};
        headers["__RequestVerificationToken"] = token;
        return headers;
    },
    ajax: function (url, data, sucess, error) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: utils.getHeadersAntiForgetTonek(),
            success: sucess,
            error: error
        });
    },
    ajaxSync: function (url, data, sucess, error) {
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            async: false,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: utils.getHeadersAntiForgetTonek(),
            success: sucess,
            error: error
        });
    },
    htmlNotLet: function () {
        return "Se detectó contenido no válido. Evite el uso de caracteres como '< o >'.";
    },
    replaceURLWithHTMLLinks: function (text) {
        var re = /(\(.*?)?\b(((?:https?|ftp|file):\/\/|www.)[-a-z0-9+&@#\/%?=~_()|!:,.;]*[-a-z0-9+&@#\/%=~_()|])/ig;
        //var re = /(\(.*?)?\b((?:https?|ftp|file):\/\/[-a-z0-9+&@#\/%?=~_()|!:,.;]*[-a-z0-9+&@#\/%=~_()|])/ig;
        return text.replace(re, function (match, lParens, url) {
            var rParens = '';
            lParens = lParens || '';
            // Try to strip the same number of right parens from url
            // as there are left parens.  Here, lParenCounter must be
            // a RegExp object.  You cannot use a literal
            //     while (/\(/g.exec(lParens)) { ... }
            // because an object is needed to store the lastIndex state.
            var lParenCounter = /\(/g;
            while (lParenCounter.exec(lParens)) {
                var m;
                // We want m[1] to be greedy, unless a period precedes the
                // right parenthesis.  These tests cannot be simplified as
                //     /(.*)(\.?\).*)/.exec(url)
                // because if (.*) is greedy then \.? never gets a chance.
                if (m = /(.*)(\.\).*)/.exec(url) ||
                        /(.*)(\).*)/.exec(url)) {
                    url = m[1];
                    rParens = m[2] + rParens;
                }
            }
            var href = url.substring(0, 4) == "www." ? "http://" + url : url;
            return lParens + "<a class='lFormated' target='_blank' href='" + href + "'>" + url + "</a>" + rParens;
        });
    },
    formatUrlWithLink: function (elementHtml) {
        try {
            var wrap = $("<div>");
            wrap.html(elementHtml);
            var html = $(".message", wrap).html();
            html = utils.replaceURLWithHTMLLinks(html);
            $(".message p", wrap).replaceWith(html);
            elementHtml = wrap.html();
        } catch (e) { }
        return elementHtml;
    },
    addMenuProduct: function () {
        if ($("#mnSRes").size() > 0) {
            var totalRes = parseInt($("#mnSRes").html());
            totalRes = totalRes > 0 ? totalRes + 1 : 0;
            $("#mnSRes").html(totalRes);
        }
    },
    subtractMenuProduct: function () {
        if ($("#mnSRes").size() > 0) {
            var totalRes = parseInt($("#mnSRes").html());
            totalRes = totalRes > 0 ? totalRes - 1 : 0;
            $("#mnSRes").html(totalRes);
        }
    },
    addMenuMyGroups: function () {
        if ($("#mnSGroups").size() > 0) {
            var totalRes = parseInt($("#mnSGroups").html());
            totalRes = totalRes > 0 ? totalRes + 1 : 0;
            $("#mnSGroups").html(totalRes);
        }
    },
    addMenuMyGroupsValue: function (count) {
        if ($("#mnSGroups").size() > 0) {
            //var totalRes = parseInt($("#mnSGroups").html());
            //totalRes = totalRes > 0 ? totalRes + 1 : 0;
            $("#mnSGroups").html(count);
        }
    },
    subtractMenuMyGroups: function () {
        if ($("#mnSGroups").size() > 0) {
            var totalRes = parseInt($("#mnSGroups").html());
            totalRes = totalRes > 0 ? totalRes - 1 : 0;
            $("#mnSGroups").html(totalRes);
        }
    },
    openModal: function (url, width, height) {
        var left = (screen.width / 2) - (width / 2);
        var top = (screen.height / 2) - (height / 2);
        window.open(url, '', 'toolbar=no, menubar=no, scrollbars=yes, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left, resizable = false);
    },
    getUrlYoutube: function (message) {
        try {
            var indexYoutubeUrl = message.indexOf("www.youtube.com/watch?v");
            if (indexYoutubeUrl != -1) {
                var url = "";
                var msg1 = message.substring(indexYoutubeUrl, message.length);
                if (msg1.indexOf(" ") != -1) {
                    url = msg1.substring(0, msg1.indexOf(" "));
                }
                else {
                    url = message;
                }
                url = utils.extractUrlYoutube(url);
                return '<object width="350" height="220" data="{0}"></object>'.format(url);
            }
            return "";             
        } catch (e) {
            return "";
        }
    },
    extractUrlYoutube: function (url) {
        //var url = "https://www.youtube.com/watch?embed=x4Bs7u61AMQ";
        var video_id = url.split('?v=')[1];
        var ampersandPosition = video_id.indexOf('&');
        if (ampersandPosition != -1) {
            video_id = video_id.substring(0, ampersandPosition);
        }
        return "http://www.youtube.com/embed/{0}".format(video_id);
    }
};
var common = {
    institutionAutocompleteWithLevel: function () {
        $("#txtInstitution").autoCompleteCustom("/Ajax/GetInstitutionsByName", { params: "'offset':" + 1, elementId: "#txtInstitutionId" });
        $("#txtInstitution").focusout(function () {
            if ($("#txtInstitutionId").val() == "") {
                $("#ddlLevels").html("");
            }
            else {
                utils.ajax('../../Ajax/GetInstitutionLevels', values, function (data) {
                    data = data.hasOwnProperty("d") ? data.d : data;
                    $("#ddlLevels").html("");
                    $(data).each(function () {
                        $("#ddlLevels").append("<option value='" + this.IDLevel + "'>" + this.Description + "</option>");
                    });
                    $('#ddlLevels').trigger("chosen:updated")
                }, null);
            }
        });
    }
};
var cropImage = {
    instance: null,
    minWidthCover: 315,
    maxWidthCover: 630,
    minHeightCover: 100,
    maxHeightCover: 200,
    avatar: {

    },
    avatar: {
        minWidth: 100,
        minHeight: 100,
        maxWidth: 400,
        maxHeight: 400,
    },
    cover: {
        minWidth: 315,
        minHeight: 100,
        maxWidth: 630,
        maxHeight: 200,
    },
    load: function () {
        var isAvatar = cropImage.isAvatar();
        if (isAvatar == null)
            utils.errorInternal();
        cropImage.init();
        $('#modal-image-crop').modal({ show: true, backdrop: 'static' });
        //Get the image size
        var imgWidth = $("#imageCropped").outerWidth();
        var imgHeight = $("#imageCropped").outerHeight();
        var x1, x2, y1, y2, yAprox, minWidth, minHeight, maxWidth, maxHeight, aspectRatio;
        if (isAvatar == false) {
            //If "cropMin" is less that imgWidth then use the cropMin else the size of image
            x2 = cropImage.cover.minWidth <= imgWidth ? cropImage.cover.minWidth : imgWidth;
            //var y2 = cropImage.cover.minHeight <= imgHeight ? cropImage.cover.minHeight : imgHeight;
            yAprox = (x2 * 26) / 100; //The height of Image is 26% of its width
            y2 = yAprox <= imgHeight ? yAprox : imgHeight;
            minWidth = 95.5;
            minHeight = 25;
            maxWidth = cropImage.cover.maxWidth;
            maxHeight = cropImage.cover.maxHeight;
            aspectRatio = "11.3:3";
        }
        else {
            if (imgWidth <= imgHeight) {
                x2 = imgWidth - 10;
                y2 = imgWidth - 10;
            }
            else {
                x2 = imgHeight - 10;
                y2 = imgHeight - 10;
            }
            minWidth = 25;
            minHeight = 25;
            maxWidth = cropImage.avatar.maxWidth;
            maxHeight = cropImage.avatar.maxHeight;
            aspectRatio = "1:1";
        }
        //Center the image vertically and horizontally
        x1 = (imgWidth - x2) / 2;
        if (x1 > 0) {
            x2 = x2 + x1;
        }
        else
            x1 = 0;
        y1 = (imgHeight - y2) / 2;
        if (y1 > 0) {
            y2 = y2 + y1;
        }
        else
            y1 = 0;
        cropImage.setValues(x1, y1, x2, y2, minWidth, minHeight, maxWidth, maxHeight, aspectRatio);
        ////Set the selection
        //cropImage.instance.setSelection(x1, y1, x2, y2, true);
        //cropImage.loadInputs(x1, y1, x2, y2);
        ////Now the minWidth and minHeight are established           
        //cropImage.instance.setOptions({ minWidth:95.5, minHeight:25, maxWidth: cropImage.cover.maxWidth, maxHeight: cropImage.cover.maxHeight });
        //cropImage.instance.update();
    },
    setValues: function (x1, y1, x2, y2, minWidth, minHeight, maxWidth, maxHeight, aspectRatio) {
        //Set the selection
        cropImage.instance.setSelection(x1, y1, x2, y2, true);
        cropImage.loadInputs(x1, y1, x2, y2);
        //Now the minWidth and minHeight are established           
        cropImage.instance.setOptions({ minWidth: minWidth, minHeight: minHeight, maxWidth: maxWidth, maxHeight: maxHeight, aspectRatio: aspectRatio });
        cropImage.instance.update();
    },
    init: function () {
        cropImage.instance = $('#imageCropped').imgAreaSelect({
            instance: true, persistent: true, minWidth: 95.5, minHeight: 25,
            aspectRatio: '1:1', x1: 0, y1: 0,
            onSelectEnd: function (img, selection) {
                cropImage.loadInputs(selection.x1, selection.y1, selection.x2, selection.y2);
            }
        });
    },
    loadInputs: function (x1, y1, x2, y2) {
        $('#cropX1').val(x1);
        $('#cropY1').val(y1);
        $('#cropX2').val(x2);
        $('#cropY2').val(y2);
    },
    isAvatar: function () {
        if ($("#imageCropped").attr("data-crop-type") == "1")
            return true;
        else if ($("#imageCropped").attr("data-crop-type") == "0")
            return false;
        else
            return null;
    }
};
var uploadFilesValid = {
    eventHandler: function (id) {
        var control = document.getElementById(id);
        control.addEventListener("change", function (event) {
            var i = 0,
                files = control.files,
                len = files.length;
            for (; i < len; i++) {
                console.log("Filename: " + files[i].name);
                console.log("Type: " + files[i].type);
                console.log("Size: " + files[i].size + " bytes");
            }
            var blah = new FileReader();
            blah.onload = function (currentFile) {
                var resp = false;
                var pieces = files[0].name.split('.');
                var mime = pieces[pieces.length - 1];
                //console.log(currentFile.target.result);
                if (files[0].type === 'image/jpeg')
                    resp = (currentFile.target.result.substring(0, 3) === 'ÿØÿ');
                else if (files[0].type === 'image/png')
                    resp = (currentFile.target.result.substring(0, 4) === 'PNG');
                else if (files[0].type === 'application/pdf')
                    resp = (currentFile.target.result.substring(0, 4) == '%PDF');
                else if (files[0].type === 'image/jpg')
                    resp = (currentFile.target.result.substring(0, 4) === 'ÿØÿ');
                else if (mime === 'epub')
                    resp = (currentFile.target.result.substring(0, 2) === 'PK');
                else if (files[0].type === 'application/msword')
                    resp = (currentFile.target.result.substring(0, 2) === 'ÐÏ');
                else if (files[0].type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                    resp = (currentFile.target.result.substring(0, 2) === 'PK');
                else if (files[0].type === 'application/vnd.ms-excel')
                    resp = (currentFile.target.result.substring(0, 2) === 'ÐÏ');
                else if (files[0].type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                    resp = (currentFile.target.result.substring(0, 2) === 'PK');
                else if (files[0].type === "application/vnd.ms-powerpoint")
                    resp = (currentFile.target.result.substring(0, 2) === 'ÐÏ');
                else if (files[0].type === "application/vnd.openxmlformats-officedocument.presentationml.presentation")
                    resp = (currentFile.target.result.substring(0, 2) === 'PK');
                if (!resp) {
                    uploadFilesValid.clearFileInput(control);
                    alert("Solo se permiten recursos con formato: " + utils.allowed_resource_extensions.join(", "));
                }
            };
            console.log(blah.readAsBinaryString(files[0]));

        }, false);
        if (history.forward(1)) {
            location.replace(history.forward(1));
        }
    },
    clearFileInput: function (ctrl) {
        try {
            var control = document.getElementById("fileResource");
            control.value = null;
            $(".file-input-name").html('')
            ctrl.value = null;

        } catch (ex) { }
        if (ctrl.value) {
            ctrl.parentNode.replaceChild(ctrl.cloneNode(true), ctrl);
        }
    }
};
var resourcePreview = {
    load: function (id, title, description, drm, enableDelete) {
        if ($("#mod-res-prev").size() > 0)
            $("#mod-res-prev").remove();
        var item, img, hasDescription;
        if (description != null && description != "") {
            img = templatesHTML.group_resource_preview.image.format(id);
            hasDescription = true;
        }
        else {
            img = templatesHTML.group_resource_preview.image_big.format(id);
            hasDescription = false;
        }
        if (drm == "1")
            item = templatesHTML.group_resource_preview.with_drm.format(id, title, description, img);
        else {
            if(enableDelete == true)
                item = templatesHTML.group_resource_preview.with_delete.format(id, title, description, img);
            else
                item = templatesHTML.group_resource_preview.normal.format(id, title, description, img);
        }
        var modal = $(item);
        if (hasDescription == false) {
            $(".modal-content", modal).addClass("big");
            $(".description", modal).remove();
        }
        modal.appendTo("body");
        //$("body").append(item);
        $("#btn-del-res").on("click", function () {
            resourcePreview.deleteResourceFromDashboard();
        });
        $("#mod-res-prev").modal();
    },
    eventHandler: function () {
        $(".resource.inserted").each(function () {
            $(this).removeClass("inserted");
            $(".show-res", this).on("click", function () {
                var parent = $(this).parent(".resource");
                var id = parent.attr("data-res");
                var title = parent.attr("title");
                var description = parent.attr("data-desc");
                var drm = parent.attr("data-drm");
                var deleteResource = $("#txtDelRes").val();
                resourcePreview.load(id, title, description, drm, (deleteResource == "1" ? true : false));
                return false;
            });
        });
    },
    deleteResourceFromDashboard: function () {
        if (confirm("¿Esta seguro que desea eliminar el Recurso?") == false)
            return;
        var id = $("#mod-res-prev").attr("data-id");
        $("#btn-del-res").attr("disabled", "disabled");
        $(".form-buttons", "#mod-res-prev").addClass("loading");
        var values = new Object();
        values.group = $("#txtIdGroup").val();
        values.id = id;
        values = JSON.stringify(values);
        utils.ajax('../../Ajax/DeleteGroupResource', values, function (result) {
            result = result.hasOwnProperty("d") ? result.d : result;
            if (result.success == true) {
                $("#mod-res-prev").modal("hide");
                //Remove de items in screen
                var elements = ".resource[data-res='{0}']".format(result.id);
                $(elements).remove();
                //Set the number of resources and add data-zero if there's no more data
                if($("#stResources").size() > 0)
                    $("#stResources").text(result.productsCount);
                utils.subtractMenuProduct();
                group.refreshResources();
                if ($("li.resource", "#list-resource").size() < 1)
                    $("#modal-resource-more").addClass("data-zero");
                if ($("li.resource", "#list-resource-min").size() < 2)
                    $("#list-resource-min").addClass("data-zero");
            }
            else {
                $("#btn-del-res").removeAttr("disabled");
                $(".form-buttons", "#mod-res-prev").removeClass("loading");
                utils.errorInternal();
            }
        }, function () {
            $("#btn-del-res").removeAttr("disabled");
            $(".form-buttons", "#mod-res-prev").removeClass("loading");
            utils.errorInternal();
        });
    },
};
/* ----------------------- Effects ----------------------- */
jQuery.fn.fadeOutAndRemove = function (speed) {
    $(this).fadeOut(speed, function () {
        $(this).remove();
    })
};