﻿$(document).ready(function () {
    change_password.load();
    $("input[type='password']").keypress(function () {
        if (typeof $(this).attr("data-encrypt") != "undefined") {
            $(this).val("");
        }
        $(this).removeAttr("data-encrypt");
    });
});
var change_password = {
    load: function () {
        $("#formProfile").validate({
            rules: {
                "CurrentPassword": {
                    required: true
                },
                "Password": {
                    required: true,
                    minlength: 8,
                    maxlength: 500
                },
                "PasswordConfirm": {
                    equalTo: "#txPass",
                    minlength: 8,
                    maxlength: 500
                }
            },
            messages: {
                "CurrentPassword": {
                    required: "La Contraseña actual es requerida"
                },
                "Password": {
                    required: "La Contraseña es requerida",
                    minlength: "La Contraseña nueva debe tener entre 8 y 20 caracteres",
                    maxlength: "La Contraseña nueva debe tener entre 8 y 20 caracteres"
                },
                "PasswordConfirm": {
                    equalTo: "Las Contraseñas no coinciden"
                }
            },
            errorElement: 'div',
            errorPlacement: function (error, element) {
                var name = $(element).attr("name");
                $(error).appendTo("#div-validations");
                //Remove message written in server
                var cls = "li[data-p='{0}']".format(name);
                if ($(cls, "#validPost").size() > 0) {
                    $(cls, "#validPost").remove();
                }
            },
            success: function () {
                //Remove message written in server
                var cls = "li[data-p='{0}']".format(name);
                if ($(cls, "#validPost").size() > 0) {
                    $(cls, "#validPost").remove();
                }
            }
        })
    },
    validate_change_pass: function () {
        var isValid = true;
        $("input[type='text']", "#formProfile").each(function () {
            if (utils.validate_form($(this).val()) == false) {
                isValid = false;
            }
        });
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            if ($("#txPass").val() == $("#txPassConfirm").val()) {
                $("#div-validations").html("");
                change_password.encriptPasswords();
                return true;
            }
            else
                return false;
        }
        change_password.encriptPasswords();
    },
    encriptPasswords: function () {
        var pkey = $("#txtEncrK").val().split(',');
        var rsa = new RSAKey();
        rsa.setPublic(pkey[1], pkey[0]);
        if ($("#txPass").val() != "") {
            try {
                if (typeof $("#txPass").attr("data-encrypt") == "undefined") {
                    if ($("#txPass").val().length > 20) {
                        $("#txPass").val("");
                    }
                    else {
                        var passRegister = rsa.encrypt($("#txPass").val());
                        $("#txPass").val(passRegister);
                        $("#txPassConfirm").val(passRegister);
                        $("#txPass").attr("data-encrypt", "1");
                    }
                }
            } catch (e) {
                $("#txPass").val("");
                $("#txPassConfirm").val("");
            }
        }
        if ($("#txCurrentPass").val() != "") {
            try {
                if (typeof $("#txCurrentPass").attr("data-encrypt") == "undefined") {
                    if ($("#txCurrentPass").val().length > 20) {
                        $("#txCurrentPass").val("");
                    }
                    else {
                        var passRegister = rsa.encrypt($("#txCurrentPass").val());
                        $("#txCurrentPass").val(passRegister);
                        $("#txCurrentPass").attr("data-encrypt", "1");
                    }
                }
            } catch (e) {
                $("#txCurrentPass").val("");
            }
        }
    }
}