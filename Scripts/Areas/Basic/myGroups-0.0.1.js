﻿$(document).ready(function () {
    $("#list-groups-main").attr("data-infinity-method", "myGroups.getMyGroups");
    $("#list-groups-main").addClass("infinityWindow");
    $("#list-groups-main").attr("data-page", $("#txtPage").val());
    $(window).scrollInfinity(".infinityWindow", { addClassInfinity: false });
});
var myGroups = {
    getMyGroups: function (callback) {
        $("#list-items").addClass("loading-items");
        var element = $("#list-groups-main");
        var values = new Object();
        values.itemsCurrent = $("li.group", "#list-groups-main").size();
        values = JSON.stringify(values);
        utils.ajax('../../AjaxGroup/GetMyGroups', values, function (result) {
            try {
                result = result.hasOwnProperty("d") ? result.d : result;
                element.attr("data-page", result.page);
                var data = result.result;
                if (data == null || data.length == 0)
                    $.fn.scrollInfinity.unbind("#list-groups-main");
                else {
                    var items = [];
                    for (var i = 0; i < data.length; i++) {
                        items.push(templatesHTML.my_groups.format(data[i].IdGroup, data[i].Name, data[i].Name.removeSymbols(), data[i].Owner, data[i].Members, data[i].Resources));
                    }
                    $(items.join("")).hide().appendTo("#list-groups-main").fadeIn("slow");
                }
            } catch (e) { }
            $("#list-items").removeClass("loading-items");
            $.fn.scrollInfinity.callBack("#list-groups-main");
        },
        function (result) {
            $("#list-items").removeClass("loading-items");
            $.fn.scrollInfinity.callBack("#list-groups-main");
        });
    }
}