﻿$(document).ready(function () {
    account.load();
    $("input[type='password']").keypress(function () {
        if (typeof $(this).attr("data-encrypt") != "undefined") {
            $(this).val("");
        }
        $(this).removeAttr("data-encrypt");
    });
});
var account = {
    load: function () {
        $("#formRegister").validate({
            rules: {
                "Register.Email": {
                    required: true,
                    email: true
                },
                "Register.Password": {
                    required: true,
                    minlength: 8,
                    maxlength: 500
                },
                "Register.PasswordConfirm": {
                    equalTo: "#txPassRegister",
                    minlength: 8,
                    maxlength: 500
                },
                "Register.FirstName": {
                    required: true,
                    minlength: 1,
                    maxlength: 100
                },
                "Register.LastName": {
                    required: true,
                    minlength: 1,
                    maxlength: 100
                }
            },
            messages: {
                "Register.Email": {
                    required: "El Email es requerido",
                    email: "El Email no es válido"
                },
                "Register.Password": {
                    required: "La Contraseña es requerida",
                    minlength: "La Contraseña debe tener entre 8 y 20 caracteres",
                    maxlength: "La Contraseña debe tener entre 8 y 20 caracteres"
                },
                "Register.PasswordConfirm": {
                    equalTo: "Las Contraseñas no coinciden"
                },
                "Register.FirstName": {
                    required: "El Nombre es requerido",
                    minlength: "El Nombre debe tener entre 1 y 100 caracteres",
                    maxlength: "El Nombre debe tener entre 1 y 100 caracteres"
                },
                "Register.LastName": {
                    required: "El Apellido es requerido",
                    minlength: "El Apellido debe tener entre 1 y 100 caracteres",
                    maxlength: "El Apellido debe tener entre 1 y 100 caracteres"
                }
            },
            errorPlacement: function (error, element) {
                var name = $(element).attr("name");
                $(error).appendTo("#div-validations");
                //Remove message written in server
                var cls = "li[data-p='{0}']".format(name);
                if ($(cls, "#validPost").size() > 0) {
                    $(cls, "#validPost").remove();
                }
            },
            success: function () {
                //Remove message written in server
                var cls = "li[data-p='{0}']".format(name);
                if ($(cls, "#validPost").size() > 0) {
                    $(cls, "#validPost").remove();
                }
            }
        })
    },
    validate_register: function () {
        var isValid = true;
        $("input[type='text']", "#formRegister").each(function () {
            if (utils.validate_form($(this).val()) == false) {
                isValid = false;
            }
        });
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            if ($("#txPassRegister").val() == $("#txPassRegisterConfirm").val()) {
                $("#div-validations").html("");
                account.encriptPasswords();
                return true;
            }
            else
                return false;
        }
        account.encriptPasswords();
    },
    validate_login: function () {
        var isValid = true;
        $("input[type='text']", "#formLogin").each(function () {
            if (utils.validate_form($(this).val()) == false) {
                isValid = false;
            }
        });
        if (isValid == false) {
            $("#div-validations-login").html(utils.htmlNotLet());
            return false;
        }
        else {
            $("#div-validations-login").html("");
            account.encriptPasswords();
            return true;
        }
        account.encriptPasswords();
    },
    encriptPasswords: function () {
        var pkey = $("#txtEncrK").val().split(',');
        var rsa = new RSAKey();
        rsa.setPublic(pkey[1], pkey[0]);
        if ($("#txPassLogin").val() != "") {
            try {
                if (typeof $("#txPassLogin").attr("data-encrypt") == "undefined") {
                    if ($("#txPassLogin").val().length > 20) {
                        $("#txPassLogin").val("");
                    }
                    else {
                        var passLogin = rsa.encrypt($("#txPassLogin").val());
                        $("#txPassLogin").val(passLogin);
                        $("#txPassLogin").attr("data-encrypt", "1");
                    }
                }
            } catch (e) {
                $("#txPassLogin").val("");
            }
        }
        if ($("#txPassRegister").val() != "") {
            try {
                if (typeof $("#txPassRegister").attr("data-encrypt") == "undefined") {
                    if ($("#txPassRegister").val().length > 20) {
                        $("#txPassRegister").val("");
                    }
                    else {
                        var passRegister = rsa.encrypt($("#txPassRegister").val());
                        $("#txPassRegister").val(passRegister);
                        $("#txPassRegisterConfirm").val(passRegister);
                        $("#txPassRegister").attr("data-encrypt", "1");
                    }
                }
            } catch (e) {
                $("#txPassRegister").val("");
                $("#txPassRegisterConfirm").val("");
            }
        }
    }
}