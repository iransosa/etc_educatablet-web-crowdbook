﻿$(document).ready(function () {
    $(".goto-first").click(function (e) {
        e.preventDefault();
        utils.scrollTo("#block-first");
    });
    $(".goto-second").click(function (e) {
        e.preventDefault();
        utils.scrollTo("#block-second");
    });
    $(".goto-third").click(function (e) {
        e.preventDefault();
        utils.scrollTo("#block-third");
    });
    $(".goto-fourth").click(function (e) {
        e.preventDefault();
        utils.scrollTo("#block-fourth");
    });
});
var start = {
    validate_form: function () {
        var isValid = true;
        $("input[type='text']", "#register").each(function () {
            if (utils.validate_form($(this).val()) == false) {
                isValid = false;
            }
        });
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            if ($("#txPassRegister").val() == $("#txPassRegisterConfirm").val()) {
                $("#div-validations").html("");
                start.encriptPasswords();
                return true;
            }
            else {
                $("#div-validations").html("Las contraseñas no coinciden");
                return false;
            }
        }
        start.encriptPasswords();
    },
    encriptPasswords: function () {
        var pkey = $("#txtEncrK").val().split(',');
        var rsa = new RSAKey();
        rsa.setPublic(pkey[1], pkey[0]);
        if ($("#txPassRegister").val() != "") {
            var passRegister = rsa.encrypt($("#txPassRegister").val());
            $("#txPassRegister").val(passRegister);
            $("#txPassRegisterConfirm").val(passRegister);
        }
    }
}
///([\<])([^\>]{1,})*([\>])/i