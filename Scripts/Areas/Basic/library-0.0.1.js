﻿$(document).ready(function () {
    $("#list-products").attr("data-infinity-method", "library.getProducts");
    $("#list-products").addClass("infinityWindow");
    $("#txtSearch").keyup(function () {
        if ($("#txtIsSearching").val() != "1") {
            if ($("#txtSearch").val().trim() != $("#txtLastSearch").val().trim()) {
                library.search();
            }
        }
    });
    $("#list-products").attr("data-page", $("#txtPage").val());
    resourcePreview.eventHandler();
    $(window).scrollInfinity(".infinityWindow", { addClassInfinity: false });
});
var library = {
    search: function(){
        $("#list-products").html("");
        $("#txtPage").val("1");
        $("#list-products").removeAttr("data-page");
        library.getProducts();
    },
    getProducts: function () {
        $("#txtIsSearching").val("1");
        $("#list-items").addClass("loading-items");
        $("#no-reg").hide();
        var isSearch = false;
        if ($("#txtSearch").val() != "")
            isSearch = true;
        var path = isSearch == true ? "../../AjaxLibrary/SearchProducts" : "../../AjaxLibrary/GetProducts";
        var element = $("#list-products");
        if (typeof element.attr("data-page") == "undefined")
            element.attr("data-page", 1);
        page = element.attr("data-page");
        var values = "";
        if (isSearch == true)
            values = "{'page':" + page + ", 'search': '" + $("#txtSearch").val().trim() + "'}";
        else
            values = "{'page':" + page + "}";
        utils.ajax(path, values, function (result) {
            try {
                result = result.hasOwnProperty("d") ? result.d : result;
                element.attr("data-page", result.page);
                var data = result.result;
                if (data == null || data.length == 0) {
                    $.fn.scrollInfinity.unbind("#list-products");
                    if ($("li.resource", "#list-products").size() == 0) {
                        $("#no-reg").show();
                    }
                }
                else {
                    var items = [];
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].HasDRM == true) {
                            items.push(templatesHTML.group_resource_drm.format(data[i].IdProduct, data[i].Name, data[i].Name.removeSymbols(), data[i].Description));
                        }
                        else {
                            items.push(templatesHTML.group_resource.format(data[i].IdProduct, data[i].Name, data[i].Name.removeSymbols(), data[i].Description));
                        }
                    }
                    $(items.join("")).hide().appendTo("#list-products").fadeIn("slow");
                    resourcePreview.eventHandler();
                }
                var conSearch = false;
                if (typeof result.search != "undefined") {
                    $("#txtLastSearch").val(result.search.trim());
                    if ($("#txtSearch").val().trim() != result.search) {
                        library.search();
                        conSearch = true;
                    }
                }
                if (conSearch == false) {
                    $("#list-items").removeClass("loading-items");
                    $("#txtIsSearching").val("0");
                }
            } catch (e) {
                $("#list-items").removeClass("loading-items");
                $("#txtIsSearching").val("0");
            }
            $.fn.scrollInfinity.callBack("#list-products");
        },
        function (result) {
            $("#txtIsSearching").val("0");
            $("#list-items").removeClass("loading-items");
            $.fn.scrollInfinity.callBack("#list-products");
        });
    }
}