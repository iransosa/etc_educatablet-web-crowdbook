﻿$(document).ready(function () {
    //common.institutionAutocompleteWithLevel();
    $("#uploadAvatar").change(function () {
        if (profile.validateImage($(this).val()) == true) {
            utils.showModalLoading(true);
            $("#formFileAvatar").submit();
        }
    });
    $("#imageCropped").load(function () {
        utils.hideModalLoading();
        cropImage.load();
    });
    $("#btnCrop").click(function () {
        profile.saveCrop();
    });
    $("#btnCancelCrop").click(function () {
        cropImage.instance.cancelSelection();
        $("#modal-image-crop").modal("hide");
    });
    profile.validation_profile();
});
var profile = {
    validateImage: function (fileName) {
        if (fileName == "") {
            alert("¡Debe cargar una imagen!");
            return false;
        }
        else {
            if (utils.validImageExension(fileName))
                return true;
            else
                return false;
        }
    },
    isFirstLoad: true,
    uploadAvatar_complete: function () {
        if (this.isFirstLoad == true) {
            this.isFirstLoad = false;
            return;
        }
        var d = new Date();
        $("#imageCropped").attr("data-crop-type", "1");
        $("#imageCropped").attr("src", "../../Image/PersonTemp/{0}?width=400&height=400&{1}".format($("#txtIdPerson").val(), d.getTime()));
        utils.hideModalLoading();
        utils.showModalLoading(false);
        document.getElementById("formFileAvatar").reset();
        var newImg = $.parseJSON($("#pUploadAvaPerTarget").contents().find("#jsonResult")[0].innerHTML);
        if (newImg.IsValid == false) {
            alert(newImg.Message);
            return;
        }
    },
    saveCrop: function () {
        var values = new Object();
        values.x1 = parseInt($("#cropX1").val());
        values.y1 = parseInt($("#cropY1").val());
        values.x2 = parseInt($("#cropX2").val());
        values.y2 = parseInt($("#cropY2").val());
        values.originalWidth = $("#imageCropped").outerWidth();
        values.originalHeight = $("#imageCropped").outerHeight();
        values = JSON.stringify(values);
        utils.showModalLoading(true);
        utils.ajaxSync('../../ImageActions/CropAvatarPerson', values,
            function (result) {
                result = result.hasOwnProperty("d") ? result.d : result;
                if (result.success == true) {
                    $("#modal-image-crop").modal("hide");
                    var d = new Date();
                    $("#imgAvatar").attr("src", $("#imgAvatar").attr("src") + "&" + d.getTime());
                    $("#imgAvatarHeader").attr("src", $("#imgAvatarHeader").attr("src") + "&" + d.getTime());
                    $("#imgAvatarSide").attr("src", $("#imgAvatarSide").attr("src") + "&" + d.getTime());
                    cropImage.instance.cancelSelection();
                    $("#imageCropped").attr("src", "");
                }
                else {
                    alert(result.error);
                }
            },
            function () {
                utils.errorInternal();
            });
        utils.hideModalLoading();
    },
    validation_profile: function () {
        $("#formProfile").validate({
            rules: {
                FirstName: {
                    required: true,
                    minlength: 1,
                    maxlength: 40,
                },
                LastName: {
                    required: true,
                    minlength: 1,
                    maxlength: 40,
                }
            },
            messages: {
                FirstName: {
                    required: "El Nombre es requerido",
                    minlength: "El Nombre debe tener entre 1 y 40 caracteres",
                    maxlength: "El Nombre debe tener entre 1 y 40 caracteres"
                },
                LastName: {
                    required: "El Apellido es requerido",
                    minlength: "El Apellido debe tener entre 1 y 40 caracteres",
                    maxlength: "El Apellido debe tener entre 1 y 40 caracteres"
                }
            }
        });
    },
    validate_form: function () {
        var isValid = true;
        $("input[type='text']", "#formProfile").each(function () {
            if (utils.validate_form($(this).val()) == false) {
                isValid = false;
            }
        });
        if (isValid == false) {
            $("#div-validations").html(utils.htmlNotLet());
            return false;
        }
        else {
            $("#div-validations").html("");
            return true;
        }
    }
}