﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Web_CrowdBook.CustomAttributes;
using Web_CrowdBook.Helpers;
using Web_CrowdBook.Utils;

namespace Web_CrowdBook
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();

            Utils.Utils.InitializeLogger();

            ModelBinders.Binders[typeof(DateTime?)] = new Web_CrowdBook.Helpers.DateTimeModelBinder()
            {
                //Date = "Date", // Date parts are not splitted in the View
                // (e.g. the whole date is held by a TextBox  with id “xxx_Date”)
                //Time = "Time", // Time parts are not  splitted in the View
                // (e.g. the whole time  is held by a TextBox with id “xxx_Time”)
                Day = "Day",
                Month = "Month",
                Year = "Year"
                //Minute = "Minute",
                //Second = "Second"
            };
        }
        protected void Application_Error()
        {

        }
        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
        }
        //protected void Application_EndRequest(Object sender, EventArgs e)
        //{
        //    HttpContext context = HttpContext.Current;
        //    if (context.Response.Status.Substring(0, 3).Equals("302"))
        //    {
        //        context.Response.ClearContent();
        //        context.Response.Redirect("/Error/TooLarge");
        //        //context.Response.Write(@"<script language='javascript'>self.location='/Error/TooLarge';</script>");
        //    }
        //}
    }
}